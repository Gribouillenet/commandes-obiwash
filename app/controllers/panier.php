<?php class Panier extends Controller {
//AFFICHAGE DU PANIER
	function index(){
		$ids = $_SESSION['Panier'];

        if ( empty($ids) ) {
            $produits = array();
        } else {
	        $ids = array_keys($_SESSION['Panier']);
            $produits = DB::select( 'SELECT * FROM products WHERE id IN (' .implode(',', $ids). ')' );
        }

		$livraisons = DB::select ('SELECT * FROM delivery');

		$id = $_SESSION['Auth']['id'];
 		$membre = DB::select ('SELECT * FROM member WHERE id = ?', [$id]);

	  	$this->view( 'panier/index', ['paniers' => $produits, 'livraisons' => $livraisons, 'membre' => $membre[0]] );
	}
}
