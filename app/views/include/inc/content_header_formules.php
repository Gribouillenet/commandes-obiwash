<?php require($_SERVER['DOCUMENT_ROOT'].'/app/views/include/header_formules.php'); ?>


	<section class="page-inner">
		<header class="page-header">
			<h1>Bon de commande</h1>
			<?php

			$membre = $data['membre'];

	   		if($membre) :

		   	?>
		   		<aside class="infos-membre">
			   		<h2><?= $membre['radio_name'] ?></h2>
			   		<h3><?= $membre['name']?></h3>
			   		<ul>
				   		<li><?= $membre['tel_mob']?></li>
				   		<li><?= $membre['tel_fixe']?></li>
				   		<li><?= $membre['mail_member']?></li>
			   		</ul>
		   		</aside>
		   		<?php endif;?>

		</header>
		<?php $formule = $data['formules'];

			if ( isset( $formule ) ) :
		?>
		<h2 class="title-formule title-formule-<?= $formule['id'] ?>"><?= $formule['title'] ?></h2>
		<?php endif;?>