<?php
class Paniers {

	public function __construct() {
		if ( !isset($_SESSION) ) {
			session_start();
		}
		if ( !isset($_SESSION['Panier']) ) {
			$_SESSION['Panier'] = array();
		}
		if ( !isset($_SESSION['Livraison']) ) {
			$_SESSION['Livraison'] = array();
		}
		if ( isset($_POST['Panier']['quantity']) || isset($_POST['Livraison']) ) {
			$this->recalc();
		}
	}

	private function recalc() {
		foreach ($_SESSION['Panier'] as $id_Products => $quantity) {
			if ( isset($_POST['Panier']['quantity'][$id_Products]) ) {
				$_SESSION['Panier'][$id_Products] = $_POST['Panier']['quantity'][$id_Products];
			}
		}
		$_SESSION['Livraison'] = $_POST['Livraison'];
	}

	static public function count() {
		return array_sum($_SESSION['Panier']);
	}

	static public function total() {
		$total = 0;
		$ids = array_keys($_SESSION['Panier']);
        if ( empty($ids) ) {
            $produits = array();
        } else {
            $produits = DB::select( 'SELECT * FROM products WHERE id IN (' .implode(',', $ids). ')' );
        }
		foreach ( $produits as $produit ) {
			$total += $produit['price'] * $_SESSION['Panier'][$produit['id']];
		}
		return $total;
	}

	static public function add( $id_Products ) {
		if ( isset($_SESSION['Panier'][$id_Products]) ) {
			$_SESSION['Panier'][$id_Products] += 1;
		} else {
			$_SESSION['Panier'][$id_Products] = 1;
		}
	}

	static public function del( $id_Products ) {
		unset($_SESSION['Panier'][$id_Products]);
	}

	static public function delAll() {
		$ids = array_keys($_SESSION['Panier']);
        if ( empty($ids) ) {
            $produits = array();
        } else {
            $produits = DB::select( 'SELECT * FROM products WHERE id IN (' .implode(',', $ids). ')' );
        }

		foreach ( $produits as $produit ) {
			unset($_SESSION['Panier'][$produit['id']]);
		}
	}
}
