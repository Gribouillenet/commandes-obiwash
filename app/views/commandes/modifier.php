<?php require($_SERVER['DOCUMENT_ROOT'].'/app/views/include/header.php'); ?>

<section class="page-inner clearfix">
    <header class="page-header page-header-editcommande d-flex flex-column justify-content-center align-items-center">
        <?php foreach ($data['commandes'] as $key => $commande) {
    $qty_products = explode(',', $commande['ref_Amount']);
    $tarif_1 = $commande['tarif_1'];
    $tarif_2 = $commande['tarif_2'];
    $tarif_3 = $commande['tarif_3'];
    $type_livraison = $commande['type'];
    $mode_paiement = $commande['mode_paiement'];
    $created_at = $commande['date_commande'];
    $idCommande = $commande[0];
    $ref_member = $commande['ref_Member']; ?>
        <h1>Modification de commande</h1>
    </header>
    <?php require($_SERVER['DOCUMENT_ROOT'].'/app/views/include/bandeau-icons.php'); ?>

    <div class="container">
        <div class="recap-commande container-panier">
            <form class="form-enter form-recap-commande bootstrap-table" action="" method="post" class="p-y-3 p-x-2" enctype="multipart/form-data" novalidate>
                <?php
                    if ($idCommande <= 9) {
                        echo '<header class="header-cmd"><h4>RÉFÉRENCE DE COMMANDE <span>OBI' . $ref_member . '-' . $created_at . '-00000' .$idCommande . '</span></h4>';
                    } elseif ($idCommande <= 99) {
                        echo '<header class="header-cmd"><h4>RÉFÉRENCE DE COMMANDE <span>OBI' . $ref_member . '-' . $created_at . '-0000' . $idCommande . '</span></h4>' ;
                    } elseif ($idCommande <= 999) {
                        echo '<header class="header-cmd"><h4>RÉFÉRENCE DE COMMANDE <span>OBI' . $ref_member . '-' . $created_at . '-000' . $idCommande . '</span></h4>';
                    } elseif ($idCommande <= 9999) {
                        echo '<header class="header-cmd"><h4>RÉFÉRENCE DE COMMANDE <span>OBI' . $ref_member . '-' . $created_at . '-00' . $idCommande . '</span></h4>';
                    } elseif ($idCommande <= 99999) {
                        echo '<header class="header-cmd"><h4>RÉFÉRENCE DE COMMANDE <span>OBI' . $ref_member . '-' . $created_at . '-0' . $idCommande . '</span></h4>';
                    } elseif ($idCommande <= 999999) {
                        echo '<header class="header-cmd"><h4>RÉFÉRENCE DE COMMANDE <span>OBI' . $ref_member . '-' . $created_at . '-' . $idCommande . '</span></h4>';
                    }
                echo '<aside class="d-none" style="background-color:#EEEEEE; padding:15px;">
            			<ul style="list-style:none;">
            				<li>Nom : <b>' . $name . '</b></li>
            				<li>Prénom : <b>' . $first_name . '</b></li>
            				<li>Email du client : <b>' . $mail_member . '</b></li>
            				<li>Tél : <b>' . $tel . '</b></li>
            			</ul>
            			</aside>
            		</header>'; ?>
                            </header>
            
                            <?php
                    $ids = explode(',', $commande['ref_Products']);
                $poids = 0;
                $i = 0;
                foreach ($ids as $key => $id_produit) :
                        foreach ($data['produits'] as $key => $produit) :
                            $id = $produit['id'];
                $title = $produit['title'];
                $poids = $produit['weight'];
                if ($id_produit == $id) {
                    $total_poids += $poids * $qty_products[$i];
                }
                endforeach;
                $i += 1;
                endforeach;
            
                $nb_obiwash = 0;
                $total = 0;
                $i = 0;
                echo '<table class="table table-striped col-12">
            				<thead class="table-header">
            						<th scope="col">Description</th>
            						<th scope="col">Prix unitaire</th>
            						<th class="col-1" scope="col">Qté</th>
            						<th class="col-1" scope="col">Total</th>
            					</thead>
            					<tbody>
            					<tr class="item-bucket-product item-bucket-product-<?= $id ?>">';
                foreach ($data['produits'] as $produit) {
                    $id = $produit['id'];
                    $image = $produit['image'];
                    $title = $produit['title'];
                    $description = $produit['description'];
                    $price = $produit['price'];
                    $stock = $produit['stock'];
            
                    $adresse = "http://".$_SERVER['SERVER_NAME'];
                    $_SESSION['Auth']['adresse'] = $adresse;
            
                    if ($image) {
                        echo '<td class="recap-panier-description d-flex align-items-center"><img src="' . $adresse . '/public/files/' . $image . '" title="Image '. $title .'" alt="Image '. $title .'" < />';
                    }
                    echo '
                                <p>' . $description . '</p>
                            </td>
                                <td class="price">' . number_format($price, 2, ',', ' ') . '€</td>
                                <td class="quantity"><input type="number" name="' . $Panier['quantity'][$id] . '" value="' . $_SESSION['Panier'][$id] . '" min="1" max="' . $stock . '"></td>
                                <td class="subtotal">' . number_format($price * $qty_products[$i], 2, ',', ' ') . '€</td>
                                <td class="action">
                                    <a class="link-delete delPanier" href="/produits/suppression/<?php //echo $id; ?>" title="Supprimer"><i class="ti-trash"></i></a>
                                </td>
                            </tr></br>';
            
                    if ($title == 'Obiwash') {
                        $nb_obiwash = $qty_products[$i];
                    }
            
                    $total += $price * $qty_products[$i];
                    $i++;
                };
            
                $frais_port = 0;
                if ($total_poids >= 5) {
                    $frais_port = $tarif_3;
                } else {
                    if ($total_poids >= 2) {
                        $frais_port = $tarif_2;
                    } else {
                        if ($total_poids > 0) {
                            $frais_port = $tarif_1;
                        }
                    }
                }
            
            
                echo '<tr>
                            <td class="items-delivery-total" colspan="2">
                                <p class="total-ht">Sous-total</p>
                                <p class="frais-port">Livraison</p>
                                <p class="total-ttc"><strong>Prix Total</strong></p>
                            </td>
                            <td class="items-delivery-total-final" colspan="2">
                                <p class="total-ht">' . number_format($total, 2, ',', ' ') . ' €</p>
                                <p class="frais-port">' . $frais_port . ' €</p>
                                <p class="total-ttc"><strong>' . number_format($total + $frais_port, 2, ',', ' ') . ' €</strong></p>
                            </td>
                        </tr>
                        </tbody>
                        </table>';?>
                        <div class="form-label form-fiche-client form-enter">
                            <div class="fieldset-cmd">
                                <h2>Choix du mode de livraison</h2>
                                <fieldset class="paiement">
                                    <ul>
                                        <?php
                                            $zones_1 = ['Réunion', 'Guadeloupe', 'Martinique', 'Mayotte', 'Guyane', 'Saint-Pierre et Miquelon', 'Saint-Martin', 'Saint-Barthélemy'];
                                            $zones_2 = ['Polynésie Française', 'Wallis et Futuna', 'Nouvelle Calédonie', 'TAAF'];
                                            $listes = array();
                                        
                                            if ($data['membre']['country'] == 'France') {
                                                $listes = ['Retrait sur place', 'Colissimo a domicile', 'Colissimo a domicile - garanti R2', 'Relais colis'];
                                            }
                                        
                                            foreach ($zones_1 as $zone_1) {
                                                if ($data['membre']['country'] == $zone_1) {
                                                    $listes = ['Colissimo Outre-mer Zone 1'];
                                                    break;
                                                }
                                            };
                                        
                                            foreach ($zones_2 as $zone_2) {
                                                if ($data['membre']['country'] == $zone_2) {
                                                    $listes = ['Colissimo Outre-mer Zone 2'];
                                                    break;
                                                }
                                            };
                                        
                                            foreach ($data['livraisons'] as $key => $livraison) {
                                                $id = $livraison['id'];
                                                $type = $livraison['type'];
                                                $tarif_1 = $livraison['tarif_1'];
                                                $tarif_2 = $livraison['tarif_2'];
                                                $tarif_3 = $livraison['tarif_3'];
                        
                                            foreach ($listes as $liste) {
                                                if ($type == $liste) { ?>
                                                        <li class="d-flex justify-content-between align-items-center">
                                                                    <div class="col-md-1">
                                                                    <input type="radio" name="type_livraison" value="<?= $type ?>" <?php
                                                                                if ($_POST['type_livraison'] == null && $type == 'Retrait sur place') {
                                                                                    echo "checked";
                                                                                } else {
                                                                                    if ($_POST['type_livraison'] == $type) {
                                                                                        echo "checked";
                                                                                    }
                                                                                }
                                                                                ?>>
                                                                    </div>
                                                                    <?php
                                                                                if ($type == 'Colissimo a domicile' or $type == 'Colissimo a domicile - garanti R2') {
                                                                                    $img = '/public/img/png/colissimo.png';
                                                                                } elseif ($type == 'Relais colis') {
                                                                                    $img = '/public/img/png/relais-colis.png';
                                                                                } else {
                                                                                    $img='';
                                                                                }
                                                                                echo '<figure class="logo-livraison col-md-1"><img src="'. $img .'"></figure>';
                                                                                ?>
                                    
                                                                    <?php if ($type == 'Relais colis') { ?>
                                                                    <div class="col-md-8 d-flex flex-column choix-relais-colis">
                                                                        <span><?= $type ?></span>
                                                                        <div class="d-flex relais-colis align-items-center">
                                                                            <a class="button link-relais-colis btn-relais-colis d-flex align-items-center justify-content-center" href="https://www.relaiscolis.com/trouver-un-relais" target="blank">Voir les relais colis</a>
                                                                            <input type="text" name="address_relais_colis" class="form-control form-obligatoire" placeholder="Merci de copier/collé ici l’adresse du relais colis de votre choix" value="<?php if (isset($_POST['address_relais_colis'])) {
                                                                                    echo $_POST['address_relais_colis'];
                                                                                } ?>"></input>
                                                                        </div>
                                                                    </div>
                                                                    <?php if (isset($data['erreur']['address_relais_colis'])) : ?>
                                                                    <div class="alert alert-danger alert-form alert-relais-colis"><?= $data['erreur']['address_relais_colis'] ?></div>
                                                                    <?php endif; ?>
                                                                    <?php } else {
                                                                                    echo '<span class="col-md-8">'. $type .'</span>';
                                                                                }
                                                                                ?>
                                                                    <div class="prix col-md-2 d-flex align-items-center justify-content-center">
                                                                        <span class="d-block">
                                                                            <?php
                                                                            if ($total_poids >= 5) {
                                                                                echo $tarif_3 . ' €';
                                                                            } else {
                                                                                if ($total_poids >= 2) {
                                                                                    echo $tarif_2 . ' €';
                                                                                } else {
                                                                                    echo $tarif_1 . ' €';
                                                                                }
                                                                            }
                                                                            ?></span>
                                                                    </div>
                                                                </li>
                                                        <?php
                                                            }
                                                    }
                                                } ?>
                                    </ul>
                                </fieldset>
            
                                <h2>Choix du mode de paiement</h2>
                                <fieldset class="paiement">
                                    <div class="paiement">
                                        <ul>
                                            <li class="d-flex justify-content-between align-items-center">
                                                <div class="col-md-1">
                                                    <input class="grey" type="radio" name="mode_paiement" value="cb" checked>
                                                </div>
                                                <div class="col-md-1">
                                                    <i class="fab fa-cc-visa"></i>
                                                    <i class="fab fa-cc-mastercard"></i>
                                                </div>
                                                <div class="col-md-10">
                                                    <span>Carte Bancaire</span>
                                                </div>
                                            </li>
                                            <li class="d-flex justify-content-between align-items-center">
                                                <div class="col-md-1">
                                                    <input type="radio" name="mode_paiement" value="paypal" <?php
                                        if ($_POST['mode_paiement'] == 'paypal') {
                                            echo "checked";
                                        } ?>>
                                                </div>
                                                <div class="col-md-1">
                                                    <i class="fab fa-cc-paypal"></i>
                                                </div>
                                                <div class="col-md-10">
                                                    <span>PayPal</span>
                                                </div>
                                            </li>
                                            <li class="d-flex justify-content-between align-items-center">
                                                <div class="col-md-1">
                                                    <input type="radio" name="mode_paiement" value="cheque" <?php
                                        if ($_POST['mode_paiement'] == 'cheque') {
                                            echo "checked";
                                        } ?>>
                                                </div>
                                                <div class="col-md-1">
                                                    <i class="fas fa-money-check"></i>
                                                </div>
                                                <div class="col-md-10">
                                                    <span>Chèque</span>
                                                </div>
                                            </li>
                                            <li class="d-flex justify-content-between align-items-center">
                                                <div class="col-md-1">
                                                    <input class="grey" type="radio" name="mode_paiement" value="virement" <?php
                                        if ($_POST['mode_paiement'] == 'virement') {
                                            echo "checked";
                                        } ?>>
                                                </div>
                                                <div class="col-md-1">
                                                    <i class="fas fa-piggy-bank"></i>
                                                </div>
                                                <div class="col-md-10">
                                                    <span>Virement Bancaire<span>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                    <div>
                                    </div>
                                </fieldset>
                                <div class="btn-modif-commande">
                                    <h4><b>ATTENTION !</b><br>
                                        Pour que vos changements dans votre commande soient pris en compte.<br>
                                        Merci de mettre à jour la commande via bouton ci-dessous prévu à cet effet avant de la valider à nouveau.</h4>
                                    <div class="button btn btn-valid d-inline-block ml-md-0">    
                                        <input type="submit" value="Mettre à jour la commande"><i class="ti-reload"></i>
                                    </div>
                                        <a class="button btn btn-ordered-ok d-inline-block" href="/commandes">Valider la commande<i class="ti-check"></i></a>
                                </div>
                            </div>
                        </div>
<?php };
?>           
            </form>
        </div>
    </div>
</section>
</div>

<?php require($_SERVER['DOCUMENT_ROOT'].'/app/views/include/footer.php'); ?>