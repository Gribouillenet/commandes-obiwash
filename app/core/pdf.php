<?php
// Création de la class PDF
class PDF extends FPDF {
    // Header
    function Header() {
        // Logo
        $this->Image(ROOT . 'public/img/jpg/obiwash.jpg',8,2,80);
        // Saut de ligne
        $this->Ln(20);
    }
    // Footer
    function Footer() {
        // **************************
        // pied de page
        // **************************
        $this->SetXY( 1, 270 );
        $this->SetFont('Arial','',7);
        $this->Cell( $this->GetPageWidth(), 1, utf8_decode("HPNT SYSTÈMES - LES PERRIERS CIDEX 715 BIS. LE CLOS DES ROCHETTES 71570 LEYNES"), 0, 0, 'C');

        $this->SetXY( 1, 274 );
        $this->Cell( $this->GetPageWidth(), 1, "siret : 79270867900024", 0, 0, 'C');
    }
}
