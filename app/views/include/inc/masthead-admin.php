<header id="masthead" class="site-header" role="banner">
	<div id="widgets-contact-reseaux" class="widgets-contact-reseaux">
		<div class="container d-flex">
			<?php include('widgets-reseaux.php');?>
		</div>
	</div>
	<div class="content-navigations">
		<div class="container container-navigation d-flex align-items-center p-0">
			<a class="logo-site d-block" href="/admin" class="navbar-brand" rel="home">Obiwash® - La boutique -  le mélangeur intelligent pour tous</a>
			<nav class="primary-navigation">
				<ul class="nav navbar d-flex">
		          <li class="nav-item nav-item-active"><a class="nav-link nav-link-gestion-produits" href="/admin">Gérer les produits</a></li>
				  <li class="nav-item"><a class="nav-link nav-link-gestion-membres" href="/admin/membres">Gérer les clients</a></li>
		          <li class="nav-item"><a class="nav-link nav-link-deconnexion" href="/connexion/deconnexion">Déconnexion</a></li>
		        </ul>
	        </nav>
		</div>
	</div>
</header>