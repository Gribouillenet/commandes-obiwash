<?php

class Membre extends Controller
{
    //AFFICHAGE DES PRODUITS
    public function index()
    {
        $produits = DB::select('SELECT * FROM products');

        $id = $_SESSION['Auth']['id'];
        $membre = DB::select('SELECT * FROM member WHERE id = ?', [$id]);

        foreach ($produits as $key => $produit) {
            $produits[$key]['description'] = nl2br($produit['description']);
        }

        $this->view('produits/index', ['produits' => $produits, 'membre' => $membre[0]]);
    }
    //INSCRIPTION COMPTE MEMBRE
    public function inscription()
    {
        if (!empty($_POST)) {
            extract($_POST);
            $erreur = [];

            if (empty($login) || empty($name) || empty($first_name) || empty($address) || empty($code_postal) || empty($city) || empty($country) || empty($mail_member) || empty($tel)) {
                $erreur['champ_obligatoire'] = '* Champs Obligatoires';
            }

            if (empty($login)) {
                $erreur['login'] = 'Le nom d\'utilisateur est obligatoire !';
            }

            if (empty($name)) {
                $erreur['name'] = 'Le nom est obligatoire !';
            }

            if (empty($first_name)) {
                $erreur['first_name'] = 'Le prénom est obligatoire !';
            }

            if (empty($address)) {
                $erreur['address'] = 'L\'adresse est obligatoire !';
            }

            if (empty($code_postal)) {
                $erreur['code_postal'] = 'Le code postal est obligatoire !';
            } else {
                if (!preg_match('`^[0-9]{5}$`', $code_postal)) {
                    $erreur['code_postal'] = 'Le code postal est invalide !';
                }
            }

            if (empty($city)) {
                $erreur['city'] = 'La ville est obligatoire !';
            }

            if (empty($country)) {
                $erreur['country'] = 'Le pays est obligatoire !';
            }

            if (empty($mail_member)) {
                $erreur['mail_member'] = 'L\'email est obligatoire !';
            } elseif (!filter_var($mail_member, FILTER_VALIDATE_EMAIL)) {
                $erreur['mail_member'] = 'L\'adresse email est invalide !';
            }

            function validerNumero($telATester)
            {
                //Retourne le numéro s'il est valide, sinon false.
                return preg_match('`^0[1-9]([-. ]?[0-9]{2}){4}$`', $telATester) ? $telATester : false;
            }

            if (empty($tel)) {
                $erreur['tel'] = 'Le téléphone est obligatoire !';
            }

            if (!empty($tel)) {
                if (!filter_var($tel, FILTER_CALLBACK, array('options' => 'validerNumero'))) {
                    $erreur['tel'] = 'Le téléphone est incorrect.';
                }
            }

            //$since=date("Y-m-d H:m:s");

            if (!$erreur) {
                $compte = DB::select('SELECT login, mail_member FROM member');
                $bool = true;
                if ($compte) {
                    for ($i = 0; $i < sizeof($compte); $i+=1) {
                        if (($compte[$i]['login'] == $login) || ($compte[$i]['mail_member'] == $mail_member)) {
                            $bool = false;
                        }
                    }
                }
                if ($bool) {
                    DB::insert('insert into member (name, first_name, company_service, address, address_supplement, code_postal, city, country, mail_member, tel, login) values (:name, :first_name, :company_service, :address, :address_supplement, :code_postal, :city, :country, :mail_member, :tel, :login)', [
                        'name' => htmlspecialchars($name),
                        'first_name' => htmlspecialchars($first_name),
                        'company_service' => htmlspecialchars($company_service),
                        'address' => htmlspecialchars($address),
                        'address_supplement' => htmlspecialchars($address_supplement),
                        'code_postal' => htmlspecialchars($code_postal),
                        'city' => htmlspecialchars($city),
                        'country' => htmlspecialchars($country),
                        'mail_member' => htmlspecialchars($mail_member),
                        'tel' => htmlspecialchars($tel),
                        'login' => htmlspecialchars($login)
                    ]);

                    $password2 = $this->generatePassword();
                    $password = md5($password2);

                    DB::update('update  member set password = :password  where login = :login', [
                        'password' =>  $password ,
                        'login' => $login
                    ]);
                    $url ='https://commandes.obiwash.gribdev.eu/public/';
                    $to = $mail_member;
                    $subject = 'Obiwash - La boutique | Inscription au site et création de vos identifiants';
                    $message = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional //EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
                    <html xmlns="http://www.w3.org/1999/xhtml">
                    <head>
                        <title>Obiwash - La boutique | Inscription au site et création de vos identifiants</title>
                        <meta name="viewport" content="width=device-width, initial-scale=1">
                        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
                        <style>
                        /* Fonts and Content */
                        body, td { font-family: "Helvetica Neue", Arial, Helvetica, Geneva, sans-serif; font-size:14px; }
                        body { background-color: #003880; margin: 0; padding: 0; -webkit-text-size-adjust:none; -ms-text-size-adjust:none; }
                        h2{ padding-top:12px; /* ne fonctionnera pas sous Outlook 2007+ */color:#000000; font-size:22px; }
                        p{margin: 15px 0;}
                    
                        @media only screen and (max-width: 480px) {
                    
                            table[class=w275], td[class=w275], img[class=w275] { width:135px !important; }
                            table[class=w30], td[class=w30], img[class=w30] { width:10px !important; }
                            table[class=w580], td[class=w580], img[class=w580] { width:280px !important; }
                            table[class=w640], td[class=w640], img[class=w640] { width:300px !important; }
                            img{ height:auto;}
                             /*illisible, on passe donc sur 3 lignes */
                            table[class=w180], td[class=w180], img[class=w180] {
                                width:280px !important;
                                display:block;
                            }
                            td[class=w20]{ display:none; }
                        }
                    
                        </style>
                    
                    </head>
                    <body style="margin:0px; padding:0px; -webkit-text-size-adjust:none; background-color:#f7f7f7;">
                        <table style="padding:0px; margin:0px auto; width:100%;">
                            <tbody>
                                <tr>
                                    <td style="text-align:center;">
                                        <table style="border:0px; padding:0px; margin:0px  auto;">
                                            <tbody>
                                                <tr>
                                                    <td class="w640" width="640" height="20">
                                                    <img style="text-decoration: none; display: block;" src="' . $url . '/img/gif/white-space.gif" alt="space"/>
                                                </td>
                                                </tr>
                    
                                                <tr class="pagetoplogo">
                                                    <td class="w640" style="width:640px">
                                                        <table class="w640" style="padding:0px; margin:0px; width:640px;">
                                                            <tbody>
                                                                <td class="w640" style="width:640px;">
                                                                    <img class="w30" style="text-decoration: none; display: block;" src="' . $url . 'img/jpg/obiwash-top-email.jpg" alt="Obiwash®, le mélangeur intelligent pour tous" />
                                                                </td>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                <tr>
                                                <tr class="content">
                                                    <td class="w640" style="background-color:#FFFFFF; width:640px">
                                                         <table class="w640" style="padding:0px; margin:0px; width:640px;">
                                                            <tbody>
                                                                <tr>
                                                                    <td class="w30" style="width:30px;">
                                                                        <img class="w30" style="text-decoration: none; display: block;" src="' . $url . '/img/gif/white-space.gif" alt="space"/>
                                                                    </td>
                                                                    <td class="w580" width="580">
                                                                        <p style="margin: 15px 0;">Bonjour,<br><br>Vous vous êtes inscrit sur le site de commande en ligne obiwash.fr<br/>et nous vous en remercions.</p>
                                                                        <p style="margin: 15px 0;">Veuillez trouver ci-après votre identifiant ainsi que votre nouveau mot de passe<br/>pour vous connecter à la boutique en ligne.</p>
                                                                        <p style="text-align:center; font-size: 14px; margin: 15px 0">
                                                                            <a href="' . $url . '" title="Voir les produits de la boutique Obiwash®" target="_blank" style="background-color: #01A0C6; border:none; color: #FFFFFF; cursor: pointer; font-size:18px; box-shadow: none!important; font-weight: bold; padding: 5px;"><button style="background-color: #01A0C6; border:none; color: #FFFFFF; cursor: pointer; font-size:18px; box-shadow: none!important; font-weight: bold; padding: 5px;">Boutique - Obiwash</a>
                                                                            </a>
                                                                        </p>
                                                                        <p style="margin: 15px 0;">Nous restons bien évidemment à votre disposition pour vous accompagner.</p><br>
                                                                        <p style="margin: 0 0 15px;">Votre identifiant : <b>' . $login . '</b></p>
                                                                        <p style="margin: 15px 0;">Votre mot de passe : <b>' . $password2 . '<b></p>
                                                                        <p style="margin: 15px 0">Merci de votre confiance.<br>L\'équipe de Obiwash®</p>
                                                                        <p style="margin: 15px 0;">
                                                                            <em style="color: #938795; font-size: 10px; font-weight: normal;">
                                                                                Obiwash® 2019. Tous droits réservés. Application réalisée par <a href="https://gribouillenet.fr" target="_blank" title="Voir les autres réalisations du webmaster">Gribouillenet©</a>
                                                                            </em>
                                                                        </p>
                                                                    </td>
                                                                    <td class="w30" width="30">
                                                                         <img class="w30" style="text-decoration: none; display: block;" src="' . $url . '/img/gif/white-space.gif" alt="space"/>
                                                                     </td>
                                                                </tr>
                                                            </tbody>
                                                         </table>
                                                     </td>
                                                </tr>
                                                <tr>
                                                    <td class="w640" width="640" height="20">
                                                        <img style="text-decoration: none; display: block;" src="' . $url . '/img/gif/white-space.gif" alt="space"/>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </body>
                    </html>' ;
                    $headers = 'From: boutique-obiwash@obiwash.fr' . "\r\n" .	'Reply-To: no-reply@obiwash.fr' . "\r\n" . 'Content-Type: text/html; charset=utf-8' .
                    'X-Mailer: PHP/' . phpversion();

                    mail($to, $subject, $message, $headers);

//                     $json['message'] = 'Votre compte a bien été créé !<br>Un mail a été envoyé à l\'adresse suivante : ' . $mail_member;
// 
//                     $this->view('membre/inscription', ['json' => $json]);
                    header( 'Location: /membre/merci' );
                } else {
                    $erreur['erreur'] = 'L\'identifiant ou l\'adresse mail sont déja existants.';

                    $this->view('membre/inscription', ['erreur' => $erreur]);
                }
            } else {
                $this->view('membre/inscription', ['erreur' => $erreur]);
            }
        }
        $this->view('membre/inscription');
    }
    //INSCRIPTION COMPTE MEMBRE MERCI
    public function merci()
    {
        $this->view('membre/merci');
    }
    //COMPTE
    public function compte()
    {
        if (!isset($_SESSION['Auth']['id'])) {
            header('Location: /');
        }

        $id = $_SESSION['Auth']['id'];

        $commandes = DB::select('SELECT * FROM order_member, member WHERE member.id = ? AND ref_Member = member.id GROUP BY order_member.id ORDER BY order_member.id DESC', [$id]);

        $membre = DB::select('SELECT * FROM  member WHERE  member.id = ?', [$id]);

        if (!$commandes) {
            $commandes = DB::select('SELECT * FROM  order_member WHERE ref_Member.id = ? ', [$id]);
        }

        $produits = DB::select('SELECT * FROM products');

        foreach ($commandes as $key => $commande) {
            $dateCommande = date_create($commande['date_commande']);
            $commandes[$key]['date_commande'] = date_format($dateCommande, 'dmy');

            $commandes[$key]['observations'] = nl2br($commande['observations']);
        }

        $this->view('membre/compte', ['account_commandes' => $commandes, 'account_produits' => $produits, 'membre' => $membre]);
    }
    //ÉDITER SON COMPTE MEMBRE
    public function editer()
    {
        if (!isset($_SESSION['Auth']['id'])) {
            header('Location: /');
        }

        $id = $_SESSION['Auth']['id'];
        $membre = DB::select('SELECT * FROM member WHERE id = ?', [$id]);

        if (!$membre) {
            header('Location: /');
        }

        if (!empty($_POST)) {
            extract($_POST);
            $erreur = [];

            if (empty($login) || empty($password) || empty($name) || empty($first_name) || empty($address) || empty($code_postal) || empty($city) || empty($country) || empty($mail_member) || empty($tel)) {
                $erreur['champ_obligatoire'] = '<sup>*</sup>Champs Obligatoires';
            }
            
            // AVEC CONFIRMATION PWD
            //if (empty($login) || empty($password) || empty($name) || empty($first_name) || empty($address) || empty($code_postal) || empty($city) || empty($country) || empty($mail_member) || empty($tel)) || empty($confirm_password) {
            //     $erreur['champ_obligatoire'] = '<sup>*</sup>Champs Obligatoires';
            // }

            if (empty($login)) {
                $erreur['login'] = 'Le nom d\'utilisateur est obligatoire !';
            }

            if (empty($password)) {
                $erreur['password'] = 'Le mot de passe est obligatoire !';
            }
            // elseif (empty($password)) {
            //     $erreur['confirm_password'] = 'La confirmation de mot de passe est obligatoire !';
            // } elseif ($password != $confirm_password) {
            //     $erreur['confirm_password'] = 'La confirmation du mot de passe est différente !';
            // }

            if (empty($name)) {
                $erreur['name'] = 'Le nom est obligatoire !';
            }

            if (empty($first_name)) {
                $erreur['first_name'] = 'Le prénom est obligatoire !';
            }

            if (empty($address)) {
                $erreur['address'] = 'L\'adresse est obligatoire !';
            }

            if (empty($code_postal)) {
                $erreur['code_postal'] = 'Le code postal est obligatoire !';
            } else {
                if (!preg_match('`^[0-9]{5}$`', $code_postal)) {
                    $erreur['code_postal'] = 'Le code postal est invalide !';
                }
            }

            if (empty($city)) {
                $erreur['city'] = 'La ville est obligatoire !';
            }

            if (empty($country)) {
                $erreur['country'] = 'Le pays est obligatoire !';
            }

            if (empty($mail_member)) {
                $erreur['mail_member'] = 'L\'email est obligatoire !';
            } elseif (!filter_var($mail_member, FILTER_VALIDATE_EMAIL)) {
                $erreur['mail_member'] = 'L\'adresse email est invalide !';
            }

            function validerNumero($telATester)
            {
                //Retourne le numéro s'il est valide, sinon false.
                return preg_match('`^0[1-9]([-. ]?[0-9]{2}){4}$`', $telATester) ? $telATester : false;
            }

            if (empty($tel)) {
                $erreur['tel'] = 'Le téléphone est obligatoire !';
            }

            if (!empty($tel)) {
                if (!filter_var($tel, FILTER_CALLBACK, array('options' => 'validerNumero'))) {
                    $erreur['tel'] = 'Le téléphone est incorrect.';
                }
            }

            //$since=date("Y-m-d H:m:s");

            if (!$erreur) {
                DB::update('update member set name = :name, first_name = :first_name, company_service = :company_service, address = :address, address_supplement = :address_supplement, code_postal = :code_postal, city = :city, country = :country, mail_member = :mail_member, tel = :tel, login = :login, password = :password where id = :id', [
                    'name' => htmlspecialchars($name),
                    'first_name' => htmlspecialchars($first_name),
                    'company_service' => htmlspecialchars($company_service),
                    'address' => htmlspecialchars($address),
                    'address_supplement' => htmlspecialchars($address_supplement),
                    'code_postal' => htmlspecialchars($code_postal),
                    'city' => htmlspecialchars($city),
                    'country' => htmlspecialchars($country),
                    'mail_member' => htmlspecialchars($mail_member),
                    'tel' => htmlspecialchars($tel),
                    'login' => htmlspecialchars($login),
                    'password' => md5($password),
                    'id' => $id
                ]);
                header('Location: /membre/compte');
            } else {
                $this->view('membre/editer', ['erreur' => $erreur, 'membre' => $membre[0]]);
            }
        }
        $this->view('membre/editer', ['membre' => $membre[0]]);
    }
    //GÉNÉRER UN MOT MOT DE PASSE
    private function generatePassword($length = 8)
    {
        $genpassword = "";
        $possible = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $i = 0;

        while ($i < $length) {
            $char = substr($possible, mt_rand(0, strlen($possible)-1), 1);
            if (!strstr($genpassword, $char)) {
                $genpassword .= $char;
                $i++;
            }
        }
        return $genpassword;
    }

    //RÉ-ÉDITER UN MOT DE PASSE
    public function password()
    {
        if (!empty($_POST)) {
            extract($_POST);
            $erreur = [];

            if (empty($login)) {
                $erreur['login'] = 'Le nom d\'utilisateur est obligatoire !';
            } else {
                $member = DB::select('SELECT * FROM member WHERE login = ?', [$login]);

                if (!$member) {
                    $erreur['login'] = 'Le nom d\'utilisateur n\'existe pas !';
                }
            }

            if (!$erreur) {
                $id = $member[0]['id'];
                $mail_member = $member[0]['mail_member'];
                $password2 = $this->generatePassword();
                $password = md5($password2);
                //$password = password_hash($password2, PASSWORD_DEFAULT );

                DB::update('update  member set password = :password  where id = :id', [
                                'password' =>  $password ,
                                'id' => $id
                            ]);
                $url ='https://commandes.obiwash.gribdev.eu/public/';
                $to = $mail_member;
                $subject = 'Obiwash - La boutique | Renouvellement de votre mot de passe';
                $message = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional //EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
		<html xmlns="http://www.w3.org/1999/xhtml">
		<head>
		    <title>Obiwash - Vos identifiants</title>
		    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		   <meta content="width=device-width">
		    <style type="text/css">
		    /* Fonts and Content */
		    body, td { font-family: "Helvetica Neue", Arial, Helvetica, Geneva, sans-serif; font-size:14px; }
		    body { background-color: #003880; margin: 0; padding: 0; -webkit-text-size-adjust:none; -ms-text-size-adjust:none; }
		    h2{ padding-top:12px; /* ne fonctionnera pas sous Outlook 2007+ */color:#000000; font-size:22px; }
		    p{margin: 15px 0;}

		    @media only screen and (max-width: 480px) {

		        table[class=w275], td[class=w275], img[class=w275] { width:135px !important; }
		        table[class=w30], td[class=w30], img[class=w30] { width:10px !important; }
		        table[class=w580], td[class=w580], img[class=w580] { width:280px !important; }
		        table[class=w640], td[class=w640], img[class=w640] { width:300px !important; }
		        img{ height:auto;}
		         /*illisible, on passe donc sur 3 lignes */
		        table[class=w180], td[class=w180], img[class=w180] {
		            width:280px !important;
		            display:block;
		        }
		        td[class=w20]{ display:none; }
		    }

		    </style>

		</head>
		<body style="margin:0px; padding:0px; -webkit-text-size-adjust:none; background-color:#f7f7f7;">
        <table style="padding:0px; margin:0px; width:100%;">
		        <tbody>
		            <tr>
		                <td style="text-align:center;">
                        <table style="border:0px; padding:0px; margin:0px  auto;">
                            <tbody>
                                <tr>
                                    <td class="w640" width="640" height="20">
                                        <img style="text-decoration: none; display: block;" src="' . $url . 'img/gif/white-space.gif" alt="space"/>
                                    </td>
                                </tr>

		                            <tr class="pagetoplogo">
			                            <td class="w640" width="640">
				                            <table class="w640" width="640" >
					                            <tbody>
						                            <td class="w640" style="width:640px; margin:0; padding:0;">
                                                    <img class="w30" style="text-decoration: none; display: block;" src="' . $url . 'img/jpg/obiwash-top-email.jpg" alt="Obiwash®, le mélangeur intelligent pour tous" />
                                                </td>
					                            </tbody>
				                            </table>
			                            </td>
			                        <tr>
				                    <tr class="content">
					                   <td class="w640" style="background-color:#FFFFFF; width:640px">
					                     	<table class="w640" style="width:640px; margin:0; padding:0;">
		                                        <tbody>
		                                            <tr>
		                                                <td class="w30" width="30">
			                                                <img class="w30" style="text-decoration: none; display: block;" src="' . $url . 'img/gif/white-space.gif" alt="space"/>
		                                                </td>
		                                                <td class="w580" width="580">
			                                                <p style="margin: 15px 0;">Bonjour,<br>Vous nous avez demandé de vous générer un nouveau mot de passe.</p>
			                                                <p style="margin: 15px 0;">Veuillez trouver ci-après votre identifiant ainsi que votre nouveau mot de passe pour vous connecter à la boutique en ligne. Si vous n\'êtes pas à l\'origine de cette demande, merci de ne pas tenir compte de cet email.</p>
			                                                <p style="text-align:center; font-size: 14px; margin: 15px 0">
				                                                <a href="' . $url . '" title="Voir les produits de la boutique Obiwash®" target="_blank" style="background-color: #01A0C6; border:none; color: #FFFFFF; cursor: pointer; font-size:18px; box-shadow: none!important; font-weight: bold; padding: 5px;"><button style="background-color: #01A0C6; border:none; color: #FFFFFF; cursor: pointer; font-size:18px; box-shadow: none!important; font-weight: bold; padding: 5px;">Boutique - Obiwash</button>
					                                            </a>
					                                        </p>
					                                        <p style="margin: 15px 0;">Nous restons bien évidemment à votre disposition pour vous accompagner.</p><br>
					                                        <p style="margin: 0 0 15px;">Votre identifiant : <b>' . $member[0]['login'] . '</b></p>
					                                        <p style="margin: 15px 0;">Votre mot de passe : <b>' . $password2 . '<b></p>
															<p style="margin: 15px 0">Merci de votre confiance.<br>L\'équipe de Obiwash</p>
					                                        <p style="margin: 15px 0;">
						                                        <em style="color: #938795; font-size: 10px; font-weight: normal;">
						                                        	Obiwash® 2019. Tous droits réservés. Application réalisée par <a href="https://gribouillenet.fr" target="_blank" title="Voir les autres réalisations du webmaster">Gribouillenet©</a>
						                                        </em>
						                                    </p>
					                                    </td>
		                                                <td class="w30" width="30">
			                                                 <img class="w30" style="text-decoration: none; display: block;" src="' . $url . 'img/gif/white-space.gif" alt="space"/>
		                                                 </td>
		                                            </tr>
		                                        </tbody>
					                     	</table>
				                     	</td>
				                    </tr>
				                    <tr>
		                                <td class="w640"  width="640" height="20">
                                        <img style="text-decoration: none; display: block;" src="' . $url . 'img/gif/white-space.gif" alt="space"/></td>
		                            </tr>
					            </tbody>
					        </table>
					    </td>
					</tr>
				</tbody>
			</table>
		</body>
		</html>' ;
                $headers = 'From: boutique-obiwash@obiwash.fr' . "\r\n" .	'Reply-To: no-reply@obiwash.fr' . "\r\n" . 'Content-Type: text/html; charset=utf-8' .
                'X-Mailer: PHP/' . phpversion();

                mail($to, $subject, $message, $headers);

                $rest = substr($mail_member, 0, 3);
                $extension = substr($mail_member, strpos($mail_member, '@')+strlen('@'));

                $mail_member = $rest . '***' . $extension;

                $json['message'] = 'Votre mot de passe a bien été modifié automatiquement,<br>Un mail a été envoyé à l\'adresse suivante : ' . $mail_member . '<br/>Merci de vérifier vos emails, y compris en notification ou spam.';
 
                 $this->view('membre/password', ['json' => $json]);
            // header( 'Location: /membre/mot-de-passe' );
            } else {
                $this->view('membre/password', ['erreur' => $erreur]);
            }
        }

        $this->view('membre/password');
    }
}
