<style>
	
	textarea[name="description"], textarea[name="data_sheet"] {
		width: 1110px;
		resize: vertical;
		min-height: 150px;
	}

	input[name="stock"] {
		padding: 0 10px !important;
	}

	img.produit-illustration {
		max-height: 250px;
		max-width: 250px;
		object-fit: cover;
		margin: 30px;
	}

	.submit-button {
		color: #fff;
		background-color: #28a745;
		border-color: #28a745;
		display: inline-block;
		font-weight: 400;
		text-align: center;
		white-space: nowrap;
		vertical-align: middle;
		user-select: none;
		border: 1px solid transparent;
		padding: 0.375rem 0.75rem;
		font-size: 1rem;
		line-height: 1.5;
		border-radius: 0.25rem;
		transition: color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out;
	}

	.submit-button:hover {
		color: #28a745 !important;
		cursor: pointer;
		background-color: white;
		border-color: #28a745;
	}

</style>

<?php require($_SERVER['DOCUMENT_ROOT'].'/app/views/include/header_admin.php'); ?>
	<section class="page-inner clearfix">
		<header class="page-header d-flex flex-column justify-content-center align-items-center">
	      <h1>Éditer un produit</h1>
		</header>
<div class="container edit-produit">
		<?php 

		if ( isset( $data['erreur'] ) ) { ?>
		    <div class="alert alert-danger alert-on"><?= $data['erreur']['champ_obligatoire'] ?></div>
		<?php } else { ?>
			<span class="champ-obligatoire d-inline-block">* Champs Obligatoires</span>
		<?php } ?>

		<form class="form-label-admin" action="" method="post" class="p-y-3 p-x-2" enctype="multipart/form-data" novalidate>

			<label>Nom du produit*</label>
			<input type="text" name="title" class="form-control form-obligatoire form-enter" placeholder="Nom du produit" value="<?= $data['produits']['title'] ?>" onkeypress="refuserToucheEntree(event)">
			<?php if ( isset( $data['erreur']['title'] ) ) : ?>
				<div class="alert alert-danger alert-form"><?= $data['erreur']['title'] ?></div>
			<?php endif; ?>

			<label for="description">Description du produit*</label>
			<textarea id="description" name="description" class="form-control form-obligatoire" placeholder="Descriptif du produit"><?= $data['produits']['description'] ?></textarea>
			<?php if ( isset( $data['erreur']['description'] ) ) : ?>
				<div class="alert alert-danger alert-form"><?= $data['erreur']['description'] ?></div>
			<?php endif; ?>

			<label for="prix-produit">Prix du produit*</label>
			<input id="prix-produit" type="text" name="price" class="form-control form-obligatoire form-enter" placeholder="Prix du produit : 9.99" value="<?= $data['produits']['price'] ?>" onkeypress="refuserToucheEntree(event)">
			<?php if ( isset( $data['erreur']['price'] ) ) : ?>
				<div class="alert alert-danger alert-form"><?= $data['erreur']['price'] ?></div>
			<?php endif; ?>

			<label>Stock du produit*</label>
			<input type="number" name="stock" class="form-control form-obligatoire form-enter" placeholder="Stock du produit" value="<?= $data['produits']['stock'] ?>" min="0" onkeypress="refuserToucheEntree(event)">
			<?php if ( isset( $data['erreur']['stock'] ) ) : ?>
				<div class="alert alert-danger alert-form"><?= $data['erreur']['stock'] ?></div>
			<?php endif;?>

			<label>Poid du produit en kg*</label>
			<input type="text" name="weight" class="form-control form-obligatoire form-enter" placeholder="Poid du produit en kg : 0.5" value="<?= $data['produits']['weight'] ?>" onkeypress="refuserToucheEntree(event)">
			<?php if ( isset( $data['erreur']['weight'] ) ) : ?>
				<div class="alert alert-danger alert-form"><?= $data['erreur']['weight'] ?></div>
			<?php endif; ?>


			<!-- <label>Produit fini HxLxP*</label>
			<input type="text" name="finished_product" class="form-control form-obligatoire form-enter" placeholder="Produit fini HxLxP : 170 x 180 x 75 (mm)" value="<?// echo $data['produits']['finished_product'] ?>" onkeypress="refuserToucheEntree(event)">
			<?php //if ( isset( $data['erreur']['finished_product'] ) ) : ?>
				<div class="alert alert-danger alert-form"><?php //echo $data['erreur']['finished_product'] ?></div>
			<?php //endif; ?>

			<label>Produit emballé*</label>
			<input type="text" name="packed_product" class="form-control form-obligatoire form-enter" placeholder="Produit emballé : 280 x 220 x 200 (mm)" value="<?// echo $data['produits']['packed_product'] ?>" onkeypress="refuserToucheEntree(event)">
			<?php //if ( isset( $data['erreur']['packed_product'] ) ) : ?>
				<div class="alert alert-danger alert-form"><?// echo $data['erreur']['packed_product'] ?></div>
			<?php //endif; ?> -->

			<label for="data_sheet">Fiche technique du produit*</label>
			<textarea id="data_sheet" name="data_sheet" class="form-control form-obligatoire" placeholder="Nom colonne : valeur;"><?= $data['produits']['data_sheet'] ?></textarea>
			<?php if ( isset( $data['erreur']['data_sheet'] ) ) : ?>
				<div class="alert alert-danger alert-form"><?= $data['erreur']['data_sheet'] ?></div>
			<?php endif; ?>

			<h3>Fichiers numériques<br><em>(ne sont acceptés que les types : png, jpeg et faisant au maximum 500Ko)</em></h3>
			<div class="upload-file file-1">
				<?php
					if(isset($data['produits']['image'])) {
						?> <img src="../../../public/files/<?= $data['produits']['image'] ?>" alt="produit-illustration-<?= $data['produit']['id']; ?>" class="produit-illustration"> <?php
					}
				?>
				<input id="input-file" name="fichier" type="file" class="form-control input-file" placeholder="Joindre un fichier" value="<?= $data['produits']['image'] ?>" >
				<div id="file-select-button" class="file-select-button"><span class="btn-file-dwnl">Sélectionnez un fichier</span><i class="fa fa-upload"></i></div>
				 <div class="upload-delete-file">
					<i id="delete-file" class="form-control delete-file fa-delete"></i>
				 </div>
			</div>
			<p class="file-return"></p>
			<?php if ( isset( $data['erreur']['fichier'] ) ) : ?>
				<div class="alert alert-danger alert-file alert-form"><?= $data['erreur']['fichier'] ?></div>
			<?php endif; ?>

			<input type="submit" class="submit-button" value="Enregistrer">
		</form>
		<div class="col-12 pl-0"><a href="/admin" class="link-come-back"><button>Retour</button></a></div>
			</div>
   </section>
<?php require($_SERVER['DOCUMENT_ROOT'].'/app/views/include/footer.php'); ?>
