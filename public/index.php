<?php
session_start();

define( 'ROOT', str_replace( 'public/index.php', '', $_SERVER['SCRIPT_FILENAME'] ) );

require_once ROOT . 'app/core/App.php';
require_once ROOT . 'app/core/Controller.php';
require_once ROOT . 'app/core/DB.php';
require_once ROOT . 'app/core/Paniers.php';
require_once ROOT . 'app/core/PayPalPayment.php';
require_once ROOT . 'app/core/fpdf.php';
require_once ROOT . 'app/core/pdf.php';

$app = new App;

// echo 'Site en construction. Merci de revenir plus tard.';