<?php class Paypal extends Controller {
//AFFICHAGE DES PRODUITS
	function index(){
        if( !isset( $_SESSION['Auth']['id'] ) ) {
            header( 'Location: /connexion' );
        }

        $id_membre = $_SESSION['Auth']['id'];
        $membre = DB::select ('SELECT * FROM member WHERE id = ?', [$id_membre]);

        /*$ids_products = DB::select( 'SELECT ref_Products FROM order_member WHERE (ref_Member = ?) ORDER BY id DESC LIMIT 1', [$id_membre] );
        $produits = DB::select( 'SELECT * FROM products WHERE id IN ('.$ids_products[0]['ref_Products'].')' );*/

        $commandes = DB::select( 'SELECT * FROM order_member, member WHERE
        order_member.id = (SELECT MAX(id) FROM order_member WHERE ref_Member = ?) AND member.id = ?', [$id_membre,$id_membre] );

	  	$this->view( 'paypal/index', ['commandes' => $commandes, 'membre' => $membre[0]] );
	}
//VALIDATION DE LA COMMANDE
	function validation(){
		$this->view( 'paypal/validation');
		echo '<section class="page-inner"><header class="page-header d-flex flex-column"><h1>Validation de commande</h1></header><div class="container paypal-validation"><h3>Merci pour votre commande</h3><p>Elle a été validée. Nous reviendrons vers vous par email dès qu’elle sera expédiée.</p><p>À bientôt sur obiwash.fr</p><h3>Toute l’équipe d’Obiwash©</h3><a href="/" class="link-come-back"><button>Retour à la boutique</button></a></div></section>';
		require($_SERVER['DOCUMENT_ROOT'].'/app/views/include/footer.php');
	}
//CREATION DU PAYMENT
	function paypal_create_payment() {
        $id_membre = $_SESSION['Auth']['id'];

        $commande =  DB::select( 'SELECT * FROM order_member, member WHERE
        order_member.id = (SELECT MAX(id) FROM order_member WHERE ref_Member = ?) AND member.id = ?', [$id_membre,$id_membre] );

		$created_at = date_create( $commande[0]['date_commande'] );
		$ref_member = $commande[0]['ref_Member'];
		$created_at = date_format( $created_at, 'dmy' );
		$idCommande = $commande[0][0];
		$prix_total = $commande[0]['price'];

		if( $idCommande <= 9 ){
			$ref_commande = 'OBI' . $ref_member . '-' . $created_at . '-00000' . $idCommande;
		}
		elseif( $idCommande <= 99 ){
			$ref_commande = 'OBI' . $ref_member . '-' . $created_at . '-00000' . $idCommande;
		}
		elseif( $idCommande <= 999 ){
			$ref_commande = 'OBI' . $ref_member . '-' . $created_at . '-00000' . $idCommande;
		}
		elseif( $idCommande <= 9999 ){
			$ref_commande = 'OBI' . $ref_member . '-' . $created_at . '-00000' . $idCommande;
		}
		elseif( $idCommande <= 99999 ){
			$ref_commande = 'OBI' . $ref_member . '-' . $created_at . '-00000' . $idCommande;
		}
		elseif( $idCommande <= 999999 ){
			$ref_commande = 'OBI' . $ref_member . '-' . $created_at . '-00000' . $idCommande;
		}

        $ids_products = DB::select( 'SELECT ref_Products FROM order_member WHERE (ref_Member = ?) ORDER BY id DESC LIMIT 1', [$id_membre] );
        $produits = DB::select( 'SELECT * FROM products WHERE id IN ('.$ids_products[0]['ref_Products'].')' );
        $qty_products = DB::select( 'SELECT ref_Amount FROM order_member WHERE (ref_Member = ?) ORDER BY id DESC LIMIT 1', [$id_membre] );
        $quantites = explode(',', $qty_products[0]['ref_Amount']);

        $success = 0;
        $msg = "Une erreur est survenue, merci de bien vouloir réessayer ultérieurement...";
        $paypal_response = [];

        $payer = new PayPalPayment();
        $payer->setSandboxMode(1); // On active le mode Sandbox
        $payer->setClientID("AYDTl3XFHXYSsICGci_nucydNmCNnm1kAp5-ps0SJny4QG4KOSmYOzHpKr8UYVkgkgOWp_C1I91wFZxM"); // On indique sont Client ID
        $payer->setSecret("EInMj0UU7AgAF7cQsA_if5_-f4F3zhFaGOW9bPymDxHkqR3AuMbFgHCTOxo8i5xg-UV4fSwz_3FZBMCj"); // On indique son Secret

		$payment_data = [
		   "intent" => "sale",
		   "redirect_urls" => [
		      "return_url" => "https://commandes.obiwash.gribdev.eu",
		      "cancel_url" => "https://commandes.obiwash.gribdev.eu"
		   ],
		   "payer" => [
		      "payment_method" => "paypal"
		   ],
		   "transactions" => [
		      [
		         "amount" => [
		            "total" => $prix_total, // Prix total de la transaction, ici le prix de notre item
		            "currency" => "EUR" // USD, CAD, etc.
		         ],
		         "description" => "Réf. commande : " . $ref_commande
		      ]
		   ]
		];

        $paypal_response = $payer->createPayment($payment_data);
        $paypal_response = json_decode($paypal_response);

        if (!empty($paypal_response->id)) {
            $insert_ok = DB::insert( 'insert into paiements (payment_id, payment_status, payment_amount, payment_currency, payment_date) values (:payment_id, :payment_status, :payment_amount, :payment_currency, :payment_date)', [
                'payment_id' => htmlspecialchars($paypal_response->id),
                'payment_status' => htmlspecialchars($paypal_response->state),
                'payment_amount' => htmlspecialchars($paypal_response->transactions[0]->amount->total),
                'payment_currency' => htmlspecialchars($paypal_response->transactions[0]->amount->currency),
                'payment_date' => htmlspecialchars($paypal_response->create_time)
            ] );

           if ($insert_ok) {
              $success = 1;
              $msg = "";
           }
        } else {
           $msg = "Une erreur est survenue durant la communication avec les serveurs de PayPal. Merci de bien vouloir réessayer ultérieurement.";
        }

        echo json_encode(["success" => $success, "msg" => $msg, "paypal_response" => $paypal_response]);
	}
//EXECUTION DU PAYMENT
	function paypal_execute_payment() {
        $success = 0;
        $msg = "Une erreur est survenue, merci de bien vouloir réessayer ultérieurement...";
        $paypal_response = [];

        if (!empty($_POST['paymentID']) AND !empty($_POST['payerID'])) {
           $paymentID = htmlspecialchars($_POST['paymentID']);
           $payerID = htmlspecialchars($_POST['payerID']);

           $payer = new PayPalPayment();
           $payer->setSandboxMode(1); // On active le mode Sandbox
           $payer->setClientID("AYDTl3XFHXYSsICGci_nucydNmCNnm1kAp5-ps0SJny4QG4KOSmYOzHpKr8UYVkgkgOWp_C1I91wFZxM"); // On indique sont Client ID
           $payer->setSecret("EInMj0UU7AgAF7cQsA_if5_-f4F3zhFaGOW9bPymDxHkqR3AuMbFgHCTOxo8i5xg-UV4fSwz_3FZBMCj"); // On indique son Secret

           $payment = DB::select( 'SELECT * FROM paiements WHERE payment_id = ?', [$paymentID] );

           if ($payment) {
              $paypal_response = $payer->executePayment($paymentID, $payerID);
              $paypal_response = json_decode($paypal_response);

              DB::update( 'update paiements set payment_status = :payment_status, payment_date = :payment_date, payer_email = :payer_email where payment_id = :payment_id', [
                  'payment_status' => htmlspecialchars( $paypal_response->state ),
                  'payment_date' => htmlspecialchars($paypal_response->create_time),
                  'payer_email' => htmlspecialchars( $paypal_response->payer->payer_info->email ),
                  'payment_id' => htmlspecialchars( $paymentID )
              ] );

              if ($paypal_response->state == "approved") {
                 $success = 1;
                 $msg = "";
              } else {
                 $msg = "Une erreur est survenue durant l'approbation de votre paiement. Merci de réessayer ultérieurement ou contacter un administrateur du site.";
              }
           } else {
              $msg = "Votre paiement n'a pas été trouvé dans notre base de données. Merci de réessayer ultérieurement ou contacter un administrateur du site. (Votre compte PayPal n'a pas été débité)";
           }
        }

        echo json_encode(["success" => $success, "msg" => $msg, "paypal_response" => $paypal_response]);
	}
}
