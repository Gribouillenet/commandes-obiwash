<div class="container-panier container">
    <?php if ($data['paniers']) { ?>
    <form class="form-enter bootstrap-table table-responsive row" action="" method="post" class="p-y-3 p-x-2" enctype="multipart/form-data" novalidate>
        <table class="table table-striped col-12">
            <thead class="table-header">
                <th scope="col">Description</th>
                <th scope="col">Prix unitaire</th>
                <th class="col-1" scope="col">Qté</th>
                <th class="col-1" scope="col">Total</th>
                <th class="col-1" scope="col"></th>
            </thead>
            <tbody>
                <?php foreach ($data['paniers'] as $key => $produit) :
                            $id = $produit['id'];
                            $image = $produit['image'];
                            $title = $produit['title'];
                            $title_link = str_replace(' ', '_', $title);
                            $title_link = htmlentities($title_link, ENT_NOQUOTES, 'UTF-8');
                            $title_link = preg_replace('#&([A-za-z])(?:uml|circ|tilde|acute|grave|cedil|ring);#', '\1', $title_link);
                            $title_link = preg_replace('#&([A-za-z]{2})(?:lig);#', '\1', $title_link);
                            $title_link = preg_replace('#&[^;]+;#', '', $title_link);
                            $title_link = strtolower($title_link);
                            $description = $produit['description'];
                            $price =  $produit['price'];
                            $stock =  $produit['stock'];

                            $url = "https://".$_SERVER['SERVER_NAME'];
                            $root = $_SERVER['DOCUMENT_ROOT']."/app/views/include";
                            $adresse = "http://".$_SERVER['SERVER_NAME'];
                            $_SESSION['Auth']['adresse'] = $adresse;
                        ?>

                <tr class="item-bucket-product item-bucket-product-<?= $id ?>">
                    <td class="recap-panier-description d-flex align-items-center"><?php include('inc/image-product.php');?><p><b><?= $title ?></b><!-- <br><?= $description ?> -->
                        </p>
                    </td>

                    <td class="price"><?= number_format($price, 2, ',', ' ') ?> €</td>
                    <td class="quantity"><input type="number" name="Panier[quantity][<?= $id ?>]" value="<?= $_SESSION['Panier'][$id] ?>" min="1" max='<?= $stock ?>'></td>
                    <td class="subtotal"><?= number_format($price * $_SESSION['Panier'][$id], 2, ',', ' ') ?> €</td>
                    <td class="action">
                        <a class="link-delete delPanier" href="/produits/suppression/<?= $id ?>" title="Supprimer"><i class="ti-trash"></i></a>
                        </span>
                    </td>
                </tr>
                <?php endforeach;?>

                <tr>
                    <td class="items-delivery-total" colspan="2">
                        <p class="total-ht">Sous-total</p>
                        <p class="frais-port">Livraison</p>
                        <p class="total-ttc"><strong>Prix Total</strong></p>
                    </td>
                    <td class="items-delivery-total-final" colspan="3">
                        <p class="total-ht"><?= number_format($panier->total(), 2, ',', ' ') ?> €</p>
                        <?php
                                foreach ($data['livraisons'] as $key => $livraison) :
                                    $type = $livraison['type'];
                                    if ($type == 'Retrait sur place') {
                                        $tarif_1 =  $livraison['tarif_1'];
                                    }
                                endforeach;
                                ?>
                        <p class="frais-port">Gratuite *</p>
                        <p class="total-ttc"><strong><?= number_format($panier->total() + $tarif_1, 2, ',', ' ') ?> €</strong></p>
                    </td>
                </tr>
                <!---
						<tr>
							<td colspan="4">
								<select name="Livraison" size="1">
									<?php
                                    foreach ($data['livraisons'] as $key => $livraison) :
                                        $id = $livraison['id'];
                                        $type = $livraison['type'];
                                        if ($type == $_SESSION['Livraison']) {
                                            $tarif_1 =  $livraison['tarif_1'];
                                            $tarif_2 =  $livraison['tarif_2'];
                                            $tarif_3 =  $livraison['tarif_3'];
                                        }
                                    ?>
									<option value="<?= $type ?>"><?= $type ?></option>
									<?php endforeach;?>
								</select>

								<INPUT type= "radio" name="tarif" value="tarif_1" checked> <?= $tarif_1 ?> €
								<INPUT type= "radio" name="tarif" value="tarif_2"> <?= $tarif_2 ?> €
								<INPUT type= "radio" name="tarif" value="tarif_3"> <?= $tarif_3 ?> €
							</td>
						</tr>
--->
            </tbody>
        </table>
        <div class="container-btn-action-bucket container-fluid d-flex justify-content-end mt-3">
            <button><input class="button recalculatePanier btn btn-record" type="submit" value="Mettre à jour le panier" /><i class="ti-reload"></i></button>
            <!-- 					<a class="button delPanier d-inline-block" href="/produits/suppressionAll" title="Supprimer tous les produits du panier">Supprimer le panier</a> -->
        </div>
    </form>
    <em class="be-carreful d-block"><b>ATTENTION !</b> Si vous procédez à une modification de quantité.<br />Merci de mettre à jour le panier avec le bouton prévu à cet effet pour qu’elle soit prise en compte.</em>
    <em class="d-block">*Les frais de livraison seront recalculés suivant le type de livraison choisi lors de la finalisation de votre commande.<br>Frais de port pour la France métropolitaine. Pour l’international et les DOM/TOM, les frais seront calculés lorsque vous saisirez l’adresse de livraison.</em>
    <div class="container-btn-ordered d-flex flex-column flex-md-row justify-content-center justify-content-md-between justify-content-lg-center">
        <?php include($_SERVER['DOCUMENT_ROOT'].'/app/views/include/inc/button-come-back.php'); ?>
        <?php include($_SERVER['DOCUMENT_ROOT'].'/app/views/include/inc/button-ordered.php'); ?>
    </div>

    <?php } else { ?>

    <h4 style="color:red;">Votre panier est vide !</h4>
    <div class="container-btn-view-bucket container-fluid d-flex justify-content-center mt-3">
        <?php include($_SERVER['DOCUMENT_ROOT'].'/app/views/include/inc/button-come-back.php'); ?>
    </div>

    <?php } ?>

</div>