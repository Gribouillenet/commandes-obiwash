<?php require($_SERVER['DOCUMENT_ROOT'].'/app/views/include/header.php'); ?>
<section class="page-inner clearfix">
	<header class="page-header d-flex flex-column justify-content-center align-items-center">
    	<h1 class="text-xs-center">Fiche client de <?php echo $data['membre']['first_name'].' '.$data['membre']['name'];?></h1>
	</header>
	<div class="container container-edit-user">
    <h2>Ici, vous pouvez modifier vos informations personnelles. N'oubliez pas d'enregistrer !</h2>

  	<?php if ( isset( $data['erreur'] ) ) { ?>
	    <div class="alert alert-danger alert-on alert-generale"><?= $data['erreur']['champ_obligatoire'] ?></div>
	<?php } else { ?>
    	<span class="champ-obligatoire d-inline-block">* Champs Obligatoires</span>
  	<?php } ?>

	  <form class="form-label form-fiche-client form-enter" action="" method="post" class="p-y-3 p-x-2" enctype="multipart/form-data" novalidate>
		<div class="edit-user">
			<label for="login">Nom d'utilisateur*</label>
			<input type="text" name="login" class="form-control form-obligatoire" placeholder="Nom d'utilisateur" value="<?php echo $data['membre']['login']; ?>">
			<?php if ( isset( $data['erreur']['login'] ) ) : ?>
	            <div class="alert alert-danger alert-form"><?= $data['erreur']['login'] ?></div>
	        <?php endif; ?>
		</div>
		
		
		
		<div class="edit-user">	
			<label for="password">Mot de passe*</label>
	        <input type="password" name="password" class="form-control form-obligatoire" placeholder="Mot de passe" value="<?= $data['password'] ?>">
	        <?php if ( isset( $data['erreur']['password'] ) ) : ?>
	            <div class="alert alert-danger alert-form"><?= $data['erreur']['password'] ?></div>
	        <?php endif; ?>
		</div>
	
		<!-- <div class="edit-user">
			<label for="confirm_password">confirmation du mot de passe</label>
			<input type="password" name="confirm_password" class="form-control form-obligatoire" placeholder="Confirmation mot de passe" value="<?//= $data['password'] ?>">
			<?php// if ( isset( $data['erreur']['confirm_password'] ) ) : ?>
				<div class="alert alert-danger alert-form"><?php// echo $data['erreur']['confirm_password']; ?></div>
			<?php// endif; ?>
		</div> -->

		<div class="edit-user">
		<label for="name">Nom*</label>
		<input type="text" name="name" class="form-control form-obligatoire" placeholder="Nom" value="<?php echo $data['membre']['name']; ?>">
		<?php if ( isset( $data['erreur']['name'] ) ) : ?>
            <div class="alert alert-danger alert-form"><?= $data['erreur']['name'] ?></div>
        <?php endif; ?>
		</div>
		<div class="edit-user">
		<label for="first_name">Prénom*</label>
		<input type="text" name="first_name" class="form-control form-obligatoire" placeholder="Prénom" value="<?php echo $data['membre']['first_name']; ?>">
		<?php if ( isset( $data['erreur']['first_name'] ) ) : ?>
            <div class="alert alert-danger alert-form"><?= $data['erreur']['first_name'] ?></div>
        <?php endif; ?>
		</div>
		<div class="edit-user">
		<label for="company_service">Société/Service</label>
		<input type="text" name="company_service" class="form-control form-obligatoire" placeholder="Société/Service" value="<?php echo $data['membre']['company_service']; ?>">
		</div>
		<div class="edit-user">
		<label for="address">Adresse*</label>
		<input type="text" name="address" class="form-control form-obligatoire" placeholder="Adresse" value="<?php echo $data['membre']['address']; ?>">
		<?php if ( isset( $data['erreur']['address'] ) ) : ?>
            <div class="alert alert-danger alert-form"><?= $data['erreur']['address'] ?></div>
        <?php endif; ?>
		</div>
		<div class="edit-user">
		<label for="address_supplement">Complément d'adresse</label>
		<input type="text" name="address_supplement" class="form-control form-obligatoire" placeholder="Complément d'adresse" value="<?php echo $data['membre']['address_supplement']; ?>">
		</div>
		<div class="edit-user">
		<label for="code_postal">Code postal*</label>
		<input type="text" name="code_postal" class="form-control form-obligatoire" placeholder="Code postal" maxlength="5" value="<?php echo $data['membre']['code_postal']; ?>">
		<?php if ( isset( $data['erreur']['code_postal'] ) ) : ?>
            <div class="alert alert-danger alert-form"><?= $data['erreur']['code_postal'] ?></div>
        <?php endif; ?>
		</div>
		<div class="edit-user">
		<label for="city">Ville*</label>
		<input type="text" name="city" class="form-control form-obligatoire" placeholder="Ville" value="<?php echo $data['membre']['city']; ?>">
		<?php if ( isset( $data['erreur']['city'] ) ) : ?>
            <div class="alert alert-danger alert-form"><?= $data['erreur']['city'] ?></div>
        <?php endif; ?>
		</div>
		<div class="edit-user">
		<div class="form-group">
			<label class="d-inline-block" for="sel1">Sélectionnez dans la liste :</label>
			<select id="sel1" class="form-control form-obligatoire" name="country">
				<option value="" disabled selected>Choisissez votre pays</option>
				<optgroup label="France Métropolitaine">
					<option value="France" <?php if ( $data['membre']['country'] == 'France' ) echo "selected" ?>>
						France
					</option>
				</optgroup>
				<optgroup label="Outre-mer Zone 1">
					<option value="Réunion" <?php if ( $data['membre']['country'] == 'Réunion' ) echo "selected" ?>>
						Réunion
					</option>
					<option value="Guadeloupe" <?php if ( $data['membre']['country'] == 'Guadeloupe' ) echo "selected" ?>>
						Guadeloupe
					</option>
					<option value="Martinique" <?php if ( $data['membre']['country'] == 'Martinique' ) echo "selected" ?>>
						Martinique
					</option>
					<option value="Mayotte" <?php if ( $data['membre']['country'] == 'Mayotte' ) echo "selected" ?>>
						Mayotte
					</option>
					<option value="Guyane" <?php if ( $data['membre']['country'] == 'Guyane' ) echo "selected" ?>>
						Guyane
					</option>
					<option value="Saint-Pierre et Miquelon" <?php if ( $data['membre']['country'] == 'Saint-Pierre et Miquelon' ) echo "selected" ?>>
						Saint-Pierre et Miquelon
					</option>
					<option value="Saint-Martin" <?php if ( $data['membre']['country'] == 'Saint-Martin' ) echo "selected" ?>>
						Saint-Martin
					</option>
					<option value="Saint-Barthélemy" <?php if ( $data['membre']['country'] == 'Saint-Barthélemy' ) echo "selected" ?>>
						Saint-Barthélemy
					</option>
				</optgroup>
				<optgroup label="Outre-mer Zone 2">
					<option value="Polynésie Française" <?php if ( $data['membre']['country'] == 'Polynésie Française' ) echo "selected" ?>>
						Polynésie Française
					</option>
					<option value="Wallis et Futuna" <?php if ( $data['membre']['country'] == 'Wallis et Futuna' ) echo "selected" ?>>
						Wallis et Futuna
					</option>
					<option value="Nouvelle Calédonie" <?php if ( $data['membre']['country'] == 'Nouvelle Calédonie' ) echo "selected" ?>>
						Nouvelle Calédonie
					</option>
					<option value="TAAF" <?php if ( $data['membre']['country'] == 'TAAF' ) echo "selected" ?>>
						TAAF
					</option>
				</optgroup>
			</select>
		</div>
		<?php if ( isset( $data['erreur']['country'] ) ) : ?>
            <div class="alert alert-danger alert-form"><?= $data['erreur']['country'] ?></div>
        <?php endif; ?>
		</div>
		<div class="edit-user">
		<label for="mail_member">Email du client*</label>
		<input type="text" name="mail_member" class="form-control form-obligatoire" placeholder="Email du client" value="<?php echo $data['membre']['mail_member']; ?>">
		<?php if ( isset( $data['erreur']['mail_member'] ) ) : ?>
            <div class="alert alert-danger alert-form"><?= $data['erreur']['mail_member'] ?></div>
        <?php endif; ?>
		</div>
		<div class="edit-user">
		<label for="tel">Téléphone*</label>
		<input type="text" name="tel" class="form-control form-obligatoire" placeholder="Téléphone" maxlength="14" value="<?php echo $data['membre']['tel']; ?>">
        <?php if ( isset( $data['erreur']['tel'] ) ) : ?>
            <div class="alert alert-danger alert-form"><?= $data['erreur']['tel'] ?></div>
        <?php endif; ?>
		</div>
		<div class="button btn btn-record d-inline-block">
			<i class="ti-save"></i><input type="submit" value="Enregistrer">
		</div>
		<div class="button btn-comeback-store d-inline-block ">
			<i class="ti-arrow-left"></i><a href="/membre/compte">Retour</a>
		</div>
    	
		</form>
		</div>

</section>
<?php require($_SERVER['DOCUMENT_ROOT'].'/app/views/include/footer.php'); ?>
