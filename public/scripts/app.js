$(function(){
	//GESTION ORIENTATION ET TAILLE ÉCRAN
	$( window ).on("orientationchange", function(){
			  location.reload();
	});
	$( window ).resize( function(){
			  location.reload();
	});
	
//GESTION MENU MON COMPTE
	if($('.nav-item-widget-compte').length){
		$('.nav-link-widget-compte').click(function(e){
			e.preventDefault();
			$('.compte-sub-menu').toggleClass('sub-menu-on');
		});
	}
	if($('#container-recap-panier').length){
		$('.nav-link-widget-panier').click(function(e){
			e.preventDefault();
			$('#container-recap-panier').toggleClass('recap-panier-on');
			$('#page-inner').toggleClass('blur');
		});
		$('.recap-close').click(function(){
			$('#container-recap-panier').removeClass('recap-panier-on');
			$('#page-inner').removeClass('blur');
		})
	}

//GESTION DE L'AJOUT DE PANIER
	if($('.addPanier').length){
		$('.addPanier').click(function(event){
			event.preventDefault();
			$.get($(this).attr('href'), {}, function(data){
				if(data.error){
					alert(data.message);
				}else{
					alertify.confirm(
					    function(){
							location.href = '/panier/index';
					    },
					    function(){
						    alertify.error('Consulter votre panier');
						    location.href = '/panier/index';
							$('#total').empty().append(data.total);
							$('#count').empty().append(data.count);
					    },
					    function(){
							location.reload();
						}
					).set({
						labels:{
							ok:'Consulter votre panier',
							cancel: 'Retour à la boutique'
						},
						padding: true
					}).setHeader('<i class="ti-shopping-cart-full"></i><p>' + data.message + '</p>');
				}
			}, 'json');
			return false;
		});
	}
//MISE À JOUR DU RÉCAP DE PANIER
	if( $('.arlertify').length ){
		$('.ajs-cancel').click(function(){
			 location.reload();
		});
		$('.ajs-buttons').addClass('d-flex');
	}
//GESTION DE SUPPRESSION DE PANIER
	if($('.delPanier').length){
		$('.delPanier').click(function(event){
			event.preventDefault();
			$.get($(this).attr('href'), {}, function(data){
				if(data.error){
					alert(data.message);
				}else{
					alertify.confirm(
					    function(){
							location.href = '/';
					    },
					    function(){
						    alertify.error('Consulter votre panier');
							location.href = '/panier/index';
					    },
					     function(){
							location.reload();
						}
					).set({
						labels:{
							ok:'Consulter votre panier',
							cancel: 'Retour à la boutique'
						},
						padding: true
					}).setHeader('<i class="ti-trash"></i><p>' + data.message + '</p>');
				}
			}, 'json');
			return false;
		});
	}

//GESTION DE SUPPRESSION LA MODIFICATION DE COMMANDE
	if($('.delModif').length){
		$('.delModif').click(function(event){
			event.preventDefault();
			$.get($(this).attr('href'), {}, function(data){
				if(data.error){
					location.href = '/panier/index';
				}else{
					location.href = '';
				}
			}, 'json');
			return false;
		});
	}

//GESTION DES UPLOADS
	if( $('#input-file').length ){
		fileInput  = document.querySelector( ".input-file" );
	    the_return = document.querySelector(".file-return");

		fileInput.addEventListener( "change", function( event ) {
	    the_return.innerHTML = this.value;
	    });
	}

    if( $('#delete-file').length ){
        $('.delete-file').click(function(){
            $('.file-return').empty();
            obj = document.getElementById("input-file");
            obj.value="";
		});
    }

//GESTION PAGGING
	if($(window).width() > 1200 ){
		if($('#pagger-list-commandes').length){
				$('#pagger-list-commandes').easyPaginate({
					elementsPerPage :6,
					effect : 'climb',
					firstButton: false,
					lastButton: false,
					prevButton: false,
					nextButton: false
				});
				$('.easyPaginateNav').addClass('d-flex justify-content-center');
			}
	}
//GESTION PAGGING TABLETTES
	if($(window).width() <= 1100 ){
		if($('#pagger-list-commandes').length){
			$('#pagger-list-commandes').easyPaginate({
				elementsPerPage :5,
				effect : 'climb',
				firstButton: false,
				lastButton: false,
				prevButton: true,
				nextButton: true

			});
			$('.easyPaginateNav').addClass('d-flex justify-content-center');
		}
	}
//GESTION PAGGING SMARTPHONES
	if($(window).width() <= 700 ){
		if($('#pagger-list-commandes').length){
			$('#pagger-list-commandes').easyPaginate({
				elementsPerPage :3,
				effect : 'climb',
				firstButton: false,
				lastButton: false,
				prevButton: true,
				nextButton: true

			});
			$('.easyPaginateNav').addClass('d-flex justify-content-center');
		}
	}
	
//GESTION DES ERREURS
	if($('.alert-form').length){
		$('.form-obligatoire').click(function(){
			$('.alert-form').remove();
		});
	}

//SUPPRESSION DE LA TOUCHE ENTREE
    if($('#form-sign-in').length){
        var formEnter = document.querySelectorAll('#form-sign-in');
        for (var i = 0; i < formEnter.length; i++) {
            formEnter[i].addEventListener("keypress", function refuserToucheEntree(event) {
                // Compatibilité IE / Firefox
                if(!event && window.event) {
                    event = window.event;
                }
                // IE
                if(event.keyCode == 13) {
                    event.returnValue = false;
                    event.cancelBubble = true;
                }
                // DOM
                if(event.which == 13) {
                    event.preventDefault();
                    event.stopPropagation();
                }
            });
        }
    }

//MASQUES DES BOUTONS HISTORIQUE
    if($('#link-historic-commande').length){
        $("#link-historic-commande").on("click", function(e) {
          e.preventDefault();
            $(".link-hist-button").toggleClass('d-none');

        });
        /*
		$('#link-historic-commande').click(function(){
			var element = document.getElementsByClassName("link-hist-button");
            element.classList.toggle("mystyle");
		});
        */
	}
//CASE ACCEPTANCE
	if($('.input-validation').length){

		$('body').on('click', 'input[name=acceptance]', function(){

			if($('input[name=acceptance]').is(':checked') ){
				$('.input-record').removeClass('submit-off').attr('disabled', false);

			}else{
				$('.input-record').addClass('submit-off').attr('disabled', true);
			}
		});
	}
//PAYPAL
	if($('#bouton-paypal').length){
		paypal.Button.render({
			env: 'sandbox', // Ou 'production',
			commit: true, // Affiche le bouton  "Payer maintenant"
			style: {
				color: 'gold', // ou 'blue', 'silver', 'black'
				size: 'responsive' // ou 'small', 'medium', 'large'
				// Autres options de style disponibles ici : https://developer.paypal.com/docs/integration/direct/express-checkout/integration-jsv4/customize-button/
			},
			payment: function() {
				// On crée une variable contenant le chemin vers notre script PHP côté serveur qui se chargera de créer le paiement
				var CREATE_URL = '/paypal/paypal_create_payment';
				// On exécute notre requête pour créer le paiement
				return paypal.request.post(CREATE_URL).then(function(data) { // Notre script PHP renvoie un certain nombre d'informations en JSON (vous savez, grâce à notre echo json_encode(...) dans notre script PHP !) qui seront récupérées ici dans la variable "data"
					if (data.success) { // Si success est vrai (<=> 1), on peut renvoyer l'id du paiement généré par PayPal et stocké dans notre data.paypal_reponse (notre script en aura besoin pour poursuivre le processus de paiement)
						return data.paypal_response.id;
					} else { // Sinon, il y a eu une erreur quelque part. On affiche donc à l'utilisateur notre message d'erreur généré côté serveur et passé dans le paramètre data.msg, puis on retourne false, ce qui aura pour conséquence de stopper net le processus de paiement.
						alert(data.msg);
						return false;
					}
				});
			},
			onAuthorize: function(data, actions) {
				// On indique le chemin vers notre script PHP qui se chargera d'exécuter le paiement (appelé après approbation de l'utilisateur côté client).
				var EXECUTE_URL = '/paypal/paypal_execute_payment';
				// On met en place les données à envoyer à notre script côté serveur
				// Ici, c'est PayPal qui se charge de remplir le paramètre data avec les informations importantes :
				// - paymentID est l'id du paiement que nous avions précédemment demandé à PayPal de générer (côté serveur) et que nous avions ensuite retourné dans notre fonction "payment"
				// - payerID est l'id PayPal de notre client
				// Ce couple de données nous permettra, une fois envoyé côté serveur, d'exécuter effectivement le paiement (et donc de recevoir le montant du paiement sur notre compte PayPal).
				// Attention : ces données étant fournies par PayPal, leur nom ne peut pas être modifié ("paymentID" et "payerID").
				var data = {
					paymentID: data.paymentID,
					payerID: data.payerID
				};
				// On envoie la requête à notre script côté serveur
				return paypal.request.post(EXECUTE_URL, data).then(function (data) { // Notre script renverra une réponse (du JSON), à nouveau stockée dans le paramètre "data"
					if (data.success) { // Si le paiement a bien été validé, on peut par exemple rediriger l'utilisateur vers une nouvelle page, ou encore lui afficher un message indiquant que son paiement a bien été pris en compte, etc.
						//window.location.replace("https://commandes.obiwash.gribdev.eu/paypal/validation");
						//alert("Paiement approuvé ! Merci !");
						location.href = 'https://commandes.obiwash.gribdev.eu/paypal/validation';
					} else {
						// Sinon, si "success" n'est pas vrai, cela signifie que l'exécution du paiement a échoué. On peut donc afficher notre message d'erreur créé côté serveur et stocké dans "data.msg".
						//location.href = '';
						alert(data.msg);
					}
				});
			},
			onCancel: function(data, actions) {
				//alert("Paiement annulé : vous avez fermé la fenêtre de paiement.");
				location.href = '';
			},
			onError: function(err) {
				alert("Paiement annulé : une erreur est survenue. Merci de bien vouloir réessayer ultérieurement.");
			}
		}, '#bouton-paypal');
	}
	
//AJOUT DE CLASSES AUX MÉDIAS SOCIAUX
	if ( $('#social-media').length ) {
		var link = $('.social-media li');
		$(link).addClass('d-inline-block');
		$(link).each(function(i){
			var social = $(this).text().toLowerCase();
			$(this).addClass(social);		
				
		});
	}
	if ($('.facebook').length){
	$('.facebook a').addClass('btn-widget d-flex align-items-center align-items-start justify-content-center fab fa-facebook-f');
	}
	if ($('.pinterest').length){
	$('.pinterest a').addClass('btn-widget widget-social d-flex align-items-center align-items-start justify-content-center fab fa-pinterest');
	}
	if ($('.twitter').length){
	$('.twitter a').addClass('btn-widget widget-social d-flex align-items-center align-items-start justify-content-center fab fa-twitter');
	}
	if ($('.linkedin').length){
	$('.linkedin a').addClass('btn-widget d-flex align-items-center align-items-start justify-content-center fab fa-linkedin');
	}
	if ($('.youtube').length){
	$('.youtube a').addClass('btn-widget d-flex align-items-center align-items-start justify-content-center fab fa-youtube');
	}
	if ($('.instagram').length){
	$('.instagram a').addClass('btn-widget d-flex align-items-center align-items-start justify-content-center fab fa-instagram');
	}
	if ($('.google').length){
	$('.google a').addClass('btn-widget d-flex align-items-center align-items-start justify-content-center fab fa-google');
	}

//BUTTON UP
	if($('.return-top').length ){
		$('.return-top').click(function(){
			$('html,body').animate({scrollTop: 0}, 'slow');
		});
	}
	
// BANDEAUX ICONS TUNNEL DE COMMANDE
	if( $('#bandeau-icons').length ){
		var urlcourante = window.location.origin;
		var idcommande = $('.bandeau-icons').attr('data-id-commande');
		
		// console.log(idcommande);
		// console.log(window.location.href == urlcourante + '/livraison/informations');
	
		if ( window.location.href == urlcourante + '/livraison/informations' || window.location.href == urlcourante + '/commandes/modifier/' + idcommande ){
			$('#bandeau-edit').addClass("active");
			$('#bandeau-money').removeClass("active");
			$('#bandeau-check').removeClass("active");
		}
		if ( window.location.href ==urlcourante + '/commandes' ){
			$('#bandeau-edit').removeClass("active");
			$('#bandeau-money').addClass("active");
			$('#bandeau-check').removeClass("active");
		}
		if ( window.location.href == urlcourante + '/commandes/validation' ){
			$('#bandeau-edit').removeClass("active");
			$('#bandeau-money').removeClass("active");
			$('#bandeau-check').addClass("active");
		}
	}
	
	//RADIO CHECK BORDER BLUE
	if( $('.form-fiche-client').length ){
		console.log('ok');
		$("input[type='radio']:checked").parent("li").addClass("check");
		$('input[type=radio]').change(function() {
			$("input[type=radio]").parent("li").removeClass("check");
			$("input[type=radio]:checked").parent("li").addClass("check");
		});
	}
	//MON COMPTE ANIMATION ARROW
	$( ".nav-link-widget-compte" ).click(function() {
		if (  $(".arrow-down").css( "transform" ) == 'none' ){
			$(".arrow-down").css("transform","rotate(-180deg)");
		} else {
			$(".arrow-down").css("transform","" );
		}
	});

	//NAVIGATION PRINCIPALE
	if($('#menu-navigation-principale').length){
		$('#menu-navigation-principale .menu-item').addClass('d-flex justify-content-center flex-column flex-md-row');
		$('#menu-navigation-principale .menu-item a').addClass('d-flex align-items-center justify-content-center');
	}
		
	//NAVIGATION RESPONSIVE
	if( $( window ).width() < 768 ){
		
		if( $('.site-header').length ){
			
			var nav = $('.primary-navigation').html();
			
			$('.container-navigation .primary-navigation').remove();
			
			$('.site-header').append('<nav class="primary-navigation">' + nav + '</nav>');	
			
			$('.menu-toggle-mobile').on('click', function(event){
				event.preventDefault();
				$('.menu-toggle-mobile').toggleClass('on');
				$('.primary-navigation').toggleClass('on');
			});
		}
		if($('#menu-navigation-principale').length){
			$('.notre-projet a:first').removeClass('d-flex').addClass('d-none');
			$('.tous-les-articles').removeClass('d-flex').addClass('d-none');
		}
	}
		
	if( $( window ).width() >= 768 ){
		
		//MENU DROPDOWN
		if($('.menu-item-has-children').length){
			$('.menu-item-has-children a').addClass('ti-angle-down submenu-ctrl');
			$('.sub-menu a').removeClass('ti-angle-down submenu-ctrl').addClass('sub-link');	
			
			$('.menu-item-has-children').click(function(){
				$(this).toggleClass('on').prev().removeClass('on');
			});
			$('.submenu-ctrl').click( function(e){
				e.preventDefault();
				// $(this).toggleClass('on').next().toggleClass('on');
			});		
			$('.sub-menu').mouseleave(function(){
				$('.menu-item-has-children').removeClass('on');
			});
			
		}
	}


	//EMPECHER VALIDATION COMMANDE SI RGPD PAS ACCEPTE
	if( $('#acceptance-checkbox').length ) {

		function change_status(is_checked, event = null) {
			validerCommandeButton.disabled = !is_checked;

			if(is_checked) {
				validerCommandeButton.style.opacity = 1;
				validerCommandeButton.style.cursor = "pointer";
			} else {
				if(event != null) event.preventDefault();
				validerCommandeButton.style.opacity = 0.5;
				validerCommandeButton.style.cursor = "cursor";
			}
		}

		const acceptanceCheckbox    = document.getElementById('acceptance-checkbox');
		const validerCommandeButton = document.getElementById('valider-commande-button');

		change_status(acceptanceCheckbox.checked);

		acceptanceCheckbox.addEventListener('change', function(event) {
			change_status(acceptanceCheckbox.checked, event);
		});
	}



});




