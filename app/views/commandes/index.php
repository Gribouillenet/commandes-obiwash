<?php require($_SERVER['DOCUMENT_ROOT'].'/app/views/include/header.php'); ?>
<section class="page-inner clearfix">
    <header class="page-header d-flex flex-column justify-content-center align-items-center">
        <h1 class="text-xs-center">Récapitulatif de commande</h1>
    </header>
    <?php require($_SERVER['DOCUMENT_ROOT'].'/app/views/include/bandeau-icons.php'); ?>

    <?php foreach ($data['commandes'] as $key => $commande) :

        $name = $commande['name'];
        $first_name = $commande['first_name'];
        $mail_member = $commande['mail_member'];
        $tel = $commande['tel'];
        $qty_products = explode(',', $commande['ref_Amount']);
        $tarif_1 = $commande['tarif_1'];
        $tarif_2 = $commande['tarif_2'];
        $tarif_3 = $commande['tarif_3'];
        $type_livraison = $commande['type'];
        $adresse_livraison_relais = $commande['address_relais_colis'];
        $mode_paiement = $commande['mode_paiement'];
        $created_at = $commande['date_commande'];
        $idCommande = $commande[0];
        $ref_member = $commande['ref_Member'];
        $tel = $commande['tel'];
        $mail = $commande['mail_member'];


        // var_dump($commande);


        if (!empty($idCommande)) {
            ?>



    <div class="recap-commande container-panier">
        <form action="commandes/validation" method="post" class="p-y-3 p-x-2 form-recap-commande bootstrap-table container" enctype="multipart/form-data" novalidate>
            <div class="row">

                <?php
            ob_start();

            if ($idCommande <= 9) {
                echo '<header class="header-cmd"><h4>RÉFÉRENCE DE COMMANDE <span>OBI' . $ref_member . '-' . $created_at . '-00000' .$idCommande . '</span></h4>';
            } elseif ($idCommande <= 99) {
                echo '<header class="header-cmd"><h4>RÉFÉRENCE DE COMMANDE <span>OBI' . $ref_member . '-' . $created_at . '-0000' . $idCommande . '</span></h4>' ;
            } elseif ($idCommande <= 999) {
                echo '<header class="header-cmd"><h4>RÉFÉRENCE DE COMMANDE <span>OBI' . $ref_member . '-' . $created_at . '-000' . $idCommande . '</span></h4>';
            } elseif ($idCommande <= 9999) {
                echo '<header class="header-cmd"><h4>RÉFÉRENCE DE COMMANDE <span>OBI' . $ref_member . '-' . $created_at . '-00' . $idCommande . '</span></h4>';
            } elseif ($idCommande <= 99999) {
                echo '<header class="header-cmd"><h4>RÉFÉRENCE DE COMMANDE <span>OBI' . $ref_member . '-' . $created_at . '-0' . $idCommande . '</span></h4>';
            } elseif ($idCommande <= 999999) {
                echo '<header class="header-cmd"><h4>RÉFÉRENCE DE COMMANDE <span>OBI' . $ref_member . '-' . $created_at . '-' . $idCommande . '</span></h4>';
            }
            echo '</header>';

            $ids = explode(',', $commande['ref_Products']);
            $poids = 0;
            $i = 0;
            foreach ($ids as $key => $id_produit) :
                foreach ($data['produits'] as $key => $produit) :
                    $id = $produit['id'];
            $title = $produit['title'];
            $poids = $produit['weight'];
            if ($id_produit == $id) {
                $total_poids += $poids * $qty_products[$i];
            }
            endforeach;
            $i += 1;
            endforeach;

            $nb_obiwash = 0;
            $total = 0;
            $i = 0;
            echo '<table class="table table-striped col-12">
            <thead class="table-header">
                    <th scope="col">Description</th>
                    <th scope="col">Prix unitaire</th>
                    <th class="col-1" scope="col">Qté</th>
                    <th class="col-1" scope="col">Total</th>
                </thead>
                <tbody>
                <tr class="item-bucket-product item-bucket-product-<?= $id ?>">';
            foreach ($data['produits'] as $produit) :
                $id = $produit['id'];
            $image = $produit['image'];
            $title = $produit['title'];
            $trimmed = trim($title, '®');
            $trimmed = strtolower($trimmed);
            $obi = 'obiwash';

            $description = $produit['description'];
            $price = $produit['price'];

            $adresse = "http://".$_SERVER['SERVER_NAME'];
            $_SESSION['Auth']['adresse'] = $adresse;

            if ($image) {
                echo '<td class="recap-panier-description d-flex align-items-center">
                    <div class="recap-panier-description d-flex align-items-center"><img src="' . $adresse . '/public/files/' . $image . '" title="Image '. $title .'" alt="Image '. $title .'" < />';
            }
            echo '<p>'. $description .'</p>
                    </div>
                </td>
                <td class="price">' . number_format($price, 2, ',', ' ') . '€</td>
                <td class="quantity">' . $qty_products[$i] . '</td>
                <td class="subtotal">' . number_format($price * $qty_products[$i], 2, ',', ' ') . '€</td>
                </tr></br>';

            if ($trimmed == $obi) {
                $nb_obiwash = $qty_products[$i];
            }

            $total += $price * $qty_products[$i];
            $i++;
            endforeach;

            $frais_port = 0;
            if ($total_poids >= 5) {
                $frais_port = $tarif_3;
            } else {
                if ($total_poids >= 2) {
                    $frais_port = $tarif_2;
                } else {
                    if ($total_poids > 0) {
                        $frais_port = $tarif_1;
                    }
                }
            }

            if ($mode_paiement == 'cb') {
                $mode_paiement = 'Carte Bancaire';
            } else {
                if ($mode_paiement == 'paypal') {
                    $mode_paiement = 'Paypal';
                    $total += 5;
                } else {
                    if ($mode_paiement == 'cheque') {
                        $mode_paiement = 'Chèque';
                    } else {
                        $mode_paiement = 'Virement Bancaire';
                    }
                }
            }

            echo '<tr>
                    <td class="items-delivery-total" colspan="2">
                        <p class="total-ht">Sous-total</p>
                        <p class="frais-port">Livraison</p>
                        <p class="total-ttc"><span><strong>Prix Total</strong></span></p>
                        <p class="mode-paiement">Mode de Paiement</p>
                    </td>
                    <td class="items-delivery-total-final" colspan="2">
                        <p class="total-ht">' . number_format($total, 2, ',', ' ') . ' €</p>
                        <p class="frais-port">' . $frais_port . ' €</p>
                        <p class="total-ttc"><strong>' . number_format($total + $frais_port, 2, ',', ' ') . ' €</strong></p>
                        <p class="mode-paiement">' . $mode_paiement . '</p>
                    </td>
                </tr>
                </tbody>
                </table>'; ?>
            </div>
            <div class="row">
                <div class="infos-cmd info-livraison col-12 col-md-6">
                    <h3>Livraison</h3>

                    <ul>
                        <li><?= $commande['name'].' '.$commande['first_name']?></li>
                        <?php if (!empty($commande['company_service'])) { ?>
                        <li>Société : <?= $commande['company_service']?></li>
                        <?php } ?>
                        <li><?= $commande['address']?></li>
                        <?php if (!empty($commande['address_supplement'])) { ?>
                        <li><?= $commande['address_supplement']?></li>
                        <?php } ?>
                        <li><?= $commande['code_postal'].' '.$commande['city']?></li>
                        <li><?= $commande['tel']?></li>
                        <li><?= $commande['mail_member']?></li>
                    </ul>

                </div>
                <div class="infos-cmd info-facturation col-12 col-md-6">
                    <h3>Facturation</h3>
                    <?php $facturation = $data['facturation'];

            // var_dump($facturation);

            if ($facturation != null) :
                   ?>
                    <ul>
                        <li><?= $facturation['name'].' '.$facturation['first_name']?></li>
                        <?php if (!empty($facturation['company_service'])) { ?>
                        <li>Société : <?= $facturation['company_service']?></li>
                        <?php } ?>
                        <li><?= $facturation['address']?></li>
                        <?php if (!empty($facturation['address_supplement'])) { ?>
                        <li><?= $facturation['address_supplement']?></li>
                        <?php } ?>
                        <li><?= $facturation['code_postal'].' '.$facturation['city']?></li>
                    </ul>

                    <?php endif; ?>
                </div>
                <div class="infos-cmd info-type-livraison col-12 col-md-6">
                    <ul class="list-item-cmd">
                        <li><?php
                           if ($type_livraison == 'Relais colis') {
                               echo '<ul>
                               <li class="relais-colis">
                                <p>
                                    <b>Livraison : ' . $type_livraison .' </b>
                                </p>
                               </li>
                               
                               <li class="relais-colis">
                                <p>'. $adresse_livraison_relais . '</p>
                               </li>
                               </ul>';
                           } else {
                               echo '<p class="relais-colis"><b>Livraison : ' . $type_livraison . '</b></p>';
                           } ?>
                        </li>
                    </ul>
                </div>
            </div>

            <?php
            //adresse de garantie si Obiwash commandé
               if ($nb_obiwash > -1) {
                   ?>
            <div class="row">
                <div class="infos-cmd info-type-livraison col-12 col-md-6">
                    <h3>Adresses de garantie</h3>
                    <ul class="list-item-cmd">
                        <?php
                               for ($i = 1; $i <= $nb_obiwash; $i+=1) {
                                   echo '<li><h4 class="title-obi-warranty">Obiwash commandé N° ' . $i .'</h4></li>';
                                   echo '<li>' . $commande['address_'.$i] . '</li>';
                               }
                    $html = ob_get_contents();
                    ob_end_flush(); ?>

                    </ul>
                </div>
            </div>
            <div class="row">
                <p><input type="checkbox" class="acceptance" id="acceptance-checkbox" name="acceptance">
                <label for="acceptance-checkbox">En cochant cette case vous accepter que la société HPNT-Systèmes enregistre vos données sur son serveur à des fins marketing et marchand. Vous affirmez avoir lu et accepter les conditions générales d'utilisation des données ainsi que les conditions générales d'utilisation du site www.obiwash.fr, consultables <a href="https://obiwash.fr/conditions-generales-utilisation-site" title="Lire les CGU">ici</a>. Selon la RGPD, vous pouvez exercer votre droit de regard et de retrait de vos données à tout moment par simple email à l'adresse suivante : contact@hpnt-systemes.fr</label></p>
                <a class="col-12 col-md-4 link-modif-commande button btn btn-cmde btn-edit-commande" href="/commandes/modifier/<?= $idCommande ?>" title="Modifier la commande">Modifier avant de commander<i class="fas fa-shopping-cart"></i></a>
                <a class="col-12 col-md-3 button btn btn-cmde btn-annuler-commande" href="/commandes/annulation/<?= $idCommande ?>" title="Annuler la commande">Annuler la commande<i class="far fa-trash-alt"></i></a>
                <button class="col-12 col-md-3 btn-valider-commande" id="valider-commande-button" type="submit">Valider la commande<i class="fas fa-check"></i></button>
            </div>
    </div>
    <?php
               } else { ?>
    <div class="row">
        <a class="col-12 col-md-4 link-modif-commande button btn btn-cmde btn-edit-commande" href="/commandes/modifier/<?= $idCommande ?>" title="Modifier la commande">Modifier avant de commander<i class="fas fa-shopping-cart"></i></a>
        <a class="col-12 col-md-3 button btn btn-cmde btn-annuler-commande" href="/commandes/annulation/<?= $idCommande ?>" title="Annuler la commande">Annuler la commande<i class="far fa-trash-alt"></i></a>
        <button class="col-12 col-md-3 btn-valider-commande" type="submit">Valider la commande<i class="fas fa-check"></i></button>
    </div>
    <?php }


            $message = preg_replace('#<div class="container">(.*)</div>#', '$1', $html); 
            $url ='https://commandes.obiwash.gribdev.eu/public/';
            
            ?>
            
    <textarea class="commande-finale d-none" name="commande">
        <?php echo '<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional //EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
            <html xmlns="http://www.w3.org/1999/xhtml">
                <head>
                    <title>Récapitulatif de votre commande de la boutique Obiwash®</title>
                    <meta name="viewport" content="width=device-width, initial-scale=1">
                    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
                    <style type="text/css">
                        /* Fonts and Content */
                        body, td { font-family: "Helvetica Neue", Arial, Helvetica, Geneva, sans-serif; font-size:14px; }
                        body { background-color: #2A374E; margin: 0; padding: 0; -webkit-text-size-adjust:none; -ms-text-size-adjust:none; }
                        h2{ padding-top:12px; /* ne fonctionnera pas sous Outlook 2007+ */color:#0E7693; font-size:22px; }

                        @media only screen and (max-width: 480px) {
                            table[class=w275], td[class=w275], img[class=w275] { width:135px !important; }
                            table[class=w30], td[class=w30], img[class=w30] { width:10px !important; }
                            table[class=w580], td[class=w580], img[class=w580] { width:280px !important; }
                            table[class=w640], td[class=w640], img[class=w640] { width:300px !important; }
                            img{ height:auto;}
                             /*illisible, on passe donc sur 3 lignes */
                            table[class=w180], td[class=w180], img[class=w180] {
                                width:280px !important;
                                display:block;
                            }
                            td[class=w20]{ display:none; }
                        }

                        td img {
                            width: 100px;
                        }

                        td div {
                            display:flex;
                            flex-wrap: nowrap;
                        }

                        td div p {
                            text-align:left;
                            padding-left:10px;
                        }

                        table thead tr {
                            background-color:#eeeeee;
                            text-align:center;
                        }

                        h3 {
                            color:#01A0C6;
                            text-transform:uppercase;
                        }

                        h4 span {
                            color:#01A0C6;
                            margin-top:0px;
                        }

                        ul {
                            list-style-type: none;
                            margin: 0px;
                            margin-bottom:30px;
                            padding-left:10px;
                        }

                        td p {
                            text-align:right;
                        }

                        td strong {
                            font-size:18px;
                        }

                        td p span strong {
                            color:#01A0C6;
                        }

                    </style>
                </head>
                <body style="margin:0px; padding:0px; -webkit-text-size-adjust:none; background-color:#f7f7f7;">
                <table style="padding:0px; margin:0px auto; width:100%;">
                        <tbody>
                            <tr>
                                <td style="text-align:center;">
                                <table style="border:0px; padding:0px; margin:0px  auto;">
                                        <tbody>
                                            <tr>
                                                <td class="w640" width="640" height="20">
                                                <img style="text-decoration: none; display: block;" src="' . $url .'/img/gif/white-space.gif" alt="space"/>
                                            </tr>

                                            <tr class="pagetoplogo">
                                                <td class="w640" style="width:640px">
                                                <table class="w640" style="padding:0px; margin:0px; width:640px;">
                                                    <tbody>
                                                        <td class="w640" style="width:640px;">
                                                            <img class="w30" style="text-decoration: none; display: block;" src="' . $url .'img/jpg/obiwash-top-email.jpg" alt="Obiwash®, le mélangeur intelligent pour tous" />
                                                        </td>
                                                    </tbody>
                                                </table>
                                            </td>
                                            <tr>
                                            <tr class="content">
                                           <td class="w640" style="background-color:#FFFFFF; width:640px">
                                                        <table class="w640" style="padding:0px; margin:0px; width:640px;">
                                                        <tbody>
                                                        
                                                            <tr>
                                                            <td class="w30" style="width:30px;">
                                                            <img class="w30" style="text-decoration: none; display: block;" src="' . $url . '/img/gif/white-space.gif" alt="space"/>
                                                        </td>
                                                            <h2 style="font-size:20px;color:black;">Bon de Commande</h2>
                                                                <td class="w580 message">' . $message . '

                                                                <p style="margin: 15px 0">Merci de votre confiance.<br>L\'équipe de Obiwash</p>
                                                                    <p style="margin: 15px 0;">
                                                                        <em style="color: #938795; font-size: 10px; font-weight: normal;">
                                                                            Obiwash© 2021. Tous droits réservés. Application réalisée par <a href="https://gribouillenet.fr" target="_blank" title="Voir les autres réalisations du webmaster">Gribouillenet©</a>
                                                                        </em>
                                                                    </p>

                                                                </td>
                                                                 <td class="w30" width="30">
                                                                  <img class="w30" style="text-decoration: none; display: block;" src="' . $url . '/img/gif/white-space.gif" alt="space"/>
                                                              </td>
                                                            </tr>
                                                        </tbody>
                                                     </table>
                                                 </td>
                                            </tr>
                                            <tr>
                                                <td class="w640" width="640" height="20">
                                                <img style="text-decoration: none; display: block;" src="' . $url . '/img/gif/white-space.gif" alt="space"/>
                                            </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </body>
            </html>'; ?>
    </textarea>

    </form>
    </div>

    <?php
        } else { ?>
    <div class="recap-commande container-panier">
        Votre panier est vide !
    </div>
    <?php }
    endforeach ;?>
</section>
<?php require($_SERVER['DOCUMENT_ROOT'].'/app/views/include/footer.php'); ?>