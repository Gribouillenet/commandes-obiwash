<p class="breadcrumb container d-flex p-lg-0">
	<a class="d-flex align-items-center" href="/" title="Retour à la page d'accueil de la boutique">Vous êtes ici :<i class="ti-home"></i><span class="home-page">Accueil</span></a><i class="ti-angle-right"></i><span class="current-page"><?= $title ?></span>
</p>