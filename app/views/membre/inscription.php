<?php require($_SERVER['DOCUMENT_ROOT'].'/app/views/include/header.php'); ?>
<section class="page-inner clearfix">
    <header class="page-header d-flex justify-content-center align-items-center">
        <h1 class="text-xs-center">Fiche d'inscription</h1>
    </header>
    <div class="container content-fiche-signin">
        
        <h2>Enregistrement de vos données personnelles</h2>
       
       <?php if (isset($data['erreur']['erreur'])) { ?>
       <div class="alert alert-danger alert-on"><?= $data['erreur']['erreur'] ?></div>
       <?php } else { ?>
       <?php if (isset($data['erreur'])) { ?>
       <div class="alert alert-danger alert-on"><?= $data['erreur']['champ_obligatoire'] ?></div>
       <?php } else { ?>
       <span class="champ-obligatoire d-inline-block"><sup>*</sup> Champs Obligatoires</span>
       <?php }
       }?>
        

        <form id="form-sign-in" class="form-sign-in row" action="" method="post" class="p-y-3 p-x-2" enctype="multipart/form-data" novalidate>
            <div class="col-12 col-md-6">
                <div class="container-input">
                    <label for="login">Votre identifiant<sup>*</sup></label>
                    <input type="text" name="login" class="form-control form-obligatoire" placeholder="Identifiant utilisateur" value="<?php if (isset($_POST['login'])) {
            echo $_POST['login'];
        } ?>">
                    <?php if (isset($data['erreur']['login'])) : ?>
                    <div class="alert alert-danger alert-form"><?= $data['erreur']['login'] ?></div>
                    <?php endif; ?>
                </div>
                <div class="container-input">
                    <label for="login">Votre nom<sup>*</sup></label>
                    <input type="text" name="name" class="form-control form-obligatoire" placeholder="Nom" value="<?php if (isset($_POST['name'])) {
            echo $_POST['name'];
        } ?>">
                    <?php if (isset($data['erreur']['name'])) : ?>
                    <div class="alert alert-danger alert-form"><?= $data['erreur']['name'] ?></div>
                    <?php endif; ?>
                </div>
                <div class="container-input">
                    <label for="login">Votre prénom<sup>*</sup></label>
                    <input type="text" name="first_name" class="form-control form-obligatoire" placeholder="Prénom" value="<?php if (isset($_POST['first_name'])) {
            echo $_POST['first_name'];
        } ?>">
                    <?php if (isset($data['erreur']['first_name'])) : ?>
                    <div class="alert alert-danger alert-form"><?= $data['erreur']['first_name'] ?></div>
                    <?php endif; ?>
                </div>
                <div class="container-input">
                    <label for="login">Votre adresse postale<sup>*</sup></label>
                    <input type="text" name="address" class="form-control form-obligatoire" placeholder="Adresse" value="<?php if (isset($_POST['address'])) {
            echo $_POST['address'];
        } ?>">
                    <?php if (isset($data['erreur']['address'])) : ?>
                    <div class="alert alert-danger alert-form"><?= $data['erreur']['address'] ?></div>
                    <?php endif; ?>
                </div>
                <div class="container-input">
                    <label for="login">Code postal<sup>*</sup></label>
                    <input type="text" name="code_postal" class="form-control form-obligatoire" placeholder="ex : 71000" maxlength="5" value="<?php if (isset($_POST['code_postal'])) {
            echo $_POST['code_postal'];
        } ?>">
                    <?php if (isset($data['erreur']['code_postal'])) : ?>
                    <div class="alert alert-danger alert-form"><?= $data['erreur']['code_postal'] ?></div>
                    <?php endif; ?>
                </div>
            </div>

            <div class="col-12 col-md-6">
                <div class="container-input">
                    <label for="login">Complément d'adresse</label>
                    <input type="text" name="address_supplement" class="form-control form-obligatoire" placeholder="Ex : bâtiment, résidence,..." value="<?php if (isset($_POST['address_supplement'])) {
            echo $_POST['address_supplement'];
        } ?>">
                </div>
                <div class="container-input">
                    <label for="login">Si vous commandez pour une Société ou un Service</label>
                    <input type="text" name="company_service" class="form-control form-obligatoire" placeholder="Société/Service" value="<?php if (isset($_POST['company_service'])) {
            echo $_POST['company_service'];
        } ?>">
                </div>
                <div class="container-input">
                    <label for="login">Votre email<sup>*</sup></label>
                    <input type="text" name="mail_member" class="form-control form-obligatoire" placeholder="Ex : monemail@domaine.com" value="<?php if (isset($_POST['mail_member'])) {
            echo $_POST['mail_member'];
        } ?>">
                    <?php if (isset($data['erreur']['mail_member'])) : ?>
                    <div class="alert alert-danger alert-form"><?= $data['erreur']['mail_member'] ?></div>
                    <?php endif; ?>
                </div>
                <div class="container-input">
                    <label for="login">Votre téléphone<sup>*</sup></label>
                    <input type="text" name="tel" class="form-control form-obligatoire" placeholder="Fixe ou portable" maxlength="14" value="<?php if (isset($_POST['tel'])) {
            echo $_POST['tel'];
        } ?>">
                    <?php if (isset($data['erreur']['tel'])) : ?>
                    <div class="alert alert-danger alert-form"><?= $data['erreur']['tel'] ?></div>
                    <?php endif; ?>
                </div>
                <div class="container-input">
                    <label for="login">Ville de résidence<sup>*</sup></label>
                    <input type="text" name="city" class="form-control form-obligatoire" placeholder="Ville" value="<?php if (isset($_POST['city'])) {
            echo $_POST['city'];
        } ?>">
                    <?php if (isset($data['erreur']['city'])) : ?>
                    <div class="alert alert-danger alert-form"><?= $data['erreur']['city'] ?></div>
                    <?php endif; ?>
                </div>
            </div>
            <h2>Calcul des frais de livraison</h2>
            <div class="form-group">
                <p>Nous livrons en France Métropolitaine et Outre-Mer. Afin de déterminer vos frais de livraison merci de choisir dans la liste ci-dessous votre pays de résidence.</p>
                <label class="d-block" for="sel1">Choisissez votre pays<sup>*</sup></label>
                <select id="sel1" class="d-block form-control form-obligatoire" name="country">
                    <option value="" disabled selected>à sélectionner dans cette liste</option>
                    <optgroup label="France Métropolitaine">
                        <option value="France" <?php if ($_POST['country'] == 'France') {
            echo "selected";
        } ?>>
                            France
                        </option>
                    </optgroup>
                    <optgroup label="Outre-mer Zone 1">
                        <option value="Réunion" <?php if ($_POST['country'] == 'Réunion') {
            echo "selected";
        } ?>>
                            Réunion
                        </option>
                        <option value="Guadeloupe" <?php if ($_POST['country'] == 'Guadeloupe') {
            echo "selected";
        } ?>>
                            Guadeloupe
                        </option>
                        <option value="Martinique" <?php if ($_POST['country'] == 'Martinique') {
            echo "selected";
        } ?>>
                            Martinique
                        </option>
                        <option value="Mayotte" <?php if ($_POST['country'] == 'Mayotte') {
            echo "selected";
        } ?>>
                            Mayotte
                        </option>
                        <option value="Guyane" <?php if ($_POST['country'] == 'Guyane') {
            echo "selected";
        } ?>>
                            Guyane
                        </option>
                        <option value="Saint-Pierre et Miquelon" <?php if ($_POST['country'] == 'Saint-Pierre et Miquelon') {
            echo "selected";
        } ?>>
                            Saint-Pierre et Miquelon
                        </option>
                        <option value="Saint-Martin" <?php if ($_POST['country'] == 'Saint-Martin') {
            echo "selected";
        } ?>>
                            Saint-Martin
                        </option>
                        <option value="Saint-Barthélemy" <?php if ($_POST['country'] == 'Saint-Barthélemy') {
            echo "selected";
        } ?>>
                            Saint-Barthélemy
                        </option>
                    </optgroup>
                    <optgroup label="Outre-mer Zone 2">
                        <option value="Polynésie Française" <?php if ($_POST['country'] == 'Polynésie Française') {
            echo "selected";
        } ?>>
                            Polynésie Française
                        </option>
                        <option value="Wallis et Futuna" <?php if ($_POST['country'] == 'Wallis et Futuna') {
            echo "selected";
        } ?>>
                            Wallis et Futuna
                        </option>
                        <option value="Nouvelle Calédonie" <?php if ($_POST['country'] == 'Nouvelle Calédonie') {
            echo "selected";
        } ?>>
                            Nouvelle Calédonie
                        </option>
                        <option value="TAAF" <?php if ($_POST['country'] == 'TAAF') {
            echo "selected";
        } ?>>
                            TAAF
                        </option>
                    </optgroup>
                </select>

                <?php if (isset($data['erreur']['country'])) : ?>
                <div class="alert alert-danger alert-form"><?= $data['erreur']['country'] ?></div>
                <?php endif; ?>
            </div>

            <div id="validation" class="validation">
                <p><input type="checkbox" class="acceptance" id="inscription-acceptance" name="acceptance">
                <label for="inscription-acceptance">En cochant cette case vous accepter que la société HPNT-Systèmes enregistre vos données sur son serveur à des fins marketing et marchand. Vous affirmez avoir lu et accepter les conditions générales d'utilisation des données ainsi que les conditions générales d'utilisation du site www.obiwash.fr, consultables <a href="https://obiwash.fr/conditions-generales-utilisation-site" title="Lire les CGU">ici</a>. Selon la RGPD, vous pouvez exercer votre droit de regard et de retrait de vos données à tout moment par simple email à l'adresse suivante : contact@hpnt-systemes.fr<label></p>
                <div id="btn-record" class="btn button btn-record d-flex justify-content-center align-items-center col-12 col-md-4">
                    <input type="submit" id="input-record" value="Enregistrer mon compte"><i class="ti-save"></i>
                </div>
            </div>
        </form>
        <div class="container-btn-view-bucket container-fluid d-flex justify-content-center mt-3">
            <?php include($_SERVER['DOCUMENT_ROOT'].'/app/views/include/inc/button-come-back.php'); ?>
        </div>
    </div>
</section>
<?php require($_SERVER['DOCUMENT_ROOT'].'/app/views/include/footer.php'); ?>