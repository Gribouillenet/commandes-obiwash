<?php
$membre = $data['membre'];


if ($membre == null) { ?>
<ul class="nav-widgets d-flex flex-row flex-lg-column">
    <li class="nav-item-widget nav-item-widget-connexionv col-4 col-lg-auto p-0">
        <a class="nav-link-widget nav-link-widget-connexion d-flex align-items-center" title="Se connecter à son compte" href="/connexion/index"><i class="ti-user"></i><span class="widget-txt d-none d-xl-block">Se Connecter</span></a>
    </li>
    <li class="nav-item-widget nav-item-widget-contact col-4 col-lg-auto p-0">
        <a class="nav-link-widget nav-link-widget-contact d-flex align-items-center" title="Nous contacter" href="<?= $url_site ?>/contact-sav/"><i class="ti-email"></i><span class="widget-txt d-none d-xl-block">Contact/SAV</span></a>
    </li>
    <li class="nav-item-widget nav-item-widget-panier d-flex col-4 col-lg-auto p-0">
        <a href="#" title="Voir mon récapitulatif panier" class="nav-link-widget nav-link-widget-panier d-flex align-items-center"><i class="ti-shopping-cart"></i><span class="widget-txt d-none d-xl-block">Panier</span><span id="count" class="count d-block"><?= $panier->count() ?></span></a>

    </li>
    <!-- 		                        <li class="nav-item-widget"><a href="/"><span id ="count"><?= $panier->count() ?></span> - <span><span id ="total"><?= number_format($panier->total(), 2, ',', ' ') ?></span> €</span></a></li> -->

</ul>
<?php } else { ?>
<ul class="nav-widgets d-flex flex-row flex-lg-column">

    <li class="nav-item-widget nav-item-widget-compte col-4 col-lg-auto p-0">
        <a title="Voir mon compte" class="nav-link-widget nav-link-widget-compte  d-flex align-items-center" href="#"><i class="ti-user"></i><span class="widget-txt d-none d-lg-block">Mon compte</span><i id="compte" class="ti-angle-down arrow-down mr-4 mr-md-0"></i></a>
        <ul class="compte-sub-menu">
            <li class="nav-item-widget">
                <a title="Voir les détails de profil" class="nav-link-widget nav-link-widget-profil d-flex align-items-center" href="/membre/compte"><i class="ti-settings"></i>Mon profil</a>
            </li>
            <li class="nav-item-widget">
                <a title="Se déconnecter" class="nav-link-widget nav-link-widget-deconnexion d-flex align-items-center" href="/connexion/deconnexion"><i class="ti-lock"></i>Se Déconnecter</a>
            </li>
        </ul>
    </li>
    <li class="nav-item-widget nav-item-widget-contact col-4 col-lg-auto p-0">
        <a class="nav-link-widget nav-link-widget-contact d-flex align-items-center" title="Nous contacter" href="<?= $url_site ?>/contact-sav/"><i class="ti-email"></i><span class="widget-txt d-none d-lg-block">Contact/SAV</span></a>
    </li>
    <li class="nav-item-widget nav-item-widget-panier d-flex col-4 col-lg-auto p-0">
        <a title="Voir mon panier" class="nav-link-widget nav-link-widget-panier d-flex align-items-center" href="/panier/index"><i class="ti-shopping-cart"></i><span class="widget-txt d-none d-lg-block">Mon panier</span><span id="count" class="count d-block"><?= $panier->count() ?></span></a>

    </li>
</ul>
<?php } ?>