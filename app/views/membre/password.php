<?php require($_SERVER['DOCUMENT_ROOT'].'/app/views/include/header.php');?>
	<section class="page-inner clearfix">
		<header class="page-header d-flex flex-column justify-content-center align-items-center">
			<h1 class="text-xs-center">Mot de passe oublié</h1>
		</header>
        <div class="container password">
                <div class="container password">
                        <?php if ( isset( $data['json']['message'] ) ) : ?>
                            <script>
                                var msg = "<?= $data['json']['message'] ?>";
                                if(!alertify.myAlert){
                                    //define a new dialog
                                    alertify.dialog('myAlert',function(){
                                        return{
                                            build:function(){
                                                this.setHeader('<i class="ti-panel"></i>');
                                            },
                                            main:function(message){
                                                this.message = message;
                                            },
                                            setup:function(){
                                                return {
                                                    buttons:[{text: "ok", key:27/*Esc*/}],
                                                    focus: { element:0 }
                                                };
                                            },
                                            prepare:function(){
                                                this.setContent(this.message);
                                            }
                                        }
                                    });
                                }
                                //launch it.
                                alertify.myAlert(msg);
                            </script>
                        <?php endif; ?>
        <form class="form-enter form-renew-pwd" action="" method="post" class="p-y-3 p-x-2" enctype="multipart/form-data" novalidate>
            <input type="text" name="login" placeholder="identifiant"class="form-control form-obligatoire" value="<?php if ( isset( $_POST['login'] ) ) echo $_POST['login'] ?>">
            <?php if ( isset( $data['erreur']['login'] ) ) : ?>
                <div class="alert alert-danger alert-form"><?= $data['erreur']['login'] ?></div>
            <?php endif; ?>

            <div class="container-btn-view-bucket container-fluid d-flex align-items-center justify-content-center mt-3">
                <?php include($_SERVER['DOCUMENT_ROOT'].'/app/views/include/inc/button-come-back.php'); ?>
                <button class="btn btn-renew-ok d-inline-block">
                        <input type="submit" value="Valider"><i class="ti-check"></i>
                </button>         
            </div>
        </form>
		
            </div>
	</section>

<?php require($_SERVER['DOCUMENT_ROOT'].'/app/views/include/footer.php'); ?>
