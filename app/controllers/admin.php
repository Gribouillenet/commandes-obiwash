<?php
class Admin extends Controller {
// AFFICHAGE DES PRODUITS
	public function index() {
		if( !isset( $_SESSION['Auth']['id'] ) || ( $_SESSION['Auth']['login'] != 'admin_obiwash' && $_SESSION['Auth']['password'] != '106e9ee5a8d402f04947809da4811c42') ) {
			header( 'Location: /connexion' );
		}

		$produits = DB::select( 'select * from products order by id asc' );

		if( !empty( $_POST ) ) {
			extract( $_POST );
			$erreur = [];

            if ( empty ( $title ) || empty( $description ) || empty( $price ) || empty( $stock ) || empty( $weight ) ) {
				$erreur['champ_obligatoire'] = '* Champs Obligatoires';
			}

			if( empty ( $title ) ) {
				$erreur['title'] = 'Titre obligatoire !';
			}

			if( empty ( $description ) ) {
				$erreur['description'] = 'Texte obligatoire !';
			}

			if( empty ( $price ) ) {
				$erreur['price'] = 'Prix obligatoire !';
			}

			if( empty ( $stock ) ) {
				$erreur['stock'] = 'Stock obligatoire !';
			}

			if( empty ( $weight ) ) {
				$erreur['weight'] = 'Poids obligatoire !';
			}

			// if( empty ( $finished_product ) ) {
			// 	$erreur['finished_product'] = 'Dimensions produit fini obligatoires !';
			// }

			// if( empty ( $packed_product ) ) {
			// 	$erreur['packed_product'] = 'Dimensions produit emballé obligatoires !';
			// }

			if( empty ( $data_sheet ) ) {
				$erreur['data_sheet'] = 'Fiche technique obligatoire !';
			}

			if ( isset( $_FILES['fichier'] ) && $_FILES['fichier']['error'] == 0 ){
                if ( !in_array( $_FILES['fichier']['type'], ['image/png', 'image/jpeg'] ) ){
                    $erreur['fichier'] = 'Format de l\'image incorrect.';
                }
                elseif ($_FILES['fichier']['size'] > 500000 ){
                    $erreur['fichier'] = 'Image trop volumineux (sup&eacute;rieur à 10Ko)';
                }
            }

			if( !$erreur) {
				DB::insert( 'insert into products (title, description, price, stock, weight, data_sheet) 
				values (:title, :description, :price, :stock, :weight, :data_sheet)', [
					'title' => htmlspecialchars( $title ),
					'description' => htmlspecialchars( $description ),
					'price' => htmlspecialchars( $price ),
					'stock' => htmlspecialchars( $stock ),
					'weight' => htmlspecialchars( $weight ),
					// 'finished_product' => htmlspecialchars( $finished_product ),
                    // 'packed_product' => htmlspecialchars( $packed_product ),
                    'data_sheet' => htmlspecialchars( $data_sheet ),
				] );

				//FICHIERS
                $filname = $_FILES['fichier']['name'];
                $tab = explode('.', $filname);
                $comm = DB::select ('SELECT max(id) as pdt FROM products');
                $com = $comm[0]['pdt'];
                $ext = $tab[count( $tab )-1];
                $camp = $_POST['title'];
                $camp = str_replace(' ', '-', $camp);

                if($ext)  $filename = strtolower($camp . '.' . $ext);

                if ( move_uploaded_file( $_FILES['fichier']['tmp_name'], ROOT . 'public/files/' . $filename ) ) {
					DB::update( 'update products set image = :image WHERE id = ' . $com , [ 'image' => $filename ]);
				}

				header( 'Location: /admin' );
			}
			else {
				$erreur[''] = 'Tous les champs doivent être remplis';
			}

			$this->view( 'admin/produits', ['erreur' => $erreur, 'produits' => $produits] );
		}

		$this->view( 'admin/produits', ['produits' => $produits] );

	}

//PUBLIER UN PRODUIT
	public function produits() {
		if( !isset( $_SESSION['Auth']['id'] ) || ( $_SESSION['Auth']['login'] != 'admin_obiwash' && $_SESSION['Auth']['password'] != '106e9ee5a8d402f04947809da4811c42') ) {
			header( 'Location: /connexion' );
		}

		$produits = DB::select( 'select * from products order by id asc' );

		if( !empty( $_POST ) ) {
			extract( $_POST );

			$erreur = [];

            if ( empty ( $title ) || empty( $description ) || empty( $price ) || empty( $stock ) || empty( $weight ) ) {
				$erreur['champ_obligatoire'] = '* Champs Obligatoires';
			}

			if( empty ( $title ) ) {
				$erreur['title'] = 'Titre obligatoire !';
			}

			if( empty ( $description ) ) {
				$erreur['description'] = 'Texte obligatoire !';
			}

			if( empty ( $price ) ) {
				$erreur['price'] = 'Prix obligatoire !';
			}

			if( empty ( $stock ) ) {
				$erreur['stock'] = 'Stock obligatoire !';
			}

			if( empty ( $weight ) ) {
				$erreur['weight'] = 'Poid obligatoire !';
			}

			if ( isset( $_FILES['fichier'] ) && $_FILES['fichier']['error'] == 0 ){
                if ( !in_array( $_FILES['fichier']['type'], ['image/png', 'image/jpeg'] ) ){
                    $erreur['fichier'] = 'Format de l\'image incorrect.';
                }
                elseif ($_FILES['fichier']['size'] > 500000 ){
                    $erreur['fichier'] = 'Image trop volumineux (sup&eacute;rieur à 10Ko)';
                }
            }

			if( !$erreur) {
				DB::insert( 'insert into products (title, description, price, stock, weight) values (:title, :description, :price, :stock, :weight)', [
					'title' => htmlspecialchars( $title ),
					'description' => htmlspecialchars( $description ),
					'price' => htmlspecialchars( $price ),
					'stock' => htmlspecialchars( $stock ),
					'weight' => htmlspecialchars( $weight )
				] );

				//FICHIERS
                $filname = $_FILES['fichier']['name'];
                $tab = explode('.', $filname);
                $comm = DB::select ('SELECT max(id) as pdt FROM products');
                $com = $comm[0]['pdt'];
                $ext = $tab[count( $tab )-1];
                $camp = $_POST['title'];
                $camp = str_replace(' ', '-', $camp);

                if($ext)  $filename = strtolower($camp . '.' . $ext);

                if ( move_uploaded_file( $_FILES['fichier']['tmp_name'], ROOT . 'public/files/' . $filename ) ) {
					DB::update( 'update products set image = :image WHERE id = ' . $com , [ 'image' => $filename ]);
				}

				header( 'Location: /admin' );
			}
			else{
				$erreur[''] = 'Tous les champs doivent être remplis';
			}

			$this->view( 'admin/produits', ['erreur' => $erreur, 'produits' => $produits] );
		}

		$this->view( 'admin/produits', ['produits' => $produits] );

	}

//SUPPRIMER UN PRODUIT
	public function supprimer( int $id ) {
		if( !isset( $_SESSION['Auth']['id'] ) || ( $_SESSION['Auth']['login'] != 'admin_obiwash' && $_SESSION['Auth']['password'] != '106e9ee5a8d402f04947809da4811c42') ) {
			header( 'Location: /connexion' );
		}

		$produits = DB::delete ( 'delete from products where id = ?', [$id] );

		header( 'Location: /admin' );
	}

//EDITER UN PRODUIT
	public function editer( int $id ) {
		if( !isset( $_SESSION['Auth']['id'] ) || ( $_SESSION['Auth']['login'] != 'admin_obiwash' && $_SESSION['Auth']['password'] != '106e9ee5a8d402f04947809da4811c42') ) {
			header( 'Location: /connexion' );
		}

		$produits = DB::select( 'select * from products where id = ?', [$id] );

		if  ( !$produits ){
			header( 'Location: /admin' );
		}

		if ( !empty( $_POST ) ) {
			extract( $_POST );
			$erreur = [];

			if ( empty ( $title ) || empty( $description ) || empty( $price ) || empty( $stock ) || empty( $weight ) || empty( $data_sheet ) ) {
				$erreur['champ_obligatoire'] = '* Champs Obligatoires';
			}

			if( empty ( $title ) ) {
				$erreur['title'] = 'Titre obligatoire !';
			}

			if( empty ( $description ) ) {
				$erreur['description'] = 'Texte obligatoire !';
			}

			if( empty ( $price ) ) {
				$erreur['price'] = 'Prix obligatoire !';
			}

			if( empty ( $stock ) ) {
				$erreur['stock'] = 'Stock obligatoire !';
			}

			if( empty ( $weight ) ) {
				$erreur['weight'] = 'Poids obligatoire !';
			}

			// if( empty ( $finished_product ) ) {
			// 	$erreur['finished_product'] = 'Dimensions produit fini obligatoires !';
			// }

			// if( empty ( $packed_product ) ) {
			// 	$erreur['packed_product'] = 'Dimensions produit emballé obligatoires !';
			// }

			if( empty ( $data_sheet ) ) {
				$erreur['data_sheet'] = 'Fiche technique obligatoire !';
			}

			if ( isset( $_FILES['fichier'] ) && $_FILES['fichier']['error'] == 0 ){
                if ( !in_array( $_FILES['fichier']['type'], ['image/png', 'image/jpeg'] ) ){
                    $erreur['fichier'] = 'Format de l\'image incorrect.';
                }
                elseif ($_FILES['fichier']['size'] > 500000 ){
                    $erreur['fichier'] = 'Image trop volumineux (sup&eacute;rieur à 10Ko)';
                }
            }

			if( !$erreur) {
				DB::update( 'update products set title = :title, description = :description, price = :price, stock = :stock, weight = :weight, data_sheet = :data_sheet where id = :id', [
					'title' => htmlspecialchars( $title ),
					'description' => htmlspecialchars( $description ),
					'price' => htmlspecialchars( $price ),
					'stock' => htmlspecialchars( $stock ),
					'weight' => htmlspecialchars( $weight ),
					'data_sheet' => htmlspecialchars( $data_sheet ),
					'id' => $id
				] );

				//FICHIERS
                $filname = $_FILES['fichier']['name'];
                $tab = explode('.', $filname);
                $ext = $tab[count( $tab )-1];
                $camp = $_POST['title'];
                $camp = str_replace(' ', '-', $camp);

                if($ext)  $filename = strtolower($camp . '.' . $ext);

                if ( move_uploaded_file( $_FILES['fichier']['tmp_name'], ROOT . 'public/files/' . $filename ) ) {
					DB::update( 'update products set image = :image WHERE id = ' . $id , [ 'image' => $filename ]);
				}

				header( 'Location: /admin/' . $id );
			}
			else{
                $erreur[''] = 'Tous les champs doivent être remplis';
			}

            $this->view( 'admin/editer', ['erreur' => $erreur, 'produits' => $produits[0]] );
		}

		$this->view( 'admin/editer', ['produits' => $produits[0]] );

	}

//CRÉATION D'UN MEMBRE
	public function membres() {
		if( !isset( $_SESSION['Auth']['id'] ) || ( $_SESSION['Auth']['login'] != 'admin_obiwash' && $_SESSION['Auth']['password'] != '106e9ee5a8d402f04947809da4811c42') ) {
			header( 'Location: /connexion' );
		}

		$member = DB::select( 'select * from member order by id asc' );

		if( !empty( $_POST ) ) {
			extract( $_POST );
			$erreur = [];

			if ( empty ( $login ) || empty ( $name ) || empty( $first_name ) || empty ( $address ) || empty ( $code_postal ) || empty ( $city ) || empty ( $country ) || empty ( $mail_member ) || empty ( $tel ) ) {
				$erreur['champ_obligatoire'] = '* Champs Obligatoires';
			}

			if( empty ( $login ) ) {
				$erreur['login'] = 'Le nom d\'utilisateur est obligatoire !';
			}

			if( empty ( $name ) ) {
				$erreur['name'] = 'Le nom est obligatoire !';
			}

			if( empty ( $first_name ) ) {
				$erreur['first_name'] = 'Le prénom est obligatoire !';
			}

			if( empty ( $address ) ) {
				$erreur['address'] = 'L\'adresse est obligatoire !';
			}

			if( empty ( $code_postal ) ) {
				$erreur['code_postal'] = 'Le code postal est obligatoire !';
			} else {
				if ( !preg_match( '`^[0-9]{5}$`' , $code_postal ) ){
					$erreur['code_postal'] = 'Le code postal est invalide !';
				}
			}

			if( empty ( $city ) ) {
				$erreur['city'] = 'La ville est obligatoire !';
			}

			if( empty ( $country ) ) {
				$erreur['country'] = 'Le pays est obligatoire !';
			}

			if( empty ( $mail_member ) ) {
				$erreur['mail_member'] = 'L\'email est obligatoire !';
			}
			elseif ( !filter_var( $mail_member, FILTER_VALIDATE_EMAIL ) ){
				$erreur['mail_member'] = 'L\'adresse email est invalide !';
			}

            function validerNumero($telATester) {
                //Retourne le numéro s'il est valide, sinon false.
                return preg_match('`^0[1-9]([-. ]?[0-9]{2}){4}$`', $telATester) ? $telATester : false;
            }

            if( empty ( $tel ) ) {
				$erreur['tel'] = 'Le téléphone est obligatoire !';
			}

            if( !empty ( $tel ) ) {
                if ( !filter_var( $tel, FILTER_CALLBACK, array('options' => 'validerNumero') ) ) {
				    $erreur['tel'] = 'Le téléphone est incorrect.';
                }
			}

			$password1 = $this->generatePassword();
			$password = md5($password1);
			//$password = password_hash($password, PASSWORD_DEFAULT );

			if( !$erreur) {
				DB::insert( 'insert into member (name, first_name, company_service, address, address_supplement, code_postal, city, country, mail_member, tel, login, password) values (:name, :first_name, :company_service, :address, :address_supplement, :code_postal, :city, :country, :mail_member, :tel, :login, :password)', [
					'name' => htmlspecialchars( $name ),
					'first_name' => htmlspecialchars( $first_name ),
					'company_service' => htmlspecialchars( $company_service ),
					'address' => htmlspecialchars( $address ),
					'address_supplement' => htmlspecialchars( $address_supplement ),
					'code_postal' => htmlspecialchars( $code_postal ),
					'city' => htmlspecialchars( $city ),
					'country' => htmlspecialchars( $country ),
					'mail_member' => htmlspecialchars( $mail_member ),
					'tel' => htmlspecialchars( $tel ),
					'login' => htmlspecialchars( $login ),
					'password' => md5( $password )
				] );

			$to = $mail_member;
			$subject = 'Django Studio - Commandes | Vos identifiants';

			$member_last = DB::select( 'SELECT * FROM member WHERE member.id = (SELECT MAX(id) FROM member)' );

			$message = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional //EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Django Studio - Vos identifiants</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
   <meta content="width=device-width">
    <style type="text/css">
    /* Fonts and Content */
    body, td { font-family: "Helvetica Neue", Arial, Helvetica, Geneva, sans-serif; font-size:14px; }
    body { background-color: #F2F0F0; margin: 0; padding: 0; -webkit-text-size-adjust:none; -ms-text-size-adjust:none; }
    h2{ padding-top:12px; /* ne fonctionnera pas sous Outlook 2007+ */color:#000000; font-size:22px; }
    p{margin: 15px 0;}

    @media only screen and (max-width: 480px) {

        table[class=w275], td[class=w275], img[class=w275] { width:135px !important; }
        table[class=w30], td[class=w30], img[class=w30] { width:10px !important; }
        table[class=w580], td[class=w580], img[class=w580] { width:280px !important; }
        table[class=w640], td[class=w640], img[class=w640] { width:300px !important; }
        img{ height:auto;}
         /*illisible, on passe donc sur 3 lignes */
        table[class=w180], td[class=w180], img[class=w180] {
            width:280px !important;
            display:block;
        }
        td[class=w20]{ display:none; }
    }

    </style>

</head>
<body style="margin:0px; padding:0px; -webkit-text-size-adjust:none;" style="background-color:#F2F0F0;">
    <table width="100%" cellpadding="0" cellspacing="0" border="0" style="background-color:#F2F0F0;">
        <tbody>
            <tr>
                <td align="center" bgcolor="#581E62">
                    <table  cellpadding="0" cellspacing="0" border="0">
                        <tbody>
                            <tr>
                                <td class="w640"  width="640" height="20"></td>
                            </tr>

                            <tr class="pagetoplogo">
	                            <td class="w640" width="640">
		                            <table class="w640" width="640" cellspacing="0" cellpadding="0" border="0" bgcolor="#F2F0F0">
			                            <tbody>
				                            <td class="w30" width="30">
					                            <img class="w30" style="text-decoration: none; display: block;" src="https://commandes.django.fr/img/jpg/bg-left-email.jpg" alt="bg-left-email" width="" height="" />
				                            </td>
				                            <td class="w580" width="580">
					                             <img class="w580" style="text-decoration: none; display: block;" src="https://commandes.django.fr/img/jpg/bg-top-email.jpg" alt="bg-top-email" width="" height="" />
				                            </td>
				                            <td class="w30" width="30">
					                             <img class="w30" style="text-decoration: none; display: block;" src="https://commandes.django.fr/img/jpg/bg-right-email.jpg" alt="bg-right-email" width="" height="" />
				                            </td>
			                            </tbody>
		                            </table>
	                            </td>
	                        <tr>
		                    <tr class="content">
			                    <td class="w640" width="640" bgcolor="#FFFFFF">
			                     	<table class="w640" width="640" cellspacing="0" cellpadding="0" border="0">
                                        <tbody>
                                            <tr>
                                                <td class="w30" width="30">
	                                                <img class="w30" style="text-decoration: none; display: block;" src="https://commandes.django.fr/img/gif/white-space.gif" alt="space" width="" height="" />
                                                </td>
                                                <td class="w580" width="580">
	                                                <p style="margin: 15px 0;">Bonjour,<br><br>Afin de faciliter vos demandes, Django Studio a mis en place pour vous une nouvelle plateforme de commande.</p>
	                                                <p style="margin: 15px 0;">Veuillez trouver ci-après votre identifiant ainsi que votre mot de passe pour vous connecter à cet utilitaire. Ne perdez plus de temps découvrez-le dès maintenant !</p>
	                                                <p style="text-align:center; font-size: 14px; margin: 15px 0">
		                                                <a href="https://commandes.django.fr" title="Découvrir l\'utilitaire de commandes Django Studio" target="_blank" style="background-color: #841A59; border:none; color: #FFFFFF; cursor: pointer; font-size:18px; box-shadow: none!important; font-weight: bold; padding: 5px;"><button style="background-color: #841A59; border:none; color: #FFFFFF; cursor: pointer; font-size:18px; box-shadow: none!important; font-weight: bold; padding: 5px;">Commandes - Django Studio</button>
			                                            </a>
			                                        </p>
			                                        <p style="margin: 15px 0;">Nous restons bien évidemment à votre disposition pour vous accompagner.</p><br>
			                                        <p style="margin: 0 0 15px;">Votre identifiant : <b>' . $member_last[0]['login'] .'</b></p>
			                                        <p style="margin: 15px 0;">Votre mot de passe : <b>' . $password1 . '<b></p>
			                                        <p style="margin: 15px 0">Merci de votre confiance.<br>L\'équipe de Django Studio</p>
			                                        <p style="margin: 15px 0;">
				                                        <em style="color: #938795; font-size: 10px; font-weight: normal;">
				                                        	Django Studio© 2018. Tous droits réservés. Application réalisée par <a href="https://gribouillenet.fr" target="_blank" title="Voir les autres réalisations du webmaster">Gribouillenet©</a>
				                                        </em>
				                                    </p>
			                                    </td>
                                                <td class="w30" width="30">
	                                                 <img class="w30" style="text-decoration: none; display: block;" src="https://commandes.django.fr/img/gif/white-space.gif" alt="space" width="" height="" />
                                                 </td>
                                            </tr>
                                        </tbody>
			                     	</table>
		                     	</td>
		                    </tr>
		                    <tr>
                                <td class="w640"  width="640" height="20"></td>
                            </tr>
			            </tbody>
			        </table>
			    </td>
			</tr>
		</tbody>
	</table>
</body>
</html>';
			$headers = 'From: production@obiwash.fr' . "\r\n" .	'Reply-To: no-reply@obiwash.fr' . "\r\n" . 'Content-Type: text/html; charset=utf-8' .
			'X-Mailer: PHP/' . phpversion();

			mail($to, $subject, $message, $headers);
 				header( 'Location: /admin/membres' );
			}
			else{

				$erreur[''] .= 'Tous les champs doivent être remplis !';
			}

			$this->view( 'admin/membres', ['erreur' => $erreur, 'member' => $member] );
		}

		$this->view( 'admin/membres', ['member' => $member] );

	}

//SUPPRIMER UN MEMBRE
	public function supprimer_client( int $id ) {
		if( !isset( $_SESSION['Auth']['id'] ) || ( $_SESSION['Auth']['login'] != 'admin_obiwash' && $_SESSION['Auth']['password'] != '106e9ee5a8d402f04947809da4811c42') ) {
			header( 'Location: /connexion' );
		}

		$member = DB::delete ( 'delete from member where id = ?', [$id] );

		header( 'Location: /admin/membres' );
	}

//EDITER UN MEMBRE
	public function editer_client( int $id ) {
		if( !isset( $_SESSION['Auth']['id'] ) || ( $_SESSION['Auth']['login'] != 'admin_obiwash' && $_SESSION['Auth']['password'] != '106e9ee5a8d402f04947809da4811c42') ) {
			header( 'Location: /connexion' );
		}

		$member = DB::select( 'select * from member where id = ?', [$id] );

		if ( !$member ) {
			header( 'Location: /admin/membres' );
		}

		if ( !empty( $_POST ) ) {
			extract( $_POST );
			$erreur = [];

			if ( empty ( $login ) || empty ( $name ) || empty( $first_name ) || empty ( $address ) || empty ( $code_postal ) || empty ( $city ) || empty ( $country ) || empty ( $mail_member ) || empty ( $tel ) ) {
				$erreur['champ_obligatoire'] = '* Champs Obligatoires';
			}

			if( empty ( $name ) ) {
				$erreur['name'] = 'Le nom est obligatoire !';
			}

			if( empty ( $first_name ) ) {
				$erreur['first_name'] = 'Le prénom est obligatoire !';
			}

			if( empty ( $address ) ) {
				$erreur['address'] = 'L\'adresse est obligatoire !';
			}

			if( empty ( $code_postal ) ) {
				$erreur['code_postal'] = 'Le code postal est obligatoire !';
			} else {
				if ( !preg_match( '`^[0-9]{5}$`' , $code_postal ) ){
					$erreur['code_postal'] = 'Le code postal est invalide !';
				}
			}

			if( empty ( $city ) ) {
				$erreur['city'] = 'La ville est obligatoire !';
			}

			if( empty ( $country ) ) {
				$erreur['country'] = 'Le pays est obligatoire !';
			}

			if( empty ( $mail_member ) ) {
				$erreur['mail_member'] = 'L\'email est obligatoire !';
			}
			elseif ( !filter_var( $mail_member, FILTER_VALIDATE_EMAIL ) ){
				$erreur['mail_member'] = 'L\'adresse email est invalide !';
			}

            function validerNumero($telATester) {
                //Retourne le numéro s'il est valide, sinon false.
                return preg_match('`^0[1-9]([-. ]?[0-9]{2}){4}$`', $telATester) ? $telATester : false;
            }

            if( empty ( $tel ) ) {
				$erreur['tel'] = 'Le téléphone est obligatoire !';
			}

            if( !empty ( $tel ) ) {
                if ( !filter_var( $tel, FILTER_CALLBACK, array('options' => 'validerNumero') ) ) {
				    $erreur['tel'] = 'Le téléphone est incorrect.';
                }
			}

            if( empty ( $login ) ) {
				$erreur['login'] = 'Identifiant obligatoire !';
			}

			if( !$erreur) {
				DB::update( 'update member set name = :name, first_name = :first_name, company_service = :company_service, address = :address, address_supplement = :address_supplement, code_postal = :code_postal, city = :city, country = :country, mail_member = :mail_member, tel = :tel, login = :login where id = :id' , [
					'name' => htmlspecialchars( $name ),
					'first_name' => htmlspecialchars( $first_name ),
					'company_service' => htmlspecialchars( $company_service ),
					'address' => htmlspecialchars( $address ),
					'address_supplement' => htmlspecialchars( $address_supplement ),
				    'code_postal' => htmlspecialchars( $code_postal ),
					'city' => htmlspecialchars( $city ),
				    'country' => htmlspecialchars( $country ),
					'mail_member' => htmlspecialchars( $mail_member ),
				    'tel' => htmlspecialchars( $tel ),
					'login' => htmlspecialchars( $login ),
					'id' => $id
				] );
 				header( 'Location: /admin/membres' );
			}
			else{
				$this->view( 'admin/editer_client', ['erreur' => $erreur, 'member' => $member[0]] );
			}
		}

		$this->view( 'admin/editer_client', ['member' => $member[0]] );

	}

//HISTORIQUE D'UN MEMBRE
    public function historique_client( int $id ) {
		if( !isset( $_SESSION['Auth']['id'] ) || ( $_SESSION['Auth']['login'] != 'admin_obiwash' && $_SESSION['Auth']['password'] != '106e9ee5a8d402f04947809da4811c42') ) {
			header( 'Location: /connexion' );
		}

		$commandes = DB::select( 'SELECT * FROM order_member, member WHERE member.id = ? AND ref_Member = member.id GROUP BY order_member.id ORDER BY order_member.id DESC', [$id] );

        $membre = DB::select( 'SELECT * FROM  member WHERE  member.id = ?', [$id] );

		if(!$commandes){
		  $commandes = DB::select( 'SELECT * FROM  order_member WHERE ref_Member.id = ? ', [$id] );
		}

		$produits = DB::select( 'SELECT id, title FROM products' );

		foreach ( $commandes as $key => $commande ){

			$dateCommande = date_create( $commande['date_commande'] );
			$commandes[$key]['date_commande'] = date_format( $dateCommande, 'dmy' );

			$commandes[$key]['observations'] = nl2br( $commande['observations'] );
		}

		if ( isset ($_POST["choix"]) ) {
			foreach ( $commandes as $key => $commande ){
				foreach ($_POST["choix"] as $key => $value) {
					if ( $commande[0] == $value ) {
						DB::update( 'update order_member set paid = :paid where id = :id' , [
			   				'paid' => 1,
			   				'id' => $commande[0]
		   				] );
						break;
					} else {
						DB::update( 'update order_member set paid = :paid where id = :id' , [
			   				'paid' => 0,
			   				'id' => $commande[0]
		   				] );
					}
				}
			}
			header( 'Location: /admin/historique_client/'. $id );
		}

		$this->view( 'admin/historique_client', ['account_commandes' => $commandes, 'account_produits' => $produits, 'account_membre' => $membre] );

    }

//SUPPRIMER UNE COMMANDE D'UN MEMBRE
    public function supprimer_hist( int $idCommande ) {
		if( !isset( $_SESSION['Auth']['id'] ) || ( $_SESSION['Auth']['login'] != 'admin_obiwash' && $_SESSION['Auth']['password'] != '106e9ee5a8d402f04947809da4811c42') ) {
			header( 'Location: /connexion' );
		}

        $membre = DB::select( 'SELECT * FROM order_member WHERE id = ?', [$idCommande] );

        DB::delete ( 'delete from order_member where id = ?', [$idCommande] );

        header( 'Location: /admin/historique_client/'.$membre[0]['ref_Member'] );
    }

//GÉNÉRÉ UN MOT MOT DE PASSE
    private function generatePassword ($length = 8){
		$genpassword = "";
		$possible = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
		$i = 0;

		while ($i < $length) {
	    	$char = substr($possible, mt_rand(0, strlen($possible)-1), 1);
				if (!strstr($genpassword, $char)) {
					$genpassword .= $char;
					$i++;
	    		}
		}
		return $genpassword;
	}

//RE-ÉDITER UN MOT DE PASSE
	public function password( int $id ) {
		if( !isset( $_SESSION['Auth']['id'] ) || ( $_SESSION['Auth']['login'] != 'admin_django' && $_SESSION['Auth']['password'] != 'a25bc682b38f754336e634178e16c140') ) {
			header( 'Location: /connexion' );
		}

		$member = DB::select( 'select * from member where id = ?', [$id] );

		if ( !$member ) {
			header( 'Location: /admin/membres' );
		}

        $mail_member = $member[0]['mail_member'];
		$password2 = $this->generatePassword();
		$password = md5($password2);
		//$password = password_hash($password2, PASSWORD_DEFAULT );

		DB::update( 'update  member set password = :password  where id = :id' , [
				        'password' =>  $password ,
				        'id' => $id
				    ] );
		$to = $mail_member;
		$subject = 'Django Studio - Commandes | Renouvellement de vos identifiants';
		$message = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional //EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Django Studio - Vos identifiants</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
   <meta content="width=device-width">
    <style type="text/css">
    /* Fonts and Content */
    body, td { font-family: "Helvetica Neue", Arial, Helvetica, Geneva, sans-serif; font-size:14px; }
    body { background-color: #F2F0F0; margin: 0; padding: 0; -webkit-text-size-adjust:none; -ms-text-size-adjust:none; }
    h2{ padding-top:12px; /* ne fonctionnera pas sous Outlook 2007+ */color:#000000; font-size:22px; }
    p{margin: 15px 0;}

    @media only screen and (max-width: 480px) {

        table[class=w275], td[class=w275], img[class=w275] { width:135px !important; }
        table[class=w30], td[class=w30], img[class=w30] { width:10px !important; }
        table[class=w580], td[class=w580], img[class=w580] { width:280px !important; }
        table[class=w640], td[class=w640], img[class=w640] { width:300px !important; }
        img{ height:auto;}
         /*illisible, on passe donc sur 3 lignes */
        table[class=w180], td[class=w180], img[class=w180] {
            width:280px !important;
            display:block;
        }
        td[class=w20]{ display:none; }
    }

    </style>

</head>
<body style="margin:0px; padding:0px; -webkit-text-size-adjust:none;" style="background-color:#F2F0F0;">
    <table width="100%" cellpadding="0" cellspacing="0" border="0" style="background-color:#F2F0F0;">
        <tbody>
            <tr>
                <td align="center" bgcolor="#581E62">
                    <table  cellpadding="0" cellspacing="0" border="0">
                        <tbody>
                            <tr>
                                <td class="w640"  width="640" height="20"></td>
                            </tr>

                            <tr class="pagetoplogo">
	                            <td class="w640" width="640">
		                            <table class="w640" width="640" cellspacing="0" cellpadding="0" border="0" bgcolor="#F2F0F0">
			                            <tbody>
				                            <td class="w30" width="30">
					                            <img class="w30" style="text-decoration: none; display: block;" src="https://commandes.django.fr/img/jpg/bg-left-email.jpg" alt="bg-left-email" width="" height="" />
				                            </td>
				                            <td class="w580" width="580">
					                             <img class="w580" style="text-decoration: none; display: block;" src="https://commandes.django.fr/img/jpg/bg-top-email.jpg" alt="bg-top-email" width="" height="" />
				                            </td>
				                            <td class="w30" width="30">
					                             <img class="w30" style="text-decoration: none; display: block;" src="https://commandes.django.fr/img/jpg/bg-right-email.jpg" alt="bg-right-email" width="" height="" />
				                            </td>
			                            </tbody>
		                            </table>
	                            </td>
	                        <tr>
		                    <tr class="content">
			                    <td class="w640" width="640" bgcolor="#FFFFFF">
			                     	<table class="w640" width="640" cellspacing="0" cellpadding="0" border="0">
                                        <tbody>
                                            <tr>
                                                <td class="w30" width="30">
	                                                <img class="w30" style="text-decoration: none; display: block;" src="https://commandes.django.fr/img/gif/white-space.gif" alt="space" width="" height="" />
                                                </td>
                                                <td class="w580" width="580">
	                                                <p style="margin: 15px 0;">Bonjour,<br><br>Vous nous avez demander de vous générer un nouveau mot de passe.</p>
	                                                <p style="margin: 15px 0;">Veuillez trouver ci-après votre identifiant ainsi que votre nouveau mot de passe pour vous connecter à l\'utilitaire de commandes</p>
	                                                <p style="text-align:center; font-size: 14px; margin: 15px 0">
		                                                <a href="https://commandes.django.fr" title="Découvrir l\'utilitaire de commandes Django Studio" target="_blank" style="background-color: #841A59; border:none; color: #FFFFFF; cursor: pointer; font-size:18px; box-shadow: none!important; font-weight: bold; padding: 5px;"><button style="background-color: #841A59; border:none; color: #FFFFFF; cursor: pointer; font-size:18px; box-shadow: none!important; font-weight: bold; padding: 5px;">Commandes - Django Studio</button>
			                                            </a>
			                                        </p>
			                                        <p style="margin: 15px 0;">Nous restons bien évidemment à votre disposition pour vous accompagner.</p><br>
			                                        <p style="margin: 0 0 15px;">Votre identifiant : <b>' . $member[0]['login'] . '</b></p>
			                                        <p style="margin: 15px 0;">Votre mot de passe : <b>' . $password2 . '<b></p>
													<p style="margin: 15px 0">Merci de votre confiance.<br>L\'équipe de Django Studio</p>
			                                        <p style="margin: 15px 0;">
				                                        <em style="color: #938795; font-size: 10px; font-weight: normal;">
				                                        	Django Studio© 2018. Tous droits réservés. Application réalisée par <a href="https://gribouillenet.fr" target="_blank" title="Voir les autres réalisations du webmaster">Gribouillenet©</a>
				                                        </em>
				                                    </p>
			                                    </td>
                                                <td class="w30" width="30">
	                                                 <img class="w30" style="text-decoration: none; display: block;" src="https://commandes.django.fr/img/gif/white-space.gif" alt="space" width="" height="" />
                                                 </td>
                                            </tr>
                                        </tbody>
			                     	</table>
		                     	</td>
		                    </tr>
		                    <tr>
                                <td class="w640"  width="640" height="20"></td>
                            </tr>
			            </tbody>
			        </table>
			    </td>
			</tr>
		</tbody>
	</table>
</body>
</html>' ;
		$headers = 'From: production@obiwash.fr' . "\r\n" .	'Reply-To: no-reply@obiwash.fr' . "\r\n" . 'Content-Type: text/html; charset=utf-8' .
        'X-Mailer: PHP/' . phpversion();

		mail($to, $subject, $message, $headers);

        header( 'Location: /admin/membres' );
	}
}
