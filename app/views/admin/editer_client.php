<?php require($_SERVER['DOCUMENT_ROOT'].'/app/views/include/header_admin_membres.php'); ?>
<section class="page-inner clearfix">
    <header class="page-header d-flex flex-column justify-content-center align-items-center">
        <h1>Éditer une fiche client</h1>
    </header>
	<div class="container edit-client">
	<div class="row">
		<div class="col-12">
            <?php if ( isset( $data['erreur'] ) ) { ?>
    		    <div class="alert alert-danger alert-on"><?= $data['erreur']['champ_obligatoire'] ?></div>
    		<?php } else { ?>
    			<span class="champ-obligatoire d-inline-block">* Champs Obligatoires</span>
    		<?php } ?>
        </div>
	</div>
	<div class="row">
        <form class="form-fiche-client form-label form-enter" action="" method="post" class="p-y-3 p-x-2" enctype="multipart/form-data" novalidate>
    		<label for="name">Nom*</label>
    		<input type="text" name="name" class="form-control form-obligatoire" placeholder="Nom" value="<?php echo $data['member']['name']; ?>">
    		<?php if ( isset( $data['erreur']['name'] ) ) : ?>
                <div class="alert alert-danger alert-form"><?= $data['erreur']['name'] ?></div>
            <?php endif; ?>

    		<label for="first_name">Prénom*</label>
    		<input type="text" name="first_name" class="form-control form-obligatoire" placeholder="Prénom" value="<?php echo $data['member']['first_name']; ?>">
    		<?php if ( isset( $data['erreur']['first_name'] ) ) : ?>
                <div class="alert alert-danger alert-form"><?= $data['erreur']['first_name'] ?></div>
            <?php endif; ?>

    		<label for="company_service">Société/Service</label>
    		<input type="text" name="company_service" class="form-control form-obligatoire" placeholder="Société/Service" value="<?php echo $data['member']['company_service']; ?>">

    		<label for="address">Adresse*</label>
    		<input type="text" name="address" class="form-control form-obligatoire" placeholder="Adresse" value="<?php echo $data['member']['address']; ?>">
    		<?php if ( isset( $data['erreur']['address'] ) ) : ?>
                <div class="alert alert-danger alert-form"><?= $data['erreur']['address'] ?></div>
            <?php endif; ?>

    		<label for="address_supplement">Complément d'adresse</label>
    		<input type="text" name="address_supplement" class="form-control form-obligatoire" placeholder="Complément d'adresse" value="<?php echo $data['member']['address_supplement']; ?>">

    		<label for="code_postal">Code postal*</label>
    		<input type="text" name="code_postal" class="form-control form-obligatoire" placeholder="Code postal" maxlength="5" value="<?php echo $data['member']['code_postal']; ?>">
    		<?php if ( isset( $data['erreur']['code_postal'] ) ) : ?>
                <div class="alert alert-danger alert-form"><?= $data['erreur']['code_postal'] ?></div>
            <?php endif; ?>

    		<label for="city">Ville*</label>
    		<input type="text" name="city" class="form-control form-obligatoire" placeholder="Ville" value="<?php echo $data['member']['city']; ?>">
    		<?php if ( isset( $data['erreur']['city'] ) ) : ?>
                <div class="alert alert-danger alert-form"><?= $data['erreur']['city'] ?></div>
            <?php endif; ?>

    		<div class="form-group">
    			<label for="sel1">Selectionner dans la liste:</label>
    			<select id="sel1" class="form-control form-obligatoire" name="country">
    				<option value="" disabled selected>Choississez votre pays</option>
    				<optgroup label="France Métropolitaine">
    					<option value="France" <?php if ( $data['member']['country'] == 'France' ) echo "selected" ?>>
    						France
    					</option>
    				</optgroup>
    				<optgroup label="Outre-mer Zone 1">
    					<option value="Réunion" <?php if ( $data['member']['country'] == 'Réunion' ) echo "selected" ?>>
    						Réunion
    					</option>
    					<option value="Guadeloupe" <?php if ( $data['member']['country'] == 'Guadeloupe' ) echo "selected" ?>>
    						Guadeloupe
    					</option>
    					<option value="Martinique" <?php if ( $data['member']['country'] == 'Martinique' ) echo "selected" ?>>
    						Martinique
    					</option>
    					<option value="Mayotte" <?php if ( $data['member']['country'] == 'Mayotte' ) echo "selected" ?>>
    						Mayotte
    					</option>
    					<option value="Guyane" <?php if ( $data['member']['country'] == 'Guyane' ) echo "selected" ?>>
    						Guyane
    					</option>
    					<option value="Saint-Pierre et Miquelon" <?php if ( $data['member']['country'] == 'Saint-Pierre et Miquelon' ) echo "selected" ?>>
    						Saint-Pierre et Miquelon
    					</option>
    					<option value="Saint-Martin" <?php if ( $data['member']['country'] == 'Saint-Martin' ) echo "selected" ?>>
    						Saint-Martin
    					</option>
    					<option value="Saint-Barthélemy" <?php if ( $data['member']['country'] == 'Saint-Barthélemy' ) echo "selected" ?>>
    						Saint-Barthélemy
    					</option>
    				</optgroup>
    				<optgroup label="Outre-mer Zone 2">
    					<option value="Polynésie Française" <?php if ( $data['member']['country'] == 'Polynésie Française' ) echo "selected" ?>>
    						Polynésie Française
    					</option>
    					<option value="Wallis et Futuna" <?php if ( $data['member']['country'] == 'Wallis et Futuna' ) echo "selected" ?>>
    						Wallis et Futuna
    					</option>
    					<option value="Nouvelle Calédonie" <?php if ( $data['member']['country'] == 'Nouvelle Calédonie' ) echo "selected" ?>>
    						Nouvelle Calédonie
    					</option>
    					<option value="TAAF" <?php if ( $data['member']['country'] == 'TAAF' ) echo "selected" ?>>
    						TAAF
    					</option>
    				</optgroup>
    			</select>
    		</div>
    		<?php if ( isset( $data['erreur']['country'] ) ) : ?>
                <div class="alert alert-danger alert-form"><?= $data['erreur']['country'] ?></div>
            <?php endif; ?>

    		<label for="mail_member">Email du client*</label>
    		<input type="text" name="mail_member" class="form-control form-obligatoire" placeholder="Email du client" value="<?php echo $data['member']['mail_member']; ?>">
    		<?php if ( isset( $data['erreur']['mail_member'] ) ) : ?>
                <div class="alert alert-danger alert-form"><?= $data['erreur']['mail_member'] ?></div>
            <?php endif; ?>

    		<label for="tel">Téléphone*</label>
    		<input type="text" name="tel" class="form-control form-obligatoire" placeholder="Téléphone" maxlength="14" value="<?php echo $data['member']['tel']; ?>">
            <?php if ( isset( $data['erreur']['tel'] ) ) : ?>
                <div class="alert alert-danger alert-form"><?= $data['erreur']['tel'] ?></div>
            <?php endif; ?>

            <label for="login">Nom d'utilisateur*</label>
    		<input type="text" name="login" class="form-control form-obligatoire" placeholder="Nom d'utilisateur" value="<?php echo $data['member']['login']; ?>">
    		<?php if ( isset( $data['erreur']['login'] ) ) : ?>
                <div class="alert alert-danger alert-form"><?= $data['erreur']['login'] ?></div>
            <?php endif; ?>

            <input type="submit" class="btn btn-success" value="Enregistrer">
        </form>
        <div class="col-12"><a href="/admin/membres" class="link-come-back"><button>Retour</button></a></div>
    </div>
			</div>
</section>
<?php require($_SERVER['DOCUMENT_ROOT'].'/app/views/include/footer.php'); ?>
