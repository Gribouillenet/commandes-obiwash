<ul class="widget-media-sociaux d-none d-md-block">
    <li class="item-media d-inline-block"><a href="" title="Nous suivre sur Facebook"><i class="fab fa-facebook-square"></i></a></li>
    <!-- 	<li class="item-media d-inline-block"><a href="" title="Nous retrouver sur google plus"><i class="fab fa-google-plus-square"></i></a></li> -->
    <li class="item-media d-inline-block"><a href="" title="Voir toute notre actualité sur Linkedin"><i class="fab fa-linkedin"></i></a></li>
    <li class="item-media d-inline-block"><a href="" title="Découvrir Obiwash® sur youtube"><i class="fab fa-youtube"></i></a></li>
</ul>
<p class="widget-delivery"><i class="ti-truck"></i><span>Livraison gratuite<sup>*</sup></span></p>
<p class="widget-cgv d-none d-md-block"><a href="/conditions-generales-de-vente" title="Lire les conditions générales de vente">CGV</a></p>
<!-- <p class="widget-tel"><i class="ti-mobile"></i><a href="tel:00 00 00 00 00">00 00 00 00 00</a></p> -->