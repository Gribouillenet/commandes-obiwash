<div id="bandeau-icons" class="bandeau-icons container-fluid" data-id-commande="<?php if ($idCommande) {
    echo $idCommande;
} ?>">
    <div class="container">
        <div class="row">
            <div id="bandeau-edit" class="bandeau-edit col-sm-4 col-xs-4 d-flex justify-content-center align-items-center">
                <span class="d-none">informations</span>
                <i class="fas fa-edit"></i>
            </div>
            <div id="bandeau-money" class="bandeau-money col-sm-4 col-xs-4 d-flex justify-content-center align-items-center">
                <span class="d-none">paiement</span>
                <i class="fas fa-money-check"></i>
            </div>
            <div id="bandeau-check" class="bandeau-check col-sm-4 col-xs-4 d-flex justify-content-center align-items-center">
                <span class="d-none">validation</span>
                <i class="far fa-check-square"></i>
            </div>
        </div>
    </div>
</div>