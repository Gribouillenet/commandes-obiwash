<?php if($image): ?>
	<figure class="image-product"><img src="<?= $url . '/public/files/' . $image ?>" title="Image '. $title .'" alt="Image '. $title .'"</></figure>
<?php endif; ?>