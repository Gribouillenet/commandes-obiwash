<?php require($_SERVER['DOCUMENT_ROOT'].'/app/views/include/header.php'); ?>
    <section class="page-inner">
        <header class="page-header d-flex flex-column">
            <h1 class="text-xs-center">Validation de commande</h1>
        </header>

        <h3>Merci de payer votre commande cliquant sur le bouton PayPal</h3>
        <div id="bouton-paypal"></div>

    </section>
<?php require($_SERVER['DOCUMENT_ROOT'].'/app/views/include/footer.php'); ?>
