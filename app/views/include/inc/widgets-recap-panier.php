<?php if ($data['Panier']) {?>
<div class="container-panier content-recap-panier">
    <div class="container">
        <header class="header-recap-panier">
            <h3>Votre récapitulatif panier</h3>
            <button class="recap-close" title="Fermer votre récapitulatif panier">
                <i class="ti-close"></i>
                <span>Fermer</span>
            </button>
        </header>
        <div class="bootstrap-table table-responsive row">
            <table class="table table-striped col-12">
                <thead class="table-header">
                    <th scope="col">Description</th>
                    <th scope="col">Prix unitaire</th>
                    <th class="col-1" scope="col">Qté</th>
                    <th class="col-2" scope="col">Total</th>
                    <!-- <th class="col-1" scope="col"></th> -->
                </thead>
                <?php foreach ($data['Panier'] as $key => $paniers) :
                    $id = $paniers['id'];
                    $title = $paniers['title'];
                    $image = $paniers['image'];
                    $title_link = str_replace(' ', '_', $title);
                    $title_link = htmlentities($title_link, ENT_NOQUOTES, 'UTF-8');
                    $title_link = preg_replace('#&([A-za-z])(?:uml|circ|tilde|acute|grave|cedil|ring);#', '\1', $title_link);
                    $title_link = preg_replace('#&([A-za-z]{2})(?:lig);#', '\1', $title_link);
                    $title_link = preg_replace('#&[^;]+;#', '', $title_link);
                    $title_link = strtolower($title_link);
                    $description = $paniers['description'];
                    $price =  $paniers['price'];
                    $stock =  $paniers['stock'];
                    $url = "https://".$_SERVER['SERVER_NAME'];
                    $root = $_SERVER['DOCUMENT_ROOT']."/app/views/include";

            //var_dump($title);
                ?>


                <tr class="item-bucket-product item-bucket-product-<?= $id ?>">
                    <td class="recap-panier-description d-flex align-items-center"><?php include('image-product.php');?><p><b><?= $title ?></b></p>
                    </td>
                    <td class="price"><?= number_format($price, 2, ',', ' ') ?> €</td>
                    <td class="quantity"><?= $_SESSION['Panier'][$id] ?></td>
                    <td class="subtotal"><?= number_format($price * $_SESSION['Panier'][$id], 2, ',', ' ') ?> €</td>
                    <!-- <td class="action">
                        <a class="link-delete delPanier" href="/produits/suppression/<?php //echo $id; ?>" title="Supprimer"><i class="ti-trash"></i></a>
                    </td> -->
                </tr>
                <?php endforeach;?>
                <tr>
                    <td class="items-delivery-total" colspan="3">
                        <p class="frais-port">Livraison</p>
                        <p class="total-ttc"><strong>Votre Total</strong></p>
                    </td>
                    <td class="items-delivery-total-final" colspan="2">
                        <p class="frais-port">Gratuite<sup>*</sup></p>
                        <p class="total-ttc"><strong><?= number_format($panier->total() + $tarif_1, 2, ',', ' ') ?> €</strong></p>
                    </td>
                </tr>
            </table>
            <em>*Les frais de livraison seront recalculés suivant le type de livraison choisi lors de la finalisation de votre commande.</em>
            <div class="container-btn-view-bucket container-fluid d-flex justify-content-center mt-3">
                <a class="button btn-consulter-panier d-inline-block" href="/panier/index" title="Voir votre panier et passer commande">Accéder au panier<i class="ti-arrow-right"></i></a>
            </div>
        </div>
    </div>
</div>
<?php } else { ?>
<div class="content-recap-panier">
    <div class="container">
        <header class="header-recap-panier">
            <h3>Votre récapitulatif panier</h3>
            <button class="recap-close">
                <i class="ti-close"></i>
                <span>Fermer</span>
            </button>
        </header>
        <div class="container-alert">
            <p class="alert alert-warning">
                <i class="ti-alert"></i>Vous n'avez aucun produit d'ajouter à votre panier.
            </p>
        </div>
    </div>
</div>
<?php } ?>