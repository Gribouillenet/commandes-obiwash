<header id="masthead" class="site-header" role="banner">
    <div id="widgets-contact-reseaux" class="widgets-contact-reseaux">
        <div class="container d-flex justify-content-center justify-content-lg-end container-reseau-account">
                <?php include('widgets-reseaux.php');?>
                <nav class="widgets-panier-navigation">
                    <?php include($_SERVER['DOCUMENT_ROOT'].'/app/views/include/inc/widgets-panier.php');?>
                </nav>
        </div>
    </div>
    <div class="content-navigations container">
        <div class="container-navigation d-flex align-items-center justify-content-center align-content-center justify-content-md-start flex-column flex-lg-row">
            <a class="logo-site d-block" href="<?= $url_site ?>" class="navbar-brand" rel="home">
                <img alt="Obiwash® - La boutique - le mélangeur intelligent pour tous" src="<?= $url_siteCommande ?>/public/img/svg/obiwash.svg" />
                <span>Obiwash® - La boutique - le mélangeur intelligent pour tous</span>
            </a>
            <button id="menu-toggle" class="menu-toggle-mobile d-block d-sm-none" aria-controls="primary-menu" aria-expanded="false"><span>Menu</span></button>
            <nav class="primary-navigation">
                <ul id="menu-navigation-principale" class="menu-navigation-principale nav d-flex flex-column flex-md-row">
                    <li id="menu-item-1225" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-1225 notre-projet"><a title="Tout connaître ou presque sur Obiwash®" href="<?= $url_site ?>/obiwash/">Notre projet</a>
                        <ul class="sub-menu">
                            <li id="menu-item-1407" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1407 obiwash®"><a href="<?= $url_site ?>/obiwash/">Obiwash®</a></li>
                            <li id="menu-item-1223" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1223 comment-installer-mon-obiwash®"><a title="Comment faire fonctionner Obiwash®" href="<?= $url_site ?>/comment-installer-mon-obiwash/">Comment installer mon Obiwash® ?</a></li>
                            <li id="menu-item-1222" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1222 faq"><a title="Toutes les questions sur Obiwash®." href="<?= $url_site ?>/faq/">FAQ</a></li>
                        </ul>
                    </li>
                    <li id="menu-item-1229" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1229 commander"><a title="Acheter Obiwash® et ses pièces détachées" href="/">Commander</a></li>
                    <li id="menu-item-1226" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1226 qui-sommes-nous"><a title="En savoir plus sur l&#8217;équipe Obiwash®" href="<?= $url_site ?>/qui-sommes-nous/">Qui sommes-nous ?</a></li>
                    <li id="menu-item-1785" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-1785 en-un-mot"><a title="Toute l&#8217;actualité d&#8217;Obiwash® et les économies d&#8217;énergie" href="#">En un mot</a>
                        <ul class="sub-menu">
                            <li id="menu-item-1791" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1791 tous-les-articles"><a title="Voir tous les articles d&#8217;actualités et sur les économies d&#8217;énergies" href="<?= $url_site ?>/en-un-mot/">Tous les articles</a></li>
                            <li id="menu-item-1227" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-1227 actualites"><a title="Quoi de neuf sur d&#8217;Obiwash®" href="<?= $url_site ?>/category/actualites/">Actualités</a></li>
                            <li id="menu-item-1228" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-1228 economies-d&#039;energie"><a title="Des explications et des renseignements autour d&#8217;Obiwash®" href="<?= $url_site ?>/category/economies-denergie/">Économies d&#8217;énergie</a></li>
                            <li id="menu-item-1224" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1224 liens-utiles"><a title="Les partenaires d&#8217;Obiwash®" href="<?= $url_site ?>/liens-utiles/">Liens utiles</a></li>
                        </ul>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
</header>