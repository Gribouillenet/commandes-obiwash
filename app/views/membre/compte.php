<style>
    .easyPaginateNav {
        gap: 15px;
        margin-top: 15px;
    }

    .current {
        text-decoration: underline;
        font-weight: bold;
    }
</style>


<?php require($_SERVER['DOCUMENT_ROOT'].'/app/views/include/header.php');

    foreach ($data['membre'] as $key => $membre) :
            $first_name = $membre['first_name'];
            $name = $membre['name'];
            $tel = $membre['tel'];
            $mail = $membre['mail_member'];
            $login = $membre['login'];
    endforeach;
?>
<section class="page-inner clearfix page-account">
    <header class="page-header d-flex flex-column justify-content-center align-items-center">
        <h1 class="text-xs-center">Mon Compte</h1>
    </header>
    <div class="infos-membre">
        <div class="container">
            <div class="row">
                <p>Bonjour <b><?= $first_name.' '.$name ?></b>, bienvenu sur votre compte client Obiwash®</p>
                <p class="title-information">informations personnelles</p>
            </div><!-- .row -->
            <div class="row">
                <ul class="col-12 col-md-8 pl-md-0">
                    <li><span class="d-block">Téléphone</span><b class="d-block"><?= $tel ?></b></li>
                    <li><span class="d-block">Email</span><b class="d-block"><?= $mail ?></b></li>
                </ul>
                <div class="col-12 col-md-4 d-flex align-items-center justify-content-end pr-md-0">
                    <a class="button nav-link-account link-edit-profil d-flex justify-content-end align-items-center" href="/membre/editer"><i class="ti-pencil pr-3"></i>Éditer son profil</a>
                </div>
            </div>
        </div>
    </div>

    <div class="container-list-commandes container">
        <div class="row flex-column">
            <h2>Mes commandes déjà effectuées</h2>

            <ul class="list-commandes" id="pagger-list-commandes">
            <li class="head-commandes row d-flex align-items-center justify-content-between ">
                <span class="col-2">Date commande</span>
                <span class="col-8">Référence et désignation produit</span>
                <span class="col-2">pdf</span>
            </li>
            <?php
            // echo '<pre>';
            // var_dump($data['account_commandes']);
            // echo '</pre>';

            if ($data['account_commandes']) {
                foreach ($data['account_commandes'] as $key => $commandes) :

                // echo "Commande : $commandes<br>";

                $idCommande = $commandes[0];

                // echo "ID COMMANDE : $idCommande<br>";
                    
                $ref_member = $commandes['ref_Member'];
                $ref_products = explode(',', $commandes['ref_Products']);
                $ref_delivery = $commandes['ref_Delivery'];
                $created_at = $commandes['date_commande'];
                $date = $created_at;
                $date = wordwrap($date, 2, "/", 1);

                $liste_produit = array();
                $i = 0;

                foreach ($ref_products as $key => $ref_product) :
                    foreach ($data['account_produits'] as $key => $produits) :
                        $idProduit = $produits['id'];
                        $title = $produits['title'];

                        if ($idProduit ==  $ref_product) {
                            $liste_produit[$i] = $title;
                            $i += 1;
                        }
                    endforeach;
                endforeach;

                $paid = $commandes['paid'];
                // echo "PAID : $paid<br><br>";

                //if ($paid == '1'): ?>

            <li class="item-commandes row d-flex align-items-center justify-content-between line-item-<?= $idCommande ?>">
                
                <span class="col-2">
                    <b><?= $date ?></b>
                </span> 
                <span class="infos-commande col-8">   
                    <?php
                        if ($idCommande <= 9) {
                            echo '<span class="ref-item-product">Obi-' . $ref_delivery . '-' . $created_at . '-00000' . $idCommande . ' </span><sep>|</sep>';
                            for ($k = 0; $k <= $i; $k+=1) {
                                echo '<span class="item-product">' . $liste_produit[$k] . '</span> ';
                            }
                        } elseif ($idCommande <= 99) {
                            echo '<span class="ref-item-product">Obi-' . $ref_delivery . '-' . $created_at . '-00000' . $idCommande . '</span><sep>|</sep>';
                            for ($k = 0; $k <= $i; $k+=1) {
                                echo '<span class="item-product">' . $liste_produit[$k] . '</span> ';
                            }
                        } elseif ($idCommande <= 999) {
                            echo '<span class="ref-item-product">Obi-' . $ref_delivery . '-' . $created_at . '-00000' . $idCommande . ' </span><sep>|</sep>';
                            for ($k = 0; $k <= $i; $k+=1) {
                                echo '<span class="item-product">' . $liste_produit[$k] . '</span> ';
                            }
                        } elseif ($idCommande <= 9999) {
                            echo '<span class="ref-item-product">Obi-' . $ref_delivery . '-' . $created_at . '-00000' . $idCommande . ' </span><sep>|</sep>';
                            for ($k = 0; $k <= $i; $k+=1) {
                                echo '<span class="item-product">' . $liste_produit[$k] . '</span> ';
                            }
                        } elseif ($idCommande <= 99999) {
                            echo '<span class="ref-item-product">Obi-' . $ref_delivery . '-' . $created_at . '-00000' . $idCommande . ' </span><sep>|</sep>';
                            for ($k = 0; $k <= $i; $k+=1) {
                                echo '<span class="item-product">' . $liste_produit[$k] . '</span> ';
                            }
                        } elseif ($idCommande <= 999999) {
                            echo '<span class="ref-item-product">Obi-' . $ref_delivery . '-' . $created_at . '-00000' . $idCommande . ' </span><sep>|</sep>';
                            for ($k = 0; $k <= $i; $k+=1) {
                                echo '<span class="item-product">' . $liste_produit[$k] . '</span> ';
                            }
                        } ?>
                </span>
                <span class="col-2">
                <a class="item-commande-pdf" href="/commandes/telecharger/<?= $idCommande ?>" title="Télécharger" target="_blank"><i class="ti-download"></i></a>
                </span>
            </li>
            <?php //endif;
                endforeach; ?>
            <?php
            } else { ?>
            <li>
                <p>Vous n'avez pas encore effectué de commande ou votre commande est en cours de validation de paiement.</p>
            </li>
            <?php }?>
        </ul>
        </div>
    </div>
</section>
<?php require($_SERVER['DOCUMENT_ROOT'].'/app/views/include/footer_compte.php'); ?>>?>