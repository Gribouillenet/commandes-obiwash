<?php class Produits extends Controller {
//AFFICHAGE DES PRODUITS
	function index(){
		//RÉCAP PANIER
		$ids = $_SESSION['Panier'];

	        if ( empty($ids) ) {

	            $paniers = [];

	        } else {

	    		$ids = array_keys($ids);
	            $paniers = DB::select( 'SELECT * FROM products WHERE id IN (' . implode(',', $ids) . ')' );
        	}

		$livraisons = DB::select ('SELECT * FROM delivery');

		$id = $_SESSION['Auth']['id'];
 		$membre = DB::select ('SELECT * FROM member WHERE id = ?', [$id]);

 		//PRODUCTS
		$produits = DB::select( 'SELECT * FROM products' );

		foreach ( $produits as $key => $produit ) {
		  	$produits[$key]['description'] = nl2br( $produit['description'] );
	  	}

	  	$this->view( 'produits/index', ['Panier' => $paniers, 'produits' => $produits, 'membre' => $membre[0]] );
	}
//VUE D'UN PRODUIT
	function single( int $id_Products ){
		//RÉCAP PANIER
		$ids = $_SESSION['Panier'];

	        if ( empty($ids) ) {

	            $paniers = [];

	        } else {

	    		$ids = array_keys($ids);
	            $paniers = DB::select( 'SELECT * FROM products WHERE id IN (' . implode(',', $ids) . ')' );
        	}

		$livraisons = DB::select ('SELECT * FROM delivery');

		$id = $_SESSION['Auth']['id'];
 		$membre = DB::select ('SELECT * FROM member WHERE id = ?', [$id]);

 		//PRODUCTS

		$produits = DB::select( 'SELECT * FROM products WHERE id = ?', [$id_Products] );

		foreach ( $produits as $key => $produit ) {
		  	$produits[$key]['description'] = nl2br( $produit['description'] );
	  	}

	  	$this->view( 'produits/single', ['Panier' => $paniers, 'produits' => $produits, 'membre' => $membre[0]] );
	}
//AJOUT D'UN PRODUIT
	function ajout( int $id_Products ) {
		$json = array('error' => true);
		$produits = DB::select( 'SELECT * FROM products WHERE id = ?', [$id_Products] );

		if ($produits) {
			Paniers::add($produits[0]['id']);
			$json['error'] = false;
			$json['total'] = number_format(Paniers::total(), 2, ',', ' ');
			$json['count'] = Paniers::count();
			$json['message'] = 'Le produit a bien été ajouté à votre panier.';
		} else {
			$json['message'] = "Le produit n'a pas pu être ajouter à votre panier.";
		}
		echo json_encode($json);
	}
//SUPPRESSION D'UN PRODUIT
	function suppression( int $id_Products ) {
		$json = array('error' => true);
		$produits = $_SESSION['Panier'][$id_Products];

		if ($produits) {
			$json['error'] = false;
			Paniers::del($id_Products);
			$json['message'] = 'Le produit a bien été supprimé de votre panier.';
		} else {
			$json['message'] = "Ce produit n'existe pas.";
		}
		echo json_encode($json);
	}
//SUPPRESSION D'UN PRODUIT DANS LA MODIFICATION
	function suppression_modif( int $id_Produit, int $id_Commandes ) {
		$json = array('error' => true);
		$commandes = DB::select( 'SELECT * FROM order_member WHERE id = ?', [$id_Commandes] );
		$id_Products = explode(',', $commandes[0]['ref_Products']);
		$qty = explode(',', $commandes[0]['ref_Amount']);
		$id_facturation = $commandes[0]['ref_Invoices_address'];

		$json['error'] = false;

		$i = 0;
		foreach ( $id_Products as $key => $id_Product ) {
			if ( $id_Product == $id_Produit ) {
				Paniers::del($id_Produit);
				unset($id_Products[$i]);
				unset($qty[$i]);

				if ( $id_Products != NULL ) {
					DB::update( 'update order_member set ref_Products = :ref_Products, ref_Amount = :ref_Amount where id = :id', [
						'ref_Products' => implode(',', $id_Products),
						'ref_Amount' => implode(',', $qty),
						'id' => $id_Commandes,
					] );
				} else {
					DB::delete ('DELETE FROM order_member WHERE id = ?', [$id_Commandes]);
					DB::delete ('DELETE FROM invoices_address WHERE id = ?', [$id_facturation]);

					$json['error'] = true;
				}
			}
			$i += 1;
		}

		echo json_encode($json);
	}
//SUPPRESSION DES PRODUITS
	function suppressionAll() {
		$json = array('error' => true);
		$produits = $_SESSION['Panier'];

		foreach ( $produits as $produit ) {
			if ($produit) {
				$json['error'] = false;
			} else {
				$json['error'] = true;
			}
		}
		if (!$json['error']) {
			Paniers::delAll();
			$json['message'] = 'Les produits ont bien été supprimés de votre panier !';
		} else {
			$json['message'] = "Un des produits n'existe pas !";
		}
		echo json_encode($json);
	}
}
