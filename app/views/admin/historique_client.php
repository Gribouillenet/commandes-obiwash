<?php require($_SERVER['DOCUMENT_ROOT'].'/app/views/include/header_admin_membres.php');
    foreach( $data['account_membre'] as $key => $membre ) :
        $first_name = $membre['first_name'];
        $name = $membre['name'];
        $tel = $membre['tel'];
        $mail = $membre['mail_member'];
        $login = $membre['login'];
    endforeach;
?>
<section class="page-inner clearfix">
    <header class="page-header d-flex flex-column justify-content-center align-items-center">
		<h1 class="text-xs-center">Historique du client</h1>
    </header>
<style>
.switch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;
}

.switch input {
  opacity: 0;
  width: 0;
  height: 0;
}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 26px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #2196F3;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}
</style>
        <div class="container-list-commandes">
            <?php if ($data['account_commandes']) { ?>
                <form class="form-label-admin" action="" method="post" class="p-y-3 p-x-2" enctype="multipart/form-data" novalidate>
                    <ul class="list-commandes" id="pagger-list-commandes">
                        <?php
                        foreach( $data['account_commandes'] as $key => $commandes ) :

                            $idCommande = $commandes[0];
        					$ref_member = $commandes['ref_Member'];
        					$ref_products = explode(',', $commandes['ref_Products']);
        					$ref_delivery = $commandes['ref_Delivery'];
        					$created_at = $commandes['date_commande'];
                            $date = $created_at;
                            $date = wordwrap($date,2,"/",1);

        					$liste_produit = array();
                            $i = 0;
                            foreach( $ref_products as $key => $ref_product ) :
            					foreach( $data['account_produits'] as $key => $produits ) :
            						$idProduit = $produits['id'];
            						$title = $produits['title'];

            						if ( $idProduit ==  $ref_product ) {
            							$liste_produit[$i] = $title;
                                        $i += 1;
            						}
            					endforeach;
                            endforeach;

                            $paid = $commandes['paid'];

                        if( $commandes ):?>

                        <li class="item-commande line-item-<?= $idCommande ?>">

                        <article class="line-cmd d-flex justify-content-between">
                            <div class="infos-cmd col-8">
                                <p>Commande du : <b><?= $date ?></b></p>
        	                    <?php
        	                    if( $idCommande <= 9 ){
        	                        echo '<aside>RÉF. Obi-' . $ref_delivery . '-' . $created_at . '-00000' . $idCommande . ' <sep>|</sep>';
        							for ($k = 0; $k <= $i; $k+=1 ) {
        								echo '<span>' . $liste_produit[$k] . '</span> ';
        							}
        							echo '</aside>';
        	                    }
        	                    elseif( $idCommande <= 99 ){
        							echo '<aside>RÉF. Obi-' . $ref_delivery . '-' . $created_at . '-00000' . $idCommande . ' <sep>|</sep>';
        							for ($k = 0; $k <= $i; $k+=1 ) {
        								echo '<span>' . $liste_produit[$k] . '</span> ';
        							}
        							echo '</aside>';
        	                    }
        	                    elseif( $idCommande <= 999 ){
        							echo '<aside>RÉF. Obi-' . $ref_delivery . '-' . $created_at . '-00000' . $idCommande . ' <sep>|</sep>';
        							for ($k = 0; $k <= $i; $k+=1 ) {
        								echo '<span>' . $liste_produit[$k] . '</span> ';
        							}
        							echo '</aside>';
        	                    }
        	                    elseif( $idCommande <= 9999 ){
        							echo '<aside>RÉF. Obi-' . $ref_delivery . '-' . $created_at . '-00000' . $idCommande . ' <sep>|</sep>';
        							for ($k = 0; $k <= $i; $k+=1 ) {
        								echo '<span>' . $liste_produit[$k] . '</span> ';
        							}
        							echo '</aside>';
        	                    }
        	                    elseif( $idCommande <= 99999 ){
        							echo '<aside>RÉF. Obi-' . $ref_delivery . '-' . $created_at . '-00000' . $idCommande . ' <sep>|</sep>';
        							for ($k = 0; $k <= $i; $k+=1 ) {
        								echo '<span>' . $liste_produit[$k] . '</span> ';
        							}
        							echo '</aside>';
        	                    }
        	                    elseif( $idCommande <= 999999 ){
        							echo '<aside>RÉF. Obi-' . $ref_delivery . '-' . $created_at . '-00000' . $idCommande . ' <sep>|</sep>';
        							for ($k = 0; $k <= $i; $k+=1 ) {
        								echo '<span>' . $liste_produit[$k] . '</span> ';
        							}
        							echo '</aside>';
        	                    }
        	                    ?>
                            </div>
                            <div class="link-hist-button col-4">
                                <div class="row">
                                    <div class="link-spr col-6">
                                        <a class="link-delete" href="/admin/supprimer_hist/<?= $idCommande ?>" title="Supprimer" onclick="return confirm('Voulez-vous vraiment supprimer cet enregistrement ?')"><span class="col-12 p-0">Supprimer</span></a>
                                    </div>
                                    <div class="col-6">
                                        <label class="switch">
                                            <input type="checkbox" name="choix[]" value="<?= $idCommande ?>"
                                                <?php
                                                if ( $paid == 1 ) {
                                                   echo "checked";
                                                }
                        						?>
                        					>
                                            <span class="slider round"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <!---
                            <input type="checkbox" data-toggle="toggle" data-on="True" data-off="False" data-onstyle="success" data-offstyle="danger">
                            ---->
                        </article>

                        </li>
        			    <?php endif; endforeach;?>
                    </ul>
                    <input type="submit" class="btn btn-success" value="Enregistrer">
                </form>
            <?php } else { ?>
                <li><h2>Le client n'a pas encore passé de commande.</h2></li>
            <?php } ?>
        </div>
       <a href="/admin/membres" class="link-come-back"><button>Retour</button></a>
      	</section>
<?php require($_SERVER['DOCUMENT_ROOT'].'/app/views/include/footer_compte.php'); ?>
