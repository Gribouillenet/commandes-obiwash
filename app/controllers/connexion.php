<?php
class Connexion extends Controller {
//VERIFICATION SI COMPTE EXISTE
	private function accountExists() : array {
		$compte = DB::select('SELECT id, login, password FROM admin UNION SELECT id, login, password FROM member');

		if ( $compte ){
            for ($i = 0; $i < sizeof($compte); $i+=1) {
                if ($compte[$i]['login'] == $_POST['login'] && $compte[$i]['password'] == md5($_POST['password'])) {
                    return $compte[$i];
                }
            }
		    return [];
		}
		else {
			return [];
		}
	}
//CONNEXION
	public function index() {
	    if ( !empty( $_POST ) ) {
	        extract( $_POST );

	        $compte = $this->accountExists();
            //var_dump($compte);
			if ( $compte ) {
                $_SESSION['Auth'] = array(
                    'id' => $compte['id'],
                    'login' => $compte['login'],
                    'password' => $compte['password']
                );

                if ($compte['login'] == 'admin_obiwash') {
                    header( 'Location: /admin' );
                }
                else {
				    header( 'Location: /' );
                }
			}
			else {
				$erreur = 'Attention : Identifiants erronés ou manquants !';

			}

			$this->view( 'connexion/index', ['erreur' => $erreur] );
	    }

		$this->view( 'connexion/index' );
	}
//DECONNEXION
	public function deconnexion(){
		if( !isset( $_SESSION['Auth']['id'] ) ) {
			header( 'Location: /' );
		}

		$_SESSION = [];

		if (ini_get("session.use_cookies")) {
		    $params = session_get_cookie_params();
		    setcookie(session_name(), '', time() - 42000,
		        $params["path"], $params["domain"],
		        $params["secure"], $params["httponly"]
		    );
		}

		session_destroy();

		header( 'Location: /' );
	}
}
