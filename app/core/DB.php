<?php

class DB extends PDO
{
    public const DSN = 'mysql:dbname=commandesobi;host=af141110-002.privatesql:35154;charset=utf8';
    public const USER = 'cmdsobi_140519';
    public const PASSWORD = 'zVvEVkgP8NX5KumG';

    public function __construct()
    {
        try {
            parent::__construct(self::DSN, self::USER, self::PASSWORD);
        } catch (PDOException $e) {
            die('Erreur : ' . $e->getMessage());
        }
    }

    public static function select(string $query, array $params = []): array
    {
        $bdd = new DB();

        if ($params) {
            $req = $bdd->prepare($query);
            $req->execute($params);
        } else {
            $req = $bdd->query($query);
        }

        $data = $req->fetchAll();

        return $data;
    }

    public static function update(string $query, array $params = []): int
    {
        $bdd = new DB();

        if ($params) {
            $req = $bdd->prepare($query);
            $req->execute($params);
        } else {
            $req = $bdd->query($query);
        }

        $updated = $req->rowCount();

        return $updated;
    }

    public static function insert(string $query, array $params = []): int
    {
        $bdd = new DB();

        if ($params) {
            $req = $bdd->prepare($query);
            $req->execute($params);
        } else {
            $req = $bdd->query($query);
        }

        $inserted = $req->rowCount();

        return $inserted;
    }

    public static function delete(string $query, array $params = []): int
    {
        $bdd = new DB();

        if ($params) {
            $req = $bdd->prepare($query);
            $req->execute($params);
        } else {
            $req = $bdd->query($query);
        }

        $deleted = $req->rowCount();

        return $deleted;
    }
}
