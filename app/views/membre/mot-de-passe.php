<?php require($_SERVER['DOCUMENT_ROOT'].'/app/views/include/header-thanks.php'); ?>
<section class="page-inner clearfix">
    <header class="page-header d-flex justify-content-center align-items-center">
        <h1 class="text-xs-center">Votre inscription</h1>
    </header>
    <div class="container content-fiche-signin-thanks">
        <p>Votre mot de passe va être renouvelé automatiquement, merci de vérifier vos emails, y compris en notification ou spam.</p>
        <p>L'équipe d'Obiwash®</p>
        
        <div class="container-btn-view-bucket container-fluid d-flex justify-content-center mt-3">
            <a class="button btn-comeback-store" href="/connexion/index"><i class="ti-arrow-left"></i>Retour à la page de connexion</a>
        </div>
        
    </div>
</section>
<?php require($_SERVER['DOCUMENT_ROOT'].'/app/views/include/footer.php'); ?>