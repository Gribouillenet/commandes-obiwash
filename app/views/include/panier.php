<div class="container-panier container">
			<?php
		if ( $data['paniers'] ) { ?>
			<form class="form-enter" action="" method="post" class="p-y-3 p-x-2" enctype="multipart/form-data" novalidate>
				<table width="100%">
				  	<thead>
						<tr>
							<th>Image</th>
							<th>Libellé produit</th>
							<th>Description</th>
		                    <th>Prix Unitaire</th>
		                    <th>Qté</th>
		                    <th>Total</th>
		                    <th>Action</th>
	                  	</tr>
	                </thead>
		            <tbody>
						<?php
						foreach( $data['paniers'] as $key => $produit ) :
							$id = $produit['id'];
							$image = $produit['image'];
					        $title = $produit['title'];
					        $title_link = str_replace( ' ', '_', $title );
							$title_link = htmlentities($title_link, ENT_NOQUOTES, 'UTF-8');
					        $title_link = preg_replace('#&([A-za-z])(?:uml|circ|tilde|acute|grave|cedil|ring);#', '\1', $title_link);
							$title_link = preg_replace('#&([A-za-z]{2})(?:lig);#', '\1', $title_link);
							$title_link = preg_replace('#&[^;]+;#', '', $title_link);
					        $title_link = strtolower( $title_link );
					        $description = $produit['description'];
							$price =  $produit['price'];
							$stock =  $produit['stock'];

							$adresse = "http://".$_SERVER['SERVER_NAME'];
							$_SESSION['Auth']['adresse'] = $adresse;
						?>

						<tr>
							<?php
							if($image){
								echo '<td><img src="' . $adresse . '/public/files/' . $image . '" title="Image '. $title .'" alt="Image '. $title .'" </></td>';
							}
							?>
							<td><span class="name"><?= $title ?></sapn></td>
							<td><span class="description"><?= $description ?></sapn></td>
							<td><span class="price"><?= number_format($price, 2, ',', ' ') ?> €</sapn></td>
							<td><span class="quantity"><input type="number" name="Panier[quantity][<?= $id ?>]" value="<?= $_SESSION['Panier'][$id] ?>" min="1" max='<?= $stock ?>'></sapn></td>
							<td><span class="subtotal"><?= number_format($price * $_SESSION['Panier'][$id], 2, ',', ' ') ?> €</sapn></td>
							<td>
								<span class="action">
									<a class="link-delete delPanier" href="/produits/suppression/<?= $id ?>" title="Supprimer"><span>Supprimer</span></a>
								</sapn>
							</td>
						</tr>
						<?php endforeach;?>

						<tr>
		                  	<td colspan="3">
			                    <p>Sous-total : <span class="total"><?= number_format($panier->total(), 2, ',', ' ') ?> €</sapn></p>
								<?php
								foreach( $data['livraisons'] as $key => $livraison ) :
									$type = $livraison['type'];
									if( $type == 'Retrait sur place') {
										$tarif_1 =  $livraison['tarif_1'];
									}
								endforeach;
								?>
								<p>Livraison : <span class="frais-port">Gratuite *</sapn></p>
								<p>Prix Total : <span class="total-ttc"><?= number_format($panier->total() + $tarif_1, 2, ',', ' ') ?> €</sapn></p>
			                </td>
		                </tr>
						<tr>
							<td colspan="4">
								<input type="submit" class="btn btn-record" value="Recalculer"/>
								<a class="link-come-back delPanier" href="/produits/suppressionAll" title="Supprimer tous le panier"><button>Supprimer le panier</button></a>
							</td>
						</tr>
<!---
						<tr>
							<td colspan="4">
								<select name="Livraison" size="1">
									<?php
									foreach( $data['livraisons'] as $key => $livraison ) :
										$id = $livraison['id'];
										$type = $livraison['type'];
										if( $type == $_SESSION['Livraison']) {
											$tarif_1 =  $livraison['tarif_1'];
											$tarif_2 =  $livraison['tarif_2'];
											$tarif_3 =  $livraison['tarif_3'];
										}
									?>
									<option value="<?= $type ?>"><?= $type ?></option>
									<?php endforeach;?>
								</select>

								<INPUT type= "radio" name="tarif" value="tarif_1" checked> <?= $tarif_1 ?> €
								<INPUT type= "radio" name="tarif" value="tarif_2"> <?= $tarif_2 ?> €
								<INPUT type= "radio" name="tarif" value="tarif_3"> <?= $tarif_3 ?> €
							</td>
						</tr>
--->
					</tbody>
				</table>
			</form>

			<p>*frais de port pour la France métropolitaine. Pour l’international et les DOM/TOM, les frais seront calculés lorsque vous saisirez l’adresse de livraison.</p>
			<a class="link-come-back" href="/livraison/info_personnelle"><button>Passer la commande</button></a>
		<?php } else { ?>
			<h4 style="color:red;">Votre panier est vide !</h4>
		<?php } ?>
		<a class="link-come-back" href="/"><button>Retour</button></a>
		</div>