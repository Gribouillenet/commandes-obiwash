<style>
	table {
		border: 0px !important;
	}
	
	textarea[name="description"], textarea[name="data_sheet"] {
		width: 100%;
		resize: vertical;
		min-height: 150px;
	}

	.submit-button {
		color: #fff;
		background-color: #28a745;
		border-color: #28a745;
		display: inline-block;
		font-weight: 400;
		text-align: center;
		white-space: nowrap;
		vertical-align: middle;
		user-select: none;
		border: 1px solid transparent;
		padding: 0.375rem 0.75rem;
		font-size: 1rem;
		line-height: 1.5;
		border-radius: 0.25rem;
		transition: color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out;
		margin: 20px 0;
	}

	.submit-button:hover {
		color: #28a745 !important;
		cursor: pointer;
		background-color: white;
		border-color: #28a745;
	}

</style>
<?php require($_SERVER['DOCUMENT_ROOT'].'/app/views/include/header_admin.php'); ?>
	<section class="page-inner clearfix">
		<header class="page-header d-flex flex-column justify-content-center align-items-center">
			<h1 class="text-xs-center">Gestion des produits</h1>
		</header>
		<div class="container gestion-produit">

		<div class="row">
			<div class="col-12">
				<h2 class="p-0">Modifier frais</h2>
			</div>

			<?php 

				if(isset($_POST['charge-1'])) {
					for($i = 1; $i <= count($_POST); $i++) {	
						$charge = $_POST['charge-' . $i];
						$query = "update charges set charge = $charge where id = $i";
						DB::update($query);
					}
				}
				?>
			
			<form class="form-label" action="#" method="post" enctype="multipart/form-data">
				<?php
					$frais = DB::select('select * from charges');

					echo '<table>';
					echo '<tr>';
					foreach ($frais as $f) {
						echo "<th>" . $f['method'] . "</th>";
					}
					echo '</tr>';

					echo '<tr>';
					foreach ($frais as $f) {
						$name = "charge-" . $f['id'];

						echo "<td><input type='number' name='$name' value='" . $f['charge'] . "' min='0' step='0.1'></td>";
					}
					echo '</tr>';

					echo '</table>';
				?>
				<input type="submit" class="submit-button" value="Mettre à jour">
			</form>
		</div>

		<div class="row">
			<div class="col-12">
				<h2 class="p-0">Modifier tarifs de livraison</h2>
			</div>

			<?php 

				if(isset($_POST['1-1'])) {
					$limit = count($_POST) / 4;

					for($i = 1; $i <= $limit; $i++) {
						$value1 = $_POST["$i-1"];
						$value2 = $_POST["$i-2"];
						$value3 = $_POST["$i-3"];

						$query = "update delivery set tarif_1 = $value1, tarif_2 = $value2, tarif_3 = $value3 where id = $i";
						DB::update($query);
					}
				}
			?>

			<form class="form-label" action="#" method="post" enctype="multipart/form-data">
				<?php 
					$tarifs = DB::select('select * from delivery order by id asc');
					$cpt = 1;

					echo '<table>';
					echo '<tr><th></th><th>Tarif 1</th><th>Tarif 2</th><th>Tarif 3</th></tr>';

					foreach ($tarifs as $tarif) {
						echo '<tr>';
						echo '<td>' . $tarif[1] . '</td>';
						echo "<input type='hidden' name='column-$cpt' value='" . $tarif[1] . "'>";

						for ($i = 1; $i <= 3; $i++) {
							echo "<td><input type='number' name='$cpt-$i' value='" . $tarif["tarif_$i"] . "' step='0.1' min='0'></td>";
						}

						$cpt++;
						echo '</tr>';
					}

					echo '</table>';
				?>
				<input type="submit" class="submit-button" value="Mettre à jour">
			</form>

		</div>

	    <div class="row">
		    <div class="col-12">
			    <h2 class="p-0">Ajouter ou modifier un produit</h2>
	            <?php if ( isset( $data['erreur']['title'] ) || isset( $data['erreur']['description'] ) ) { ?>
	                <div class="alert alert-danger alert-on"><?= $data['erreur']['champ_obligatoire'] ?></div>
	            <?php } else { ?>
	                <span class="champ-obligatoire d-inline-block">* Champs Obligatoires</span>
	            <?php } ?>
		    </div>

	    </div>
	    <div class="row">
	        <div class="col-md-6">
	            <form class="form-label" action="/admin" method="post" class="p-y-3 p-x-2" enctype="multipart/form-data" novalidate>
		            
					<label>Nom du produit*</label>
					<input type="text" name="title" class="form-control form-obligatoire form-enter" placeholder="Nom du produit" value="<?= $data['produits']['title'] ?>" onkeypress="refuserToucheEntree(event)">
					<?php if ( isset( $data['erreur']['title'] ) ) : ?>
						<div class="alert alert-danger alert-form"><?= $data['erreur']['title'] ?></div>
					<?php endif; ?>

					<label for="description">Description du produit*</label>
					<textarea id="description" name="description" class="form-control form-obligatoire" placeholder="Descriptif du produit"><?= $data['produits']['description'] ?></textarea>
					<?php if ( isset( $data['erreur']['description'] ) ) : ?>
						<div class="alert alert-danger alert-form"><?= $data['erreur']['description'] ?></div>
					<?php endif; ?>

					<label for="prix-produit">Prix du produit*</label>
					<input id="prix-produit" type="text" name="price" class="form-control form-obligatoire form-enter" placeholder="Prix du produit : 9.99" value="<?= $data['produits']['price'] ?>" onkeypress="refuserToucheEntree(event)">
					<?php if ( isset( $data['erreur']['price'] ) ) : ?>
						<div class="alert alert-danger alert-form"><?= $data['erreur']['price'] ?></div>
					<?php endif; ?>

					<label>Stock du produit*</label>
					<input type="number" name="stock" class="form-control form-obligatoire form-enter" placeholder="Stock du produit" value="<?= $data['produits']['stock'] ?>" min="0" onkeypress="refuserToucheEntree(event)">
					<?php if ( isset( $data['erreur']['stock'] ) ) : ?>
						<div class="alert alert-danger alert-form"><?= $data['erreur']['stock'] ?></div>
					<?php endif;?>

					<label>Poid du produit en kg*</label>
					<input type="text" name="weight" class="form-control form-obligatoire form-enter" placeholder="Poid du produit en kg : 0.5" value="<?= $data['produits']['weight'] ?>" onkeypress="refuserToucheEntree(event)">
					<?php if ( isset( $data['erreur']['weight'] ) ) : ?>
						<div class="alert alert-danger alert-form"><?= $data['erreur']['weight'] ?></div>
					<?php endif; ?>


			<!-- <label>Produit fini HxLxP*</label>
			<input type="text" name="finished_product" class="form-control form-obligatoire form-enter" placeholder="Produit fini HxLxP : 170 x 180 x 75 (mm)" value="<?// echo $data['produits']['finished_product'] ?>" onkeypress="refuserToucheEntree(event)">
			<?php //if ( isset( $data['erreur']['finished_product'] ) ) : ?>
				<div class="alert alert-danger alert-form"><?php //echo $data['erreur']['finished_product'] ?></div>
			<?php //endif; ?>

			<label>Produit emballé*</label>
			<input type="text" name="packed_product" class="form-control form-obligatoire form-enter" placeholder="Produit emballé : 280 x 220 x 200 (mm)" value="<?// echo $data['produits']['packed_product'] ?>" onkeypress="refuserToucheEntree(event)">
			<?php //if ( isset( $data['erreur']['packed_product'] ) ) : ?>
				<div class="alert alert-danger alert-form"><?// echo $data['erreur']['packed_product'] ?></div>
			<?php //endif; ?> -->

					<label for="data_sheet">Fiche technique du produit*</label>
					<textarea id="data_sheet" name="data_sheet" class="form-control form-obligatoire" placeholder="Nom colonne : valeur;"><?= $data['produits']['data_sheet'] ?></textarea>
					<?php if ( isset( $data['erreur']['data_sheet'] ) ) : ?>
						<div class="alert alert-danger alert-form"><?= $data['erreur']['data_sheet'] ?></div>
					<?php endif; ?>

					<h3>Fichiers numériques<br><em>(ne sont acceptés que les types : png, jpeg et faisant au maximum 500Ko)</em></h3>
		            <div class="upload-file file-1">
		                <input id="input-file" name="fichier" type="file" class="form-control input-file" placeholder="Joindre un fichier" value="<?php if ( isset( $_POST['fichier'] ) ) echo $_POST['fichier']; ?>" >
		                <div id="file-select-button" class="file-select-button"><span class="btn-file-dwnl">Sélectionnez un fichier</span><i class="fa fa-upload"></i></div>
		                 <div class="upload-delete-file">
		                    <i id="delete-file" class="form-control delete-file fa-delete"></i>
		                 </div>
		            </div>
		            <p class="file-return"></p>
		            <?php if ( isset( $data['erreur']['fichier'] ) ) : ?>
		                <div class="alert alert-danger alert-file alert-form"><?= $data['erreur']['fichier'] ?></div>
		            <?php endif; ?>

					<input type="submit" class="submit-button" value="Publier">
	            </form>
	        </div>
	        <div class="col-md-6 bootstrap-table">
	            <table class="table table-striped">
                    <tbody>
                        <?php
                        foreach($data['produits'] as $produits) :
                        ?>
                        <tr>
	                        <th scope="row">Ref-<?= $produits['id'] ?></th>
	                        <td><?= $produits['title'] ?></td>
	                        <td><a href="/admin/editer/<?= $produits['id'] ?>" class="edit"><span>Éditer</span></a></td>
	                        <td><a href="/admin/supprimer/<?= $produits['id'] ?>" class="delete" onclick="return confirm('Voulez-vous vraiment supprimer cet enregistrement ?')"><span>Supprimer</span></a></td>
                        </tr>
                        <?php
                        endforeach;
                        ?>
                    </tbody>
	            </table>
	        </div>
	    </div>
					</div>
	</section>
<?php require($_SERVER['DOCUMENT_ROOT'].'/app/views/include/footer.php'); ?>
