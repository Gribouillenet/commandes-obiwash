		</main>
		<footer id="container-footer" class="container-footer">
		    <div class="container">
		        <div class="row">
		            <div class="col-12 col-lg-4 d-flex flex-column align-items-center">
		                <figure class="logo-brand-footer d-flex align-items-end"><img src="/public/img/svg/obiwash-logo-blc.svg" alt="obiwash-logo-blc" /><span class="d-block">Obiwash® - La boutique - le mélangeur intelligent pour tous</span></figure>
		                <figure class="logo-ulule"><a href="https://www.obiwash.gribdev.eu/contributeurs-ulule/"><img src="/public/img/png/ulule-stamp-fr.png" alt="ulule-stamp-fr" /></a><span>Merci à vous !</span></figure>
		            </div>
		            <nav class="col-12 col-lg-2 container-nav-page-footer d-none d-lg-block">
		                <ul class="nav-page-footer">
		                    <li class="item-nav-page-footer"><a href="<?= $url_site ?>" title="En savoir plus sur Obiwash">Obiwash©</li>
		                    <li class="item-nav-page-footer"><a href="<?= $url_siteCommande ?>" title="Commander un Obiwash© ou des pièces détachées">Commander</li>
		                    <li class="item-nav-page-footer"><a href="<?= $url_site ?>/qui-sommes-nous" title="Découvrir l'histoire d'Obiwash©">Qui sommes-nous ?</li>
		                    <li class="item-nav-page-footer"><a href="<?= $url_site ?>/actualites" title="Quoi de neuf chez Obiwash©">Actualités</li>
		                </ul>
		            </nav>
		            <nav class="col-12 col-lg-4 container-nav-mentions-footer">
		                <ul class="nav-mentions-footer">
		                    <li class="item-mentions-footer"><a href="<?= $url_site ?>/conditions-generales-dutilisation-du-site" title="Lire les CGU">Conditions Générales d’Utilisation du site</a></li>
		                    <li class="item-mentions-footer"><a href="<?= $url_site ?>/conditions-generales-de-vente" title="Lire les CGV">Conditions Générales de Vente</a></li>
		                    <li class="item-mentions-footer"><a href="<?= $url_site ?>/faq" title="Voir les questions fréquemment posées">FAQ</a></li>
		                    <li class="item-mentions-footer"><a href="<?= $url_site ?>/guide-dinstallation-et-dutilisation-obiwash/" target="_blank">Guide d’installation et d’utilisations</a></li>
		                    <li class="item-copyright-footer">Obiwash© 2020. Tous droits réservés. Site réalisé par <a class="link-copyright" href="https://gribouillenet.fr" target="_blank" title="Voir le site du créateur de la solution">Gribouillenet©</a></li>
		                    <li class="item-copyright-footer"><sup>*</sup>Voir conditions générales de ventes</li>
		                </ul>
		            </nav>
		            <div class="col-12 col-lg-2 container-payment-social">
		                <button class="return-top d-flex align-items-center justify-content-center"><i class="ti-angle-up"></i><span class="d-block">Retour en haut</span></button>
		                <h4 class="payment-title d-flex flex-column flex-lg-row"><i class="ti-lock"></i>Paiements sécurisés</h4>
		                <ul class="payment-mode">
		                    <li class="item-payment type-paypal d-inline-block"><i class="fab fa-cc-paypal"></i><span class="d-block">Paypal</span></li>
		                    <li class="item-payment type-mastercard d-inline-block"><i class="fab fa-cc-mastercard"></i><span class="d-block">CB MasterCard</li>
		                    <li class="item-payment type-visa d-inline-block"><i class="fab fa-cc-visa"></i><span class="d-block">CB Visa</li>
		                    <li class="item-payment type-cheque d-inline-block"><i class="fas fa-money-check"></i><span class="d-block">Chèque</li>
		                </ul>
		                <ul class="social-media">
		                    <li class="item-social item-facebook d-inline-block"><a href="" title="Nous suivre sur Facebook" target="_blank"><i class="fab fa-facebook-square"></i><span class="d-block">Facebook</span></a></li>
		                    <li class="item-social item-linkedin d-inline-block"><a href="" title="Nous suivre sur Linkedin" target="_blank"><i class="fab fa-linkedin"></i><span class="d-block">Linkedin</span></a></li>
		                    <li class="item-social item-youtube d-inline-block"><a href="" title="Voir nos vidéos sur Youtube" target="_blank"><i class="fab fa-youtube-square"></i><span class="d-block">Youtube</span></a></li>
		                </ul>
		            </div>
		        </div>
		    </div>
		</footer>