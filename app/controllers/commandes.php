<?php
class Commandes extends Controller{
// COMMANDE EN COURS
	public function index(){
		if( !isset( $_SESSION['Auth']['id'] ) ) {
			header( 'Location: /connexion' );
		}

		$id_membre = $_SESSION['Auth']['id'];
		$membre = DB::select ('SELECT * FROM member WHERE id = ?', [$id_membre]);

		$ids_products = DB::select( 'SELECT ref_Products FROM order_member WHERE (ref_Member = ?) ORDER BY id DESC LIMIT 1', [$id_membre] );
        $produits = DB::select( 'SELECT * FROM products WHERE id IN ('.$ids_products[0]['ref_Products'].')' );

		$commandes = DB::select( 'SELECT * FROM order_member, member, delivery WHERE
		order_member.id = (SELECT MAX(id) FROM order_member WHERE ref_Member = ?) AND ref_Delivery = delivery.id AND member.id = ?', [$id_membre,$id_membre] );

		$facturation = DB::select( 'SELECT * FROM invoices_address WHERE invoices_address.id = (SELECT MAX(id) FROM invoices_address WHERE ref_Member = ?)', [$id_membre] );

		foreach ( $commandes as $key => $commande ){
			$dateCommande = date_create( $commande['date_commande'] );
			$commandes[$key]['date_commande'] = date_format( $dateCommande, 'dmy' );
		}

		$this->view( 'commandes/index', ['commandes' => $commandes, 'produits' => $produits, 'facturation' => $facturation[0], 'membre' => $membre[0]] );
	}
// VALIDATION DE LA COMMANDE
	public function validation(){
		if( !isset( $_SESSION['Auth']['id'] ) ) {
			header( 'Location: /connexion' );
		}

		$id_membre = $_SESSION['Auth']['id'];

		$commande =  DB::select( 'SELECT * FROM order_member, member WHERE
		order_member.id = (SELECT MAX(id) FROM order_member WHERE ref_Member = ?) AND member.id = ?', [$id_membre,$id_membre] );

	    foreach ( $commande as $key => $validation ){
			$created_at = date_create( $validation['date_commande'] );
			$created_at = date_format( $created_at, 'dmy' );
			$idCommande = $validation[0];
			$mode_paiement = $validation['mode_paiement'];
			$ref_member = $validation['ref_Member'];
			$commercial_first_name = $validation['first_name'];
			$commercial_name = $validation['name'];
			$commercial_tel = $validation['tel'];
			$commercial_mail = $validation['mail_member'];

			if( $idCommande <= 9 ){
				$ref_commande = 'RÉF. Obi-' . $ref_member . '-' . $created_at . '-00000' .$idCommande;
			}
			elseif( $idCommande <= 99 ){
				$ref_commande = 'RÉF. Obi-' . $ref_member . '-' . $created_at . '-0000' . $idCommande;
			}
			elseif( $idCommande <= 999 ){
				$ref_commande = 'RÉF. Obi-' . $ref_member . '-' . $created_at . '-000' . $idCommande;
			}
			elseif( $idCommande <= 9999 ){
				$ref_commande = 'RÉF. Obi-' . $ref_member . '-' . $created_at . '-00' . $idCommande;
			}
			elseif( $idCommande <= 99999 ){
				$ref_commande = 'RÉF. Obi-' . $ref_member . '-' . $created_at . '-0' . $idCommande;
			}
			elseif( $idCommande <= 999999 ){
				$ref_commande = 'RÉF. Obi-' . $ref_member . '-' . $created_at . '-' . $idCommande;
			}

			$subject = 'Nouvelle commande : ' . $ref_commande . ' - ' . $validation['nom_campagne'];
		}

		$mail_admin = 'testappligrib@gmail.com';
		$message = $_POST['commande'];

		//Attention ne pas oublier de remplacer la valeur de $mail_admin production@obiwash.fr une fois mis en ligne
		$to = ''. $commercial_mail .','. $mail_admin .'';
		$eol="\r\n";
		$headers = 'From: Obiwash - Commandes <no-reply@obiwash.fr>' . $eol;
		$headers .= 'Reply-To: no-reply@obiwash.fr' . $eol;
		$headers .= 'Return-Path: no-reply@obiwash.fr' . $eol;
		$headers .= 'MIME-Version: 1.0' . $eol;
		$headers .= 'Content-Type: text/html; charset=utf-8' . $eol;
		
		$ids_products = DB::select( 'SELECT ref_Products FROM order_member WHERE (ref_Member = ?) ORDER BY id DESC LIMIT 1', [$id_membre] );
		$produits = DB::select( 'SELECT * FROM products WHERE id IN ('.$ids_products[0]['ref_Products'].')' );
		$qty_products = DB::select( 'SELECT ref_Amount FROM order_member WHERE (ref_Member = ?) ORDER BY id DESC LIMIT 1', [$id_membre] );
		$quantites = explode(',', $qty_products[0]['ref_Amount']);

		$valide = true;
		$i = 0;
		foreach( $produits as $produit ) :
			$id = $produit['id'];
			$stock = $produit['stock'];
			$stock -= $quantites[$i];
			$url = 'https://commandes.obiwash.gribdev.eu/';
			
			if ( $stock < 0 ) {
				$this->view( 'commandes/validation');
				echo '
				<section class="page-inner">
					<header class="page-header flex-column justify-content-center align-items-center"><h1>Erreur</h1>
					</header>
					<div class="container message-error">
					<p>Votre commande n\'a pas pu être envoyé en raison de l\'épuisement d\'un des produits. Nous nous en excusons, merci de nous contacter.
						
					</p>
					<a class="button btn-comeback-store" href="'. $url . '" title="Revenir à la page des produits"><i class="ti-arrow-left"></i>Retour à la boutique</a>
					<a class="button d-inline-block" href="'. $url . '/contact-sav" title="Revenir à la page des produits">Contactez-nous</a>
					
					</div>
				</section>
				';
				require($_SERVER['DOCUMENT_ROOT'].'/app/views/include/footer.php');
				$valide = false;
			}

			$i++;
		endforeach;
		if ($valide) {
			//FONCTION ENVOI EMAIL
			if( mail( $to,$subject,$message,$headers ) ){

				$i = 0;
				foreach( $produits as $produit ) :
					$id = $produit['id'];
					$stock = $produit['stock'];
					$stock -= $quantites[$i];
					$url = 'https://commandes.obiwash.gribdev.eu/';
					DB::update( 'update products set stock = :stock where id = :id', [
	                    'stock' => $stock,
	                    'id' => $id
	                ] );

	                $i++;
	            endforeach;

				Paniers::delAll();

				$this->view( 'commandes/validation', ['validation' => $validation] );

				
					echo '<section class="page-inner"><header class="page-header d-flex flex-column justify-content-center align-items-center"><h1>Validation de commande</h1></header>'; ?>
					<?php require($_SERVER['DOCUMENT_ROOT'].'/app/views/include/bandeau-icons.php'); ?>
					<?php
					echo '<div class="container validation-commande"><h4>Merci pour votre commande</h4>';
					
					if( $mode_paiement == 'cb') {
						// PAYPLUG
						require_once($_SERVER['DOCUMENT_ROOT'] . '/app/core/set_payplug.php');
					}
					
					if ( $mode_paiement == 'paypal' ) {
						echo '<h4>Merci de payer votre commande cliquant sur le bouton PayPal</h4>
						<div id="bouton-paypal"></div>';
					}
					if ( $mode_paiement == 'virement' ) {
						echo '<p>Veuillez nous faire parvenir votre réglement par virement bancaire à l’aide des coordonnées suivantes :</p><p><b>PONT HERVE HPNT-SYSTEMES - FR76 1080 7000 6452 4212 4495 620 - BPBFC CRECHES SUR SAONE</b></p><p>Nous traiterons votre commande dès réception de votre paiement.</p><p>Nous reviendrons vers vous par email dès qu’elle sera expédiée.</p>';
					}
					if ( $mode_paiement == 'cheque' ) {
						echo '<p>Veuillez nous faire parvenir votre réglement à l’ordre de HPNT Systems à l’adresse suivante :</p>
						<p><b>HPNT Systems - Cidex 715bis - 29 imp des rochettes - 71570 leynes</b></p><p>Nous traiterons votre commande dès réception de votre paiement.</p><p>Nous reviendrons vers vous par email dès qu’elle sera expédiée.</p>';
					}
					// if ( $mode_paiement == 'cb' ) {
					// 	echo '<p>Elle va être validée par nos services.</p><p>Nous allons vous envoyé un ordre de paiement par email. Dès que celui-ci nous sera parvenu,</p><p>nous reviendrons vers vous par email pour vous notifier de l’expédition votre commande.</p>';
					// }

					echo '<p>À bientôt sur la boutique obiwash.fr</p><h4>Toute l’équipe d’Obiwash<exp>®</exp></h4><a class="button btn-comeback-store" href="/"><i class="ti-arrow-left"></i>Retour à la boutique</a></section></div>';
					require($_SERVER['DOCUMENT_ROOT'].'/app/views/include/footer.php');
				
			}
			else{
				
				// header( 'Location: /erreur' );
				$this->view( 'commandes/erreur');
				echo '
				<section class="page-inner">
				<header class="page-header flex-column justify-content-center align-items-center"><h1>Erreur</h1>
				</header>
				<div class="container message-error">
				<p>Votre email n\'a pas pu être envoyé. Merci de nous contacter au besoin.</p>
				<a class="button btn-comeback-store" href="'. $url . '"><i class="ti-arrow-left"></i>Retour à la boutique</a>
				</div>
			</section>';
				require($_SERVER['DOCUMENT_ROOT'].'/app/views/include/footer.php');
			}
		}
		$this->view( 'commandes/validation', ['validation' => $validation] );
	}
// ANNULATION DE LA COMMANDE
	public function annulation(int $id_COMD ){
		if( !isset( $_SESSION['Auth']['id'] ) ) {
			header( 'Location: /connexion' );
		}

		$commandes = DB::select( 'SELECT * FROM order_member WHERE id = ?', [$id_COMD] );
		$id_facturation = $commandes[0]['ref_Invoices_address'];

		DB::delete ('DELETE FROM order_member WHERE id = ?', [$id_COMD]);
		DB::delete ('DELETE FROM invoices_address WHERE id = ?', [$id_facturation]);

		header( 'Location: /panier/index' );
	}
// MODIFICATION DE COMMANDE EN COURS
	public function modifier(int $id_COMD ){
		if( !isset( $_SESSION['Auth']['id'] ) ) {
			header( 'Location: /connexion' );
		}

		$id_membre = $_SESSION['Auth']['id'];
		$membre = DB::select ('SELECT * FROM member WHERE id = ?', [$id_membre]);

		$ids_products = DB::select( 'SELECT ref_Products FROM order_member WHERE id = ?', [$id_COMD] );
        $produits = DB::select( 'SELECT * FROM products WHERE id IN ('.$ids_products[0]['ref_Products'].')' );

		$livraisons = DB::select ('SELECT * FROM delivery');

		$modifCommande = DB::select( 'SELECT * FROM order_member, member, delivery WHERE
		order_member.id = (SELECT MAX(id) FROM order_member WHERE ref_Member = ?) AND ref_Member = member.id AND ref_Delivery = delivery.id', [$id_membre] );

		foreach ( $modifCommande as $key => $commande ){
			$dateCommande = date_create( $commande['date_commande'] );
			$modifCommande[$key]['date_commande'] = date_format( $dateCommande, 'dmy' );
			$qty = explode(',', $commande['ref_Amount']);
		}

		if  ( !$modifCommande ){
			header( 'Location: /produits' );
		}

		if ( !empty( $_POST ) ) {
            extract( $_POST );
			$erreur = [];

			$livraison = DB::select ('SELECT * FROM delivery WHERE type = ?', [$type_livraison]);
			$id_livraison = $livraison[0]['id'];
			$tarif_1 = $livraison[0]['tarif_1'];
			$tarif_2 = $livraison[0]['tarif_2'];
			$tarif_3 = $livraison[0]['tarif_3'];

			$bool = true;
			$nb_obiwash = 0;
			$i = 0;
			foreach( $produits as $key => $produit ) :
				$id = $produit['id'];
				$title = $produit['title'];
				if ($title == 'Obiwash') {
					$nb_obiwash = $quantity[$id];
					if ( $nb_obiwash != $qty[$i] ) {
						$bool = false;
					}
				}
				$i++;
			endforeach;

			$prix_total = 0;
			$poids_total = 0;
			foreach ($produits as $key => $produit) {
				$id = $produit['id'];
				if ($quantity[$id] != NULL) {
					$poids = $produit['weight'] * $quantity[$id];
					$price = $produit['price'] * $quantity[$id];
					$prix_total += $price;
					$poids_total += $poids;
				} else {
					$erreur['total'] = 'L\'id du produit ne peut pas être modifié ou n\'existe pas !';
				}
			}

			if ( $bool ) {
				if ( isset ( $address_1 )  && empty ( $address_1 ) ) {
					$erreur['address_1'] = 'L\'adresse est obligatoire pour garentir l\'obiwash !';
				}
				if ( isset ( $address_2 )  && empty ( $address_2 ) ) {
					$erreur['address_2'] = 'L\'adresse est obligatoire pour garentir l\'obiwash !';
				}
				if ( isset ( $address_3 )  && empty ( $address_3 ) ) {
					$erreur['address_3'] = 'L\'adresse est obligatoire pour garentir l\'obiwash !';
				}
				if ( isset ( $address_4 )  && empty ( $address_4 ) ) {
					$erreur['address_4'] = 'L\'adresse est obligatoire pour garentir l\'obiwash !';
				}
			}

			if ( $type_livraison == 'Relais colis' ) {
				if ( empty ( $address_relais_colis ) ) {
					$erreur['address_relais_colis'] = 'L\'adresse est obligatoire pour le relais colis !';
				}
			}

			if( !$erreur) {
				if ( $poids_total >= 5 ) {
					$prix_total += $tarif_3;
				} else {
					if ( $poids_total >= 2 ) {
						$prix_total += $tarif_2;
					} else {
						if ( $poids_total > 0 ) {
							$prix_total += $tarif_1;
						}
					}
				}

				if ($mode_paiement == 'paypal') {
					$prix_total += 5;
				}

                DB::update( 'update order_member set ref_Amount = :ref_Amount, ref_Delivery = :ref_Delivery, mode_paiement = :mode_paiement, price = :price where id = :id', [
                    'ref_Amount' => implode(',', $quantity),
                    'ref_Delivery' => $id_livraison,
                    'mode_paiement' => htmlspecialchars( $mode_paiement ),
					'price' => htmlspecialchars( number_format($prix_total, 2, '.', ' ') ),
                    'id' => $id_COMD,
                ] );

				DB::update( 'update order_member set address_1 = :address_1 where id = :id', [
					'address_1' => $address_1,
					'id' => $id_COMD,
				] );

				DB::update( 'update order_member set address_2 = :address_2 where id = :id', [
					'address_2' => $address_2,
					'id' => $id_COMD,
				] );

				DB::update( 'update order_member set address_3 = :address_3 where id = :id', [
					'address_3' => $address_3,
					'id' => $id_COMD,
				] );

				DB::update( 'update order_member set address_4 = :address_4 where id = :id', [
					'address_4' => $address_4,
					'id' => $id_COMD,
				] );

				DB::update( 'update order_member set address_relais_colis = :address_relais_colis where id = :id', [
					'address_relais_colis' => $address_relais_colis,
					'id' => $id_COMD,
				] );

                header( 'Location: /commandes/modifier/'. $id_COMD . '?#title' );
			}
			$this->view( 'commandes/modifier', ['erreur' => $erreur, 'commandes' => $modifCommande, 'livraisons' => $livraisons, 'membre' => $membre[0], 'produits' => $produits] );
		}

		$this->view( 'commandes/modifier', ['commandes' => $modifCommande, 'livraisons' => $livraisons, 'membre' => $membre[0], 'produits' => $produits] );
	}
//SUPPRIMER UNE COMMANDE
    public function supprimer( int $idCommande ) {
        if( !isset( $_SESSION['Auth']['id'] ) ) {
			header( 'Location: /connexion' );
		}

        DB::delete ( 'DELETE FROM order_member WHERE id = ?', [$idCommande] );

        header( 'Location: /membre/compte' );
    }
//IMPRIMER UNE COMMANDE (TÉLÉCHARGEMENT PDF)
    public function telecharger( int $idCommande ) {
		$commandes = DB::select( 'SELECT * FROM order_member WHERE id = ?', [$idCommande] );

		foreach ( $commandes as $key => $commande ){
			$ref_member = $commande['ref_Member'];
			$ids_products = explode(',', $commande['ref_Products']);
			$qty_products = explode(',', $commande['ref_Amount']);
			$ref_delivery = $commande['ref_Delivery'];
			$ref_invoices_address = $commande['ref_Invoices_address'];
			$mode_paiement = $commande['mode_paiement'];
			$created_at = date_create( $commande['date_commande'] );
			$created_at = date_format( $created_at, 'dmy' );


			// $complete = "-";
			// for($i = strlen($idCommande); $i < 6; $i++)
			// 	$complete .= '0';

			// $ref_commande = 'RÉF. Obi-' . $ref_member . '-' . $created_at . $complete .$idCommande;

			if( $idCommande <= 9 ){
				$ref_commande = 'RÉF. Obi-' . $ref_member . '-' . $created_at . '-00000' .$idCommande;
			}
			elseif( $idCommande <= 99 ){
				$ref_commande = 'RÉF. Obi-' . $ref_member . '-' . $created_at . '-0000' . $idCommande;
			}
			elseif( $idCommande <= 999 ){
				$ref_commande = 'RÉF. Obi-' . $ref_member . '-' . $created_at . '-000' . $idCommande;
			}
			elseif( $idCommande <= 9999 ){
				$ref_commande = 'RÉF. Obi-' . $ref_member . '-' . $created_at . '-00' . $idCommande;
			}
			elseif( $idCommande <= 99999 ){
				$ref_commande = 'RÉF. Obi-' . $ref_member . '-' . $created_at . '-0' . $idCommande;
			}
			elseif( $idCommande <= 999999 ){
				$ref_commande = 'RÉF. Obi-' . $ref_member . '-' . $created_at . '-' . $idCommande;
			}
		}

		$membres = DB::select( 'SELECT * FROM member WHERE id = ?', [$ref_member] );
		foreach ( $membres as $key => $membre ){
			$name = $membre['name'];
			$first_name = $membre['first_name'];
			$company_service = $membre['company_service'];
			$adresse = $membre['address'];
			$adresse_supplement = $membre['address_supplement'];
			$code_postal = $membre['code_postal'];
			$city = $membre['city'];
			$country = $membre['country'];
			$mail = $membre['mail_member'];
			$tel = $membre['tel'];
		}

		$produits = DB::select( 'SELECT * FROM products WHERE id IN ('.$commandes[0]['ref_Products'].')' );

		$poids = 0;
		$i = 0;
		foreach( $ids_products as $key => $id_produit ) :
			foreach( $produits as $key => $produit ) :
				$id = $produit['id'];
				$poids = $produit['weight'];
				if ($id_produit == $id) {
					$total_poids += $poids * $qty_products[$i];
				}
			endforeach;
			$i += 1;
		endforeach;

		// Activation de la classe
		$pdf = new PDF('P','mm','A4');
		$pdf->AddPage();
		$pdf->SetFont('Arial','',11);
		$pdf->SetTextColor(0);

        $pdf->SetLineWidth(0.1);
		$pdf->SetFillColor(192);
		$pdf->Rect(120, 15, 85, 8, "DF");
        $pdf->SetXY( 120, 15 );
		$pdf->SetFont( "Arial", "", 12 );
		$pdf->Cell( 85, 8, utf8_decode('Commande N° ').utf8_decode($ref_commande), 0, 0, 'C');

        // date facture
        $champ_date = date_create($commandes[0]['date_commande']);
		$date_fact = date_format($champ_date, 'd/m/Y');
        $pdf->SetFont('Arial','',11);
		$pdf->SetXY( 122, 30 );
        $pdf->Cell( 60, 8, $membres[0]['city'] .", le " . $date_fact, 0, 0, '');

		// Infos du client calées à gauche
		$pdf->SetFont( "Arial", "BU", 10 );
		$pdf->SetXY( 5, 50 ) ;
		$pdf->Cell($pdf->GetStringWidth("Adresse de livraison :"), 0, "Adresse de livraison :", 0, "L");
		$pdf->SetFont('Arial', "", 10); $x = 5 ; $y = 50;
		$pdf->SetXY( $x, $y ); $pdf->Cell( 100, 9, utf8_decode($first_name).' '.utf8_decode($name), 0, 0, ''); $y += 4;
		if ($company_service) {
			$pdf->SetXY( $x, $y ); $pdf->Cell( 100, 9, utf8_decode('Société : ').utf8_decode($company_service), 0, 0, ''); $y += 4;
		}
		if ($adresse_supplement) {
			$pdf->SetXY( $x, $y ); $pdf->Cell( 100, 9, utf8_decode($adresse).' - '.utf8_decode($adresse_supplement), 0, 0, ''); $y += 4;
		} else {
			$pdf->SetXY( $x, $y ); $pdf->Cell( 100, 9, utf8_decode($adresse), 0, 0, ''); $y += 4;
		}
		$pdf->SetXY( $x, $y ); $pdf->Cell( 100, 9, $code_postal.' '.utf8_decode($city).', '.utf8_decode($country), 0, 0, ''); $y += 8;
		$pdf->SetXY( $x, $y ); $pdf->Cell( 100, 9, utf8_decode('Tél : ').$tel, 0, 0, ''); $y += 4;
		$pdf->SetXY( $x, $y ); $pdf->Cell( 100, 9, 'Email : '.utf8_decode($mail), 0, 0, ''); $y += 4;

		$invoices_address = DB::select( 'SELECT * FROM invoices_address WHERE id = ?', [$ref_invoices_address] );
		foreach ( $invoices_address as $invoice_address ){
			$name = $invoice_address['name'];
			$first_name = $invoice_address['first_name'];
			$company_service = $invoice_address['company_service'];
			$adresse = $invoice_address['address'];
			$adresse_supplement = $invoice_address['address_supplement'];
			$code_postal = $invoice_address['code_postal'];
			$city = $invoice_address['city'];
		}

		// Infos du client calées à droite
		$pdf->SetFont( "Arial", "BU", 10 );
		$pdf->SetXY( 110, 50 ) ;
		$pdf->Cell($pdf->GetStringWidth("Adresse de facturation :"), 0, "Adresse de facturation :", 0, "L");
		$pdf->SetFont('Arial', "", 10); $x = 110 ; $y = 50;
		$pdf->SetXY( $x, $y ); $pdf->Cell( 100, 9, utf8_decode($first_name).' '.utf8_decode($name), 0, 0, ''); $y += 4;
		if ($company_service) {
			$pdf->SetXY( $x, $y ); $pdf->Cell( 100, 9, utf8_decode('Société : ').utf8_decode($company_service), 0, 0, ''); $y += 4;
		}
		if ($adresse_supplement) {
			$pdf->SetXY( $x, $y ); $pdf->Cell( 100, 9, utf8_decode($adresse).' - '.utf8_decode($adresse_supplement), 0, 0, ''); $y += 4;
		} else {
			$pdf->SetXY( $x, $y ); $pdf->Cell( 100, 9, utf8_decode($adresse), 0, 0, ''); $y += 4;
		}
		$pdf->SetXY( $x, $y ); $pdf->Cell( 100, 9, $code_postal.' '.utf8_decode($city).', '.utf8_decode($country), 0, 0, ''); $y += 4;

		// ***********************
        // le cadre des articles
        // ***********************
        // cadre avec 18 lignes max ! et 118 de hauteur --> 95 + 118 = 213 pour les traits verticaux
        $pdf->SetLineWidth(0.1);
		$pdf->Rect(5, 95, 200, 118, "D");
        // cadre titre des colonnes
        $pdf->Line(5, 105, 205, 105);
        // les traits verticaux colonnes
		$pdf->Line(158, 95, 158, 213);
		$pdf->Line(176, 95, 176, 213);
		$pdf->Line(187, 95, 187, 213);
        // titre colonne
        $pdf->SetXY( 1, 96 ); $pdf->SetFont('Arial','B',8); $pdf->Cell( 153, 8, "Description", 0, 0, 'C');
        $pdf->SetXY( 156, 96 ); $pdf->SetFont('Arial','B',8); $pdf->Cell( 22, 8, "PU HT", 0, 0, 'C');
        $pdf->SetXY( 177, 96 ); $pdf->SetFont('Arial','B',8); $pdf->Cell( 10, 8, utf8_decode("Qté"), 0, 0, 'C');
        $pdf->SetXY( 185, 96 ); $pdf->SetFont('Arial','B',8); $pdf->Cell( 22, 8, "TOTAL HT", 0, 0, 'C');

        // les articles
        $pdf->SetFont('Arial','',8);
        $y = 97;

		$total = 0;
		$i = 0;
		foreach( $produits as $produit ) :
			$id = $produit['id'];
			$image = $produit['image'];
			$title = $produit['title'];
			$description = $produit['description'];
			$price =  $produit['price'];

			if (strlen($description) > 85) {
				$description= substr($description,0,85). ' [...]';
			}
			// libelle
			$pdf->SetFont( "Arial", "B", 10 );
			$pdf->SetXY( 25, $y+9 );
			$pdf->Cell( 140, 5, utf8_decode($title), 0, 0, 'L');
			$pdf->SetFont( "Arial", "", 8 );
            $pdf->SetXY( 25, $y+13 );
			$pdf->Cell( 140, 5, utf8_decode($description), 0, 0, 'L');
			$pdf->Image(ROOT . 'public/files/' . $image, 7, $y+9, 13);
            // PU
            $pdf->SetXY( 158, $y+11 ); $pdf->Cell( 18, 5, number_format($price, 2, ',', ' ') . ' ' . chr(128), 0, 0, 'R');
            // Qté
            $pdf->SetXY( 177, $y+11 ); $pdf->Cell( 10, 5, $qty_products[$i], 0, 0, 'R');
            // total
            $pdf->SetXY( 187, $y+11 ); $pdf->Cell( 18, 5, number_format($price * $qty_products[$i], 2, ',', ' ') . ' ' . chr(128), 0, 0, 'R');

            $pdf->Line(5, $y+20, 205, $y+20);
            $y += 12;

			$total += $price * $qty_products[$i];
			$i++;
		endforeach;

		$livraisons = DB::select( 'SELECT * FROM delivery WHERE id = ?', [$ref_delivery] );
		foreach ( $livraisons as $livraison ){
			$tarif_1 = $livraison['tarif_1'];
	        $tarif_2 = $livraison['tarif_2'];
	        $tarif_3 = $livraison['tarif_3'];
		}

		$frais_port = 0;
		if ( $total_poids >= 5 ) {
			$frais_port = $tarif_3;
		} else {
			if ( $total_poids >= 2 ) {
				$frais_port = $tarif_2;
			} else {
				if ( $total_poids > 0 ) {
					$frais_port = $tarif_1;
				}
			}
		}


		if ($mode_paiement == 'cb') {
			$mode_paiement = 'Carte Bancaire';
		} else {
			if ($mode_paiement == 'paypal') {
				$mode_paiement = 'Paypal';
				$total += 5;
			} else {
				if ($mode_paiement == 'cheque') {
					$mode_paiement = 'Chèque';
				} else {
					$mode_paiement = 'Virement Bancaire';
				}
			}
		}

		$pdf->SetFont('Arial','',10);
		$pdf->Rect(95, 213, 110, 24, "D");
		// trait vertical cadre totaux, 8 de hauteur -> 213 + 24 = 237
		$pdf->Line(158, 213, 158, 237);

		// Le sous-total
		$pdf->SetXY( 95, 213 );
		$pdf->Cell( 63, 8, "Sous-total", 0, 0, 'C');

        $nombre_format_francais = number_format($total, 2, ',', ' ') . ' ' . chr(128);
		$pdf->SetXY( 158, 213 );
		$pdf->Cell( 47, 8, $nombre_format_francais, 0, 0, 'C');
		$pdf->Line(95, 221, 205, 221);

		// La livraison
		$pdf->SetXY( 95, 221 );
		$pdf->Cell( 63, 8, "Livraison", 0, 0, 'C');

        $nombre_format_francais = number_format($frais_port, 2, ',', ' ') . ' ' . chr(128);
		$pdf->SetXY( 158, 221 );
		$pdf->Cell( 47, 8, $nombre_format_francais, 0, 0, 'C');
		$pdf->Line(95, 229, 205, 229);

		// le Prix total
		$pdf->SetLineWidth(0.1);
		$pdf->SetFillColor(192);
		$pdf->Rect(95, 229, 63, 8, "DF");
		$pdf->SetFont('Arial','B',12);
		$pdf->SetXY( 95, 229 );
		$pdf->Cell( 63, 8, utf8_decode("Net à payer TTC"), 0, 0, 'C');

		$nombre_format_francais = number_format($total + $frais_port, 2, ',', ' ') . ' ' . chr(128);
		$pdf->SetXY( 158, 229 );
		$pdf->Cell( 47, 8, $nombre_format_francais, 0, 0, 'C');

        // reglement
		$pdf->SetFont('Arial','B',8);
        $pdf->SetXY( 95, 237 );
		$pdf->Cell( 63, 8, utf8_decode("Mode de Règlement :"), 0, 0, 'C');
		$pdf->Cell( 47, 8, utf8_decode($mode_paiement), 0, 0, 'C');

		// Nom du fichier
		$nom = 'Obiwash-Bon-de-commande-'.$idCommande.'.pdf';

		// Création du PDF
		$pdf->Output($nom,'I');
    }
}
