<?php
class Livraison extends Controller {

//AFFICHAGE
	public function index() {
        $ids = array_keys($_SESSION['Panier']);
        if ( empty($ids) ) {
            $produits = array();
        } else {
            $produits = DB::select( 'SELECT * FROM products WHERE id IN (' .implode(',', $ids). ')' );
        }

		$livraisons = DB::select ('SELECT * FROM delivery');

		$id_membre = $_SESSION['Auth']['id'];
 		$membre = DB::select ('SELECT * FROM member WHERE id = ?', [$id_membre]);

	  	$this->view( 'livraison/index', ['livraisons' => $livraisons, 'membre' => $membre[0]] );
	}
//VERIFICATION SI COMPTE EXISTE
	private function accountExists() : array {
		$compte = DB::select('SELECT id, login, password FROM admin UNION SELECT id, login, password FROM member');

		if ( $compte ){
            for ($i = 0; $i < sizeof($compte); $i+=1) {
                if ($compte[$i]['login'] == $_POST['login'] && $compte[$i]['password'] == md5($_POST['password'])) {
                    return $compte[$i];
                }
            }
		    return [];
		}
		else {
			return [];
		}
	}
//INFOS MEMBRE
	public function informations() {
		if( !isset( $_SESSION['Auth']['id'] ) ) {

			if ( !empty( $_POST ) ) {
		        extract( $_POST );

		        $compte = $this->accountExists();

				if ( $compte ) {
	                $_SESSION['Auth'] = array(
	                    'id' => $compte['id'],
	                    'login' => $compte['login'],
	                    'password' => $compte['password']
	                );

	                if ($compte['login'] == 'admin_obiwash') {
	                    header( 'Location: /admin' );
	                }
	                else {
					    header( 'Location: /livraison/informations' );
	                }
				}
				else {
					$erreur = 'Attention : Identifiants erronés ou manquants !';

				}

				$this->view( 'livraison/informations', ['erreur' => $erreur] );
		    }

			$this->view( 'livraison/informations' );

		} else {

			$livraisons = DB::select ('SELECT * FROM delivery');

			$ids_products = array_keys($_SESSION['Panier']);
			asort($ids_products);
	        if ( empty($ids_products) ) {
	            $produits = array();
	        } else {
	            $produits = DB::select( 'SELECT * FROM products WHERE id IN (' .implode(',', $ids_products). ')' );
	        }

			$id_membre = $_SESSION['Auth']['id'];
	 		$membre = DB::select ('SELECT * FROM member WHERE id = ?', [$id_membre]);

			if( !empty( $_POST ) ) {
				extract( $_POST );
				$erreur = [];

				if ( isset ( $address_1 )  && empty ( $address_1 ) ) {
					$erreur['address_1'] = 'L\'adresse est obligatoire pour garentir l\'obiwash !';
				}
				if ( isset ( $address_2 )  && empty ( $address_2 ) ) {
					$erreur['address_2'] = 'L\'adresse est obligatoire pour garentir l\'obiwash !';
				}
				if ( isset ( $address_3 )  && empty ( $address_3 ) ) {
					$erreur['address_3'] = 'L\'adresse est obligatoire pour garentir l\'obiwash !';
				}
				if ( isset ( $address_4 )  && empty ( $address_4 ) ) {
					$erreur['address_4'] = 'L\'adresse est obligatoire pour garentir l\'obiwash !';
				}

				if ( $type_livraison == 'Relais colis' ) {
					if ( empty ( $address_relais_colis ) ) {
						$erreur['address_relais_colis'] = 'L\'adresse est obligatoire pour le relais colis !';
					}
				}

				if( !$erreur) {

					if ( !empty ( $name ) || !empty( $first_name ) || !empty ( $address ) || !empty ( $code_postal ) || !empty ( $city ) ) {
						if ( !empty ( $name ) && !empty( $first_name ) && !empty ( $address ) && !empty ( $code_postal ) && !empty ( $city ) ) {
							DB::insert( 'insert into invoices_address (ref_Member, name, first_name, company_service, address, address_supplement, code_postal, city) values (:ref_Member, :name, :first_name, :company_service, :address, :address_supplement, :code_postal, :city)', [
								'ref_Member' => $id_membre,
								'name' => htmlspecialchars( $name ),
								'first_name' => htmlspecialchars( $first_name ),
								'company_service' => htmlspecialchars( $company_service ),
								'address' => htmlspecialchars( $address ),
								'address_supplement' => htmlspecialchars( $address_supplement ),
								'code_postal' => htmlspecialchars( $code_postal ),
								'city' => htmlspecialchars( $city )
							] );
						} else {
						   $erreur['champ'] = "Tous les champ doit être remplis !";
					   }
				   } else {
					   DB::insert( 'insert into invoices_address (ref_Member, name, first_name, company_service, address, address_supplement, code_postal, city) values (:ref_Member, :name, :first_name, :company_service, :address, :address_supplement, :code_postal, :city)', [
						   'ref_Member' => $id_membre,
						   'name' => htmlspecialchars( $membre[0]['name'] ),
						   'first_name' => htmlspecialchars( $membre[0]['first_name'] ),
						   'company_service' => htmlspecialchars( $membre[0]['company_service'] ),
						   'address' => htmlspecialchars( $membre[0]['address'] ),
						   'address_supplement' => htmlspecialchars( $membre[0]['address_supplement'] ),
						   'code_postal' => htmlspecialchars( $membre[0]['code_postal'] ),
						   'city' => htmlspecialchars( $membre[0]['city'] )
					   ] );
				   }

					if( !$erreur) {
						ksort($_SESSION['Panier']);
						$qty_products = array();
						foreach ($_SESSION['Panier'] as $value) {
							$qty_products[] = $value;
						}

						$prix_total = 0;
						$poids_total = 0;
						$i = 0;
						foreach ($produits as $produit) {
							$poids = $produit['weight'] * $qty_products[$i];
							$price = $produit['price'] * $qty_products[$i];
							$prix_total += $price;
							$poids_total += $poids;
							$i += 1;
						}

						$livraison = DB::select ('SELECT * FROM delivery WHERE type = ?', [$type_livraison]);
						$id_livraison = $livraison[0]['id'];
						$tarif_1 = $livraison[0]['tarif_1'];
				        $tarif_2 = $livraison[0]['tarif_2'];
				        $tarif_3 = $livraison[0]['tarif_3'];

			            if ( $poids_total >= 5 ) {
			                $prix_total += $tarif_3;
			            } else {
			                if ( $poids_total >= 2 ) {
			                    $prix_total += $tarif_2;
			                } else {
								if ( $poids_total > 0 ) {
				                    $prix_total += $tarif_1;
				                }
			                }
			            }

						if ($mode_paiement == 'paypal') {
							$prix_total += 5;
						}

						$id_facturation = DB::select( 'SELECT MAX(id) FROM invoices_address WHERE ref_Member = ?', [$id_membre] );

						DB::insert( 'insert into order_member (ref_Member, ref_Products, ref_Amount, ref_Delivery, ref_Invoices_address, mode_paiement, price) values (:ref_Member, :ref_Products, :ref_Amount, :ref_Delivery, :ref_Invoices_address, :mode_paiement, :price)', [
							'ref_Member' => $id_membre,
							'ref_Products' => implode(',', $ids_products),
							'ref_Amount' => implode(',', $qty_products),
							'ref_Delivery' => $id_livraison,
							'ref_Invoices_address' => $id_facturation[0][0],
							'mode_paiement' => htmlspecialchars( $mode_paiement ),
							'price' => htmlspecialchars( number_format($prix_total, 2, '.', ' ') )
							] );

						$commande = DB::select( 'SELECT MAX(id) FROM order_member WHERE ref_Member = ?', [$id_membre] );

						DB::update( 'update order_member set address_1 = :address_1 where id = :id', [
		                    'address_1' => $address_1,
		                    'id' => $commande[0]['MAX(id)'],
		                ] );


						DB::update( 'update order_member set address_2 = :address_2 where id = :id', [
		                    'address_2' => $address_2,
		                    'id' => $commande[0]['MAX(id)'],
		                ] );


						DB::update( 'update order_member set address_3 = :address_3 where id = :id', [
		                    'address_3' => $address_3,
		                    'id' => $commande[0]['MAX(id)'],
		                ] );


						DB::update( 'update order_member set address_4 = :address_4 where id = :id', [
		                    'address_4' => $address_4,
		                    'id' => $commande[0]['MAX(id)'],
		                ] );

						DB::update( 'update order_member set address_relais_colis = :address_relais_colis where id = :id', [
							'address_relais_colis' => $address_relais_colis,
							'id' => $commande[0]['MAX(id)'],
						] );

						header( 'Location: /commandes' );
					} else {
						$this->view( 'livraison/informations', ['erreur' => $erreur, 'paniers' => $produits, 'livraisons' => $livraisons, 'membre' => $membre[0]] );
					}
				} else {
					$this->view( 'livraison/informations', ['erreur' => $erreur, 'paniers' => $produits, 'livraisons' => $livraisons, 'membre' => $membre[0]] );
				}
			}

		  	$this->view( 'livraison/informations', ['paniers' => $produits, 'livraisons' => $livraisons, 'membre' => $membre[0]] );
		}
	}
}
