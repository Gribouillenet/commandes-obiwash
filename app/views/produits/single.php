<?php require($_SERVER['DOCUMENT_ROOT'].'/app/views/include/header.php'); ?>

<section id="container-recap-panier" class="container-recap-panier">
<?php require($_SERVER['DOCUMENT_ROOT'].'/app/views/include/inc/widgets-recap-panier.php'); ?>
</section>
<section id="page-inner" class="page-inner">
	<header class="page-header d-flex justify-content-center align-items-center">
   		<h1 class="container p-lg-0">La boutique</h1>
	</header>
	<aside>
		<?php
			foreach( $data['produits'] as $key => $produit ) :
			include('data-products.php');
			require($_SERVER['DOCUMENT_ROOT'].'/app/views/include/breadcrumb.php');
		?>
	</aside>
	<div class="container-article container p-lg-0">
	        <article class="row">
				<div class="col-12 col-md-5">
					<?php include($root . '/inc/image-product.php');?>
				</div>
				<div class="col-12 col-md-7">
					<header class="header-product d-flex justify-content-between">
					<h2 class="title-product"><?= $title ?></h2><p class="price-product"><?= number_format($price, 2, ',', ' ') ?> €</p>
				</header>
            	<p><?= $description ?></p>

				<style>
					.data-sheet-details {
						background: white;
					}
					
					.data-sheet-details :nth-child(even) {
						background: #f0f0f0;
					}

					.data-sheet-details li {
						display: flex;
						min-height: 50px;
						justify-content: space-between;
						align-items: center;
						padding: 0 25px;
					}

					.data-sheet-details-attributes {
						font-weight: bold;
						max-width: 35%;
					}
				</style>

				<footer class="footer-product d-flex justify-content-beetween">
					<?php if ( $stock == 0 ) { ?>
						<p class="alert alert-warning col-auto">Stock épuisé.</p>
					<?php } else { ?>
						<a id="data-id-<?= $id ?>" class="addPanier button col-auto" href="/produits/ajout/<?= $id ?>" data-id="<?= $id ?>"><i class="ti-shopping-cart"></i>Ajouter au panier</a>
					<?php } ?>

				</div>
	        </article>
				</footer>

				<hr/>
				<h2 class="mt-4">Descriptif du produit</h2>
				<ul class="data-sheet-details col-12">
					<?php	
						$donnees = DB::select( "select data_sheet from products where id = $id order by id asc" );
						$produits = explode(';', $donnees[0]['data_sheet']);

						foreach($produits as $produit) {
							if($produit != "") {
								$elements = explode(':', $produit);
								echo '<li><span class="data-sheet-details-attributes">' . $elements[0] . '</span>'. $elements[1] . '</li>';
							}
						}
					
					?>
				</ul>

		<?php endforeach;?>
		</div>
	</div><!-- .container -->

</section>
<?php require($_SERVER['DOCUMENT_ROOT'].'/app/views/include/footer.php'); ?>