<?php require($_SERVER['DOCUMENT_ROOT'].'/app/views/include/header_admin_membres.php'); ?>
<section class="page-inner clearfix">
    <header class="page-header d-flex flex-column justify-content-center align-items-center">
        <h1>Gestion des clients</h1>
    </header>
	<div class="container gestion-membres">
    <div class="row">
	    <div class="col-12">
	   <h2 class="p-0">Ajouter ou modifier un client</h2>
        <?php if ( isset( $data['erreur'] ) ) { ?>
   	       <div class="alert alert-danger alert-on"><?= $data['erreur']['champ_obligatoire'] ?></div>
   	    <?php } else { ?>
       	    <span class="champ-obligatoire d-inline-block">* Champs Obligatoires</span>
     	<?php } ?>
	    </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <form class="form-label form-enter" action="/admin/membres" method="post" enctype="multipart/form-data" novalidate>

                <input type="text" name="name" class="form-control form-obligatoire" placeholder="Nom*" value="<?php if ( isset( $_POST['name'] ) ) echo $_POST['name'] ?>">
        		<?php if ( isset( $data['erreur']['name'] ) ) : ?>
                    <div class="alert alert-danger alert-form"><?= $data['erreur']['name'] ?></div>
                <?php endif; ?>

                <input type="text" name="first_name" class="form-control form-obligatoire" placeholder="Prénom*" value="<?php if ( isset( $_POST['first_name'] ) ) echo $_POST['first_name'] ?>">
        		<?php if ( isset( $data['erreur']['first_name'] ) ) : ?>
                    <div class="alert alert-danger alert-form"><?= $data['erreur']['first_name'] ?></div>
                <?php endif; ?>

                <input type="text" name="company_service" class="form-control form-obligatoire" placeholder="Société/Service" value="<?php if ( isset( $_POST['company_service'] ) ) echo $_POST['company_service'] ?>">

                <input type="text" name="address" class="form-control form-obligatoire" placeholder="Adresse*" value="<?php if ( isset( $_POST['address'] ) ) echo $_POST['address'] ?>">
        		<?php if ( isset( $data['erreur']['address'] ) ) : ?>
                    <div class="alert alert-danger alert-form"><?= $data['erreur']['address'] ?></div>
                <?php endif; ?>

                <input type="text" name="address_supplement" class="form-control form-obligatoire" placeholder="Complément d'adresse" value="<?php if ( isset( $_POST['address_supplement'] ) ) echo $_POST['address_supplement'] ?>">

                <input type="text" name="code_postal" class="form-control form-obligatoire" placeholder="Code postal*" maxlength="5" value="<?php if ( isset( $_POST['code_postal'] ) ) echo $_POST['code_postal'] ?>">
        		<?php if ( isset( $data['erreur']['code_postal'] ) ) : ?>
                    <div class="alert alert-danger alert-form"><?= $data['erreur']['code_postal'] ?></div>
                <?php endif; ?>

        		<input type="text" name="city" class="form-control form-obligatoire" placeholder="Ville*" value="<?php if ( isset( $_POST['city'] ) ) echo $_POST['city'] ?>">
        		<?php if ( isset( $data['erreur']['city'] ) ) : ?>
                    <div class="alert alert-danger alert-form"><?= $data['erreur']['city'] ?></div>
                <?php endif; ?>

        		<div class="form-group">
        			<label for="sel1">Selectionner dans la liste:</label>
        			<select id="sel1" class="form-control form-obligatoire" name="country">
        				<option value="" disabled selected>Choississez votre pays</option>
        				<optgroup label="France Métropolitaine">
        					<option value="France" <?php if ( $_POST['country'] == 'France' ) echo "selected" ?>>
        						France
        					</option>
        				</optgroup>
        				<optgroup label="Outre-mer Zone 1">
        					<option value="Réunion" <?php if ( $_POST['country'] == 'Réunion' ) echo "selected" ?>>
        						Réunion
        					</option>
        					<option value="Guadeloupe" <?php if ( $_POST['country'] == 'Guadeloupe' ) echo "selected" ?>>
        						Guadeloupe
        					</option>
        					<option value="Martinique" <?php if ( $_POST['country'] == 'Martinique' ) echo "selected" ?>>
        						Martinique
        					</option>
        					<option value="Mayotte" <?php if ( $_POST['country'] == 'Mayotte' ) echo "selected" ?>>
        						Mayotte
        					</option>
        					<option value="Guyane" <?php if ( $_POST['country'] == 'Guyane' ) echo "selected" ?>>
        						Guyane
        					</option>
        					<option value="Saint-Pierre et Miquelon" <?php if ( $_POST['country'] == 'Saint-Pierre et Miquelon' ) echo "selected" ?>>
        						Saint-Pierre et Miquelon
        					</option>
        					<option value="Saint-Martin" <?php if ( $_POST['country'] == 'Saint-Martin' ) echo "selected" ?>>
        						Saint-Martin
        					</option>
        					<option value="Saint-Barthélemy" <?php if ( $_POST['country'] == 'Saint-Barthélemy' ) echo "selected" ?>>
        						Saint-Barthélemy
        					</option>
        				</optgroup>
        				<optgroup label="Outre-mer Zone 2">
        					<option value="Polynésie Française" <?php if ( $_POST['country'] == 'Polynésie Française' ) echo "selected" ?>>
        						Polynésie Française
        					</option>
        					<option value="Wallis et Futuna" <?php if ( $_POST['country'] == 'Wallis et Futuna' ) echo "selected" ?>>
        						Wallis et Futuna
        					</option>
        					<option value="Nouvelle Calédonie" <?php if ( $_POST['country'] == 'Nouvelle Calédonie' ) echo "selected" ?>>
        						Nouvelle Calédonie
        					</option>
        					<option value="TAAF" <?php if ( $_POST['country'] == 'TAAF' ) echo "selected" ?>>
        						TAAF
        					</option>
        				</optgroup>
        			</select>
        		</div>
        		<?php if ( isset( $data['erreur']['country'] ) ) : ?>
                    <div class="alert alert-danger alert-form"><?= $data['erreur']['country'] ?></div>
                <?php endif; ?>

        		<input type="text" name="mail_member" class="form-control form-obligatoire" placeholder="Email*" value="<?php if ( isset( $_POST['mail_member'] ) ) echo $_POST['mail_member'] ?>">
        		<?php if ( isset( $data['erreur']['mail_member'] ) ) : ?>
                    <div class="alert alert-danger alert-form"><?= $data['erreur']['mail_member'] ?></div>
                <?php endif; ?>

        		<input type="text" name="tel" class="form-control form-obligatoire" placeholder="Téléphone*" maxlength="14" value="<?php if ( isset( $_POST['tel'] ) ) echo $_POST['tel'] ?>">
                <?php if ( isset( $data['erreur']['tel'] ) ) : ?>
                    <div class="alert alert-danger alert-form"><?= $data['erreur']['tel'] ?></div>
                <?php endif; ?>

                <input type="text" name="login" class="form-control form-obligatoire" placeholder="Nom d'utilisateur*" value="<?php if ( isset( $_POST['login'] ) ) echo $_POST['login'] ?>">
        		<?php if ( isset( $data['erreur']['login'] ) ) : ?>
                    <div class="alert alert-danger alert-form"><?= $data['erreur']['login'] ?></div>
                <?php endif; ?>

                <div class="btn btn-valid" ><input type="submit" value="Enregistrer"></div>
            </form>
        </div>
        <div class="col-md-8 bootstrap-table">
            <table class="table table-striped">
                <tbody>
                  <?php
                  foreach($data['member'] as $member) :
                  ?>
                  <tr>
                    <th>Obi-<?= $member['id'] ?></th>
                    <td><?= $member['name'] ?></td>
                    <td><a href="/admin/historique_client/<?= $member['id'] ?>" class="hist"><span>Historique</span></a></td>
                    <td><a href="/admin/editer_client/<?= $member['id'] ?>" class="edit"><span>Éditer</span></a></td>
                    <td><a href="/admin/password/<?= $member['id'] ?>" class="new-password"><span>Générer</span></a></td>
                    <td><a href="/admin/supprimer_client/<?= $member['id'] ?>" class="delete" onclick="return confirm('Voulez-vous vraiment supprimer ce client ?')"><span>Supprimer</span></a></td>
                  </tr>
                  <?php
                  endforeach;
                  ?>
                </tbody>
            </table>
        </div>
    </div>
				</div>
</section>
<?php require($_SERVER['DOCUMENT_ROOT'].'/app/views/include/footer.php'); ?>
