<?php require($_SERVER['DOCUMENT_ROOT'].'/app/views/include/header.php'); ?>



    <?php if (isset($_SESSION['Auth']['id'])){ ?>
    <section class="page-inner clearfix">
	    <header class="page-header d-flex justify-content-center align-items-center">
	    	<h1 class="text-xs-center">Informations personnelles</h1>
		</header>
		<div class="container">
			<h3>Vos informations de livraison</h3>
	        <form class="form-label form-fiche-client form-enter" action="" method="post" class="p-y-3 p-x-2" enctype="multipart/form-data" novalidate>
	    		<label for="name">Nom</label>
	    		<input type="text" name="name" class="form-control form-obligatoire" placeholder="Nom" value="<?php echo $data['membre']['name']; ?>" disabled>

	    		<label for="first_name">Prénom</label>
	    		<input type="text" name="first_name" class="form-control form-obligatoire" placeholder="Prénom" value="<?php echo $data['membre']['first_name']; ?>" disabled>

	    		<label for="company_service">Société/Service</label>
	    		<input type="text" name="company_service" class="form-control form-obligatoire" placeholder="Société/Service" value="<?php echo $data['membre']['company_service']; ?>" disabled>

	    		<label for="address">Adresse</label>
	    		<input type="text" name="address" class="form-control form-obligatoire" placeholder="Adresse" value="<?php echo $data['membre']['address']; ?>" disabled>

	    		<label for="address_supplement">Complément d'adresse</label>
	    		<input type="text" name="address_supplement" class="form-control form-obligatoire" placeholder="Complément d'adresse" value="<?php echo $data['membre']['address_supplement']; ?>" disabled>

	    		<label for="code_postal">Code postal</label>
	    		<input type="text" name="code_postal" class="form-control form-obligatoire" placeholder="Code postal" value="<?php echo $data['membre']['code_postal']; ?>" disabled>

	    		<label for="city">Ville</label>
	    		<input type="text" name="city" class="form-control form-obligatoire" placeholder="Ville" value="<?php echo $data['membre']['city']; ?>" disabled>

	    		<label for="country">Pays</label>
	    		<input type="text" name="country" class="form-control form-obligatoire" placeholder="Pays" value="<?php echo $data['membre']['country']; ?>" disabled>

	    		<label for="mail_membre">Email</label>
	    		<input type="text" name="mail_membre" class="form-control form-obligatoire" placeholder="Email" value="<?php echo $data['membre']['mail_member']; ?>" disabled>

	    		<label for="tel">Téléphone</label>
	    		<input type="text" name="tel" class="form-control form-obligatoire" placeholder="Téléphone" value="<?php echo $data['membre']['tel']; ?>" disabled>
	        </form>

	        <a class="link-come-back" href="/membre/editer"><button>Éditer son profil</button></a>

	        <form class="form-label form-fiche-client form-enter" action="" method="post" class="p-y-3 p-x-2" enctype="multipart/form-data" novalidate>

				<h3>Vos informations de facturation (si différentes)</h3>

				<?php if ( isset( $data['erreur']['champ'] ) ) : ?>
		            <div class="alert alert-danger alert-on"><?= $data['erreur']['champ'] ?></div>
		        <?php endif; ?>

				<label for="name">Nom</label>
				<input type="text" name="name" class="form-control form-obligatoire" placeholder="Nom" value="<?php if ( isset( $_POST['name'] ) ) echo $_POST['name'] ?>">

				<label for="first_name">Prénom</label>
				<input type="text" name="first_name" class="form-control form-obligatoire" placeholder="Prénom" value="<?php if ( isset( $_POST['first_name'] ) ) echo $_POST['first_name'] ?>">

				<label for="company_service">Société/Service</label>
				<input type="text" name="company_service" class="form-control form-obligatoire" placeholder="Société/Service" value="<?php if ( isset( $_POST['company_service'] ) ) echo $_POST['company_service'] ?>">

				<label for="address">Adresse</label>
				<input type="text" name="address" class="form-control form-obligatoire" placeholder="Adresse" value="<?php if ( isset( $_POST['address'] ) ) echo $_POST['address'] ?>">

				<label for="address_supplement">Complément d'adresse</label>
				<input type="text" name="address_supplement" class="form-control form-obligatoire" placeholder="Complément d'adresse" value="<?php if ( isset( $_POST['address_supplement'] ) ) echo $_POST['address_supplement'] ?>">

				<label for="code_postal">Code postal</label>
				<input type="text" name="code_postal" class="form-control form-obligatoire" placeholder="Code postal" value="<?php if ( isset( $_POST['code_postal'] ) ) echo $_POST['code_postal'] ?>">

				<label for="city">Ville</label>
				<input type="text" name="city" class="form-control form-obligatoire" placeholder="Ville" value="<?php if ( isset( $_POST['city'] ) ) echo $_POST['city'] ?>">

				<?php
				$nb_obiwash = 0;
				foreach( $data['paniers'] as $key => $produit ) :
					$id = $produit['id'];
					$title = $produit['title'];
					if ($title == 'Obiwash') {
						$nb_obiwash = $_SESSION['Panier'][$id];
					}
				endforeach;

				if ( $nb_obiwash != 0 ) { ?>
					<p>ATTENTION ! Vous avez mis <?= $nb_obiwash ?> Obiwash© dans votre panier. Afin de garantir légalement et nominativement chaque appareil, veuillez remplir les champs suivant. Merci de votre compréhension</p>
					<p>* Champs Obligatoires</p>
					<h3>Adresses de garentie</h3>

					<?php for ($i = 1; $i <= $nb_obiwash; $i+=1 ) { ?>
						<h4>Obiwash <?= $i ?></h4>
						<label for="address_<?= $i ?>">Nom Prénom - Adresse postale*</label>
						<input type="text" name="address_<?= $i ?>" class="form-control form-obligatoire" placeholder="Nom Prénom - Adresse postale" value="<?php if ( isset( $_POST['address_'.$i] ) ) echo $_POST['address_'.$i] ?>">
						<?php if ( isset( $data['erreur']['address_'.$i] ) ) : ?>
				            <div class="alert alert-danger alert-form"><?= $data['erreur']['address_'.$i] ?></div>
				        <?php endif; ?>
					<?php }
				}?>

				<?php
				$ids = array_keys($_SESSION['Panier']);
				$poids = 0;
				foreach( $ids as $key => $id_produit ) :
					foreach( $data['paniers'] as $key => $produit ) :
						$id = $produit['id'];
						$title = $produit['title'];
						$poids = $produit['weight'];
						if ($id_produit == $id) {
							$total_poids += $poids * $_SESSION['Panier'][$id];
						}
					endforeach;
				endforeach;

				//echo $total_poids;
				?>

				<h3>Choix du mode de livraison</h3>
	            <fieldset>
	                <ul>
	                    <?php
	                    $zones_1 = ['Réunion', 'Guadeloupe', 'Martinique', 'Mayotte', 'Guyane', 'Saint-Pierre et Miquelon', 'Saint-Martin', 'Saint-Barthélemy'];
	                    $zones_2 = ['Polynésie Française', 'Wallis et Futuna', 'Nouvelle Calédonie', 'TAAF'];
	                    $listes = array();

	                    if ( $data['membre']['country'] == 'France' ) {
	                        $listes = ['Retrait sur place', 'Colissimo a domicile', 'Colissimo a domicile - garanti R2', 'Relais colis'];
	                    }

	                    foreach( $zones_1 as $zone_1 ) :
	                        if ( $data['membre']['country'] == $zone_1 ) {
	                            $listes = ['Colissimo Outre-mer Zone 1'];
	                            break;
	                        }
	                    endforeach;

	                    foreach( $zones_2 as $zone_2 ) :
	                        if ( $data['membre']['country'] == $zone_2 ) {
	                            $listes = ['Colissimo Outre-mer Zone 2'];
	                            break;
	                        }
	                    endforeach;

	                    foreach( $data['livraisons'] as $key => $livraison ) :
	                        $id = $livraison['id'];
	                        $type = $livraison['type'];
							$tarif_1 = $livraison['tarif_1'];
							$tarif_2 = $livraison['tarif_2'];
							$tarif_3 = $livraison['tarif_3'];

	                        foreach( $listes as $liste ) :
	                            if ( $type == $liste ) { ?>
									<li>
										<input type= "radio" name="type_livraison" value="<?= $type ?>"
											<?php
											if ( $_POST['type_livraison'] == NULL && $type == 'Retrait sur place' ) {
												echo "checked";
											} else {
												if ( $_POST['type_livraison'] == $type ){
													echo "checked";
												}
											}
											?>
										> <?= $type ?>
										<?php
										if ( $total_poids >= 5 ) {
											echo $tarif_3 . ' €';
										} else {
											if ( $total_poids >= 2 ) {
												echo $tarif_2 . ' €';
											} else {
												echo $tarif_1 . ' €';
											}
										}
										?>
									</li>
									<?php if ( $type == 'Relais colis' ){ ?>
										<input type="text" name="address_relais_colis" class="form-control form-obligatoire" placeholder="Merci de copier/collé ici l’adresse du relais colis de votre choix" value="<?php if ( isset( $_POST['address_relais_colis'] ) ) echo $_POST['address_relais_colis'] ?>">
										<?php if ( isset( $data['erreur']['address_relais_colis'] ) ) : ?>
								            <div class="alert alert-danger alert-form"><?= $data['erreur']['address_relais_colis'] ?></div>
								        <?php endif; ?>
									<?php }
	                            }
	                        endforeach;
	                    endforeach;?>
	                </ul>
	            </fieldset>

				<h3>Choix du mode de paiement</h3>
	            <fieldset>
	                <div>
	                    <input type= "radio" name="mode_paiement" value="cb" checked> Carte Bancaire
	                    <input type= "radio" name="mode_paiement" value="paypal"
							<?php
							if ( $_POST['mode_paiement'] == 'paypal' ) {
								echo "checked";
							}
							?>
						> PayPal
	                    <input type= "radio" name="mode_paiement" value="cheque"
							<?php
							if ( $_POST['mode_paiement'] == 'cheque' ) {
								echo "checked";
							}
							?>
						> Chèque
						<input type= "radio" name="mode_paiement" value="virement"
							<?php
							if ( $_POST['mode_paiement'] == 'virement' ) {
								echo "checked";
							}
							?>
						> Virement Bancaire
	                </div>
	                <div>
	                    <input type="submit" class="btn btn-record" value="Passer la commande">
	                </div>
	            </fieldset>
	        </form>
	    <?php }else{ ?>
	    <section class="page-inner inner-sign-in clearfix">
			<header class="page-header d-flex justify-content-center align-items-center">
		    	<h1 class="text-xs-center">Connectez-vous</h1>
			</header>
			<div class="container content-log-sign">
		        <div class="row">
			        <div class="col-12 col-md-6 col-log-in">
				        <h2>Vous devez avoir un compte client et être connecter pour passer votre commande.</h2>
				        <form class="connexion-inner form-enter" action="" method="post" class="p-y-3 p-x-2" enctype="multipart/form-data" novalidate>
				    <input type="text" name="login" class="form-control" placeholder="Identifiant" value="<?php if ( isset( $_POST['login'] ) ) echo $_POST['login'] ?>">
		            <input type="password" name="password" class="form-control" placeholder="Mot de passe">
		            <div class="btn input-log-in">
		                <input type="submit" value="Connexion">
		            </div>
		        </form>
		        <?php if ( isset( $data['erreur'] ) ) : ?>
				    <div class="alert alert-danger alert-on"><?= $data['erreur'] ?></div>
		        <?php endif; ?>
			        </div>
			        <div class="col-12 col-md-6 col-sign-in">
				        <h2>Si vous n'avez pas encore de compte merci de vous inscrire préalablement.</h2>
				        <a href="/membre/inscription" class="button d-inline-block link-sign-in">S'inscrire</a>
			        </div>
		        </div>


		<?php }?>
				<div class="container-btn-view-bucket container-fluid d-flex justify-content-center mt-3">
					<?php include($_SERVER['DOCUMENT_ROOT'].'/app/views/include/inc/button-come-back.php'); ?>
				</div>
			</div>
	</section>
	<?php require($_SERVER['DOCUMENT_ROOT'].'/app/views/include/footer.php'); ?>
