<?php	require($_SERVER['DOCUMENT_ROOT'].'/app/views/include/header.php');?>

	<section id="container-recap-panier" class="container-recap-panier">
<?php require($_SERVER['DOCUMENT_ROOT'].'/app/views/include/inc/widgets-recap-panier.php'); ?>
	</section>
	<section id="page-inner" class="page-inner">
		<header class="page-header d-flex justify-content-center align-items-center">
	   		<h1 class="container p-lg-0">La boutique</h1>
		</header>
		<div class="container-article container p-lg-0">
			<div class="row">
			<?php
				foreach( $data['produits'] as $key => $produit ) :
				include('data-products.php');	?>
		        <article class="col-12 col-md-6 col-lg-4">
					<?php include($root . '/inc/image-product.php');?>
					<header class="header-product d-flex justify-content-between">
						<h2 class="title-product"><?= $title ?></h2><p class="price-product"><?= number_format($price, 2, ',', ' ') ?> €</p>
					</header>
<!-- 	            	<p><?= $description ?></p> -->

					<footer class="footer-product d-flex justify-content-between">
						<a class="seeMore button col-auto mr-0 pr-3 pl-3" href="/produits/single/<?= $id ?>">En savoir plus</a>
						<?php if ( $stock == 0 ) { ?>
							<p class="alert alert-warning stock-warning col-auto">Stock épuisé.</p>
						<?php } else { ?>
							<a id="data-id-<?= $id ?>" class="addPanier button pr-3 pl-3 col-auto" href="/produits/ajout/<?= $id ?>" data-id="<?= $id ?>"><i class="ti-shopping-cart"></i>Ajouter au panier</a>
						<?php } ?>
		        </article>

					</footer>

			<?php endforeach;?>
    		</div>
		</div><!-- .container -->

	</section>

<?php require($_SERVER['DOCUMENT_ROOT'].'/app/views/include/footer.php'); ?>