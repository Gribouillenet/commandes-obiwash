<?php require($_SERVER['DOCUMENT_ROOT'].'/app/views/include/header.php');?>
<section class="page-inner inner-sign-in clearfix">
    <header class="page-header d-flex justify-content-center align-items-center">
        <h1 class="text-xs-center">Connectez-vous</h1>
        <?php if (isset($data['erreur'])) : ?>
        <?php endif; ?>
    </header>
    <div class="container content-log-sign">
        <div class="row">
            <div class="col-12 col-md-6 col-log-in d-flex flex-column justify-content-center align-items-center">
                <h2>Pour commander,<span class="d-block">vous devez avoir un compte client et être connecté.</span></h2>
                <form class="connexion-inner form-enter" action="" method="post" class="p-y-3 p-x-2" enctype="multipart/form-data" novalidate>
                    <!-- <label for="login">Identifiant</label> -->
                    <input type="text" name="login" class="form-control" placeholder="Identifiant" value="<?php if (isset($_POST['login'])) {
    echo $_POST['login'];
} ?>">
                    <!--  <label for="password">Mot de passe</label> -->
                    <input type="password" name="password" class="form-control" placeholder="Mot de passe">
                    <div class="btn input-log-in">
                        <button><input type="submit" value="Se connecter"><i class="ti-lock"></i></button>
                        <a href="/membre/password" class="mdp-forget d-block">Mot de passe oublié ?</a>
                    </div>
                </form>
                <?php if (isset($data['erreur'])) : ?>
                <div class="alert alert-danger alert-on"><?= $data['erreur'] ?></div>
                <?php endif; ?>
            </div>
            <div class="col-12 col-md-6 col-sign-in d-flex flex-column justify-content-center align-items-center">
                <h2>Vous n'avez pas encore de compte ?<span class="d-block">Merci de vous inscrire préalablement en cliquant ci-dessous.</span></h2>
                <a href="/membre/inscription" class="button d-inline-block link-sign-in">S'inscrire<i class="ti-user"></i></a>
            </div>
        </div>
        <div class="container-btn-view-bucket container-fluid d-flex justify-content-center mt-3">
            <?php include($_SERVER['DOCUMENT_ROOT'].'/app/views/include/inc/button-come-back.php'); ?>
        </div>
    </div>
</section>
<?php require($_SERVER['DOCUMENT_ROOT'].'/app/views/include/footer.php'); ?>