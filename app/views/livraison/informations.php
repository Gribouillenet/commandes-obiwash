<?php require($_SERVER['DOCUMENT_ROOT'].'/app/views/include/header.php'); ?>


<?php if (isset($_SESSION['Auth']['id'])) { ?>
<section class="page-inner clearfix">
    <header class="page-header d-flex justify-content-center align-items-center">
        <h1>Informations personnelles</h1>
    </header>
    <?php require($_SERVER['DOCUMENT_ROOT'].'/app/views/include/bandeau-icons.php'); ?>
    <div class="container">
        <div class="container-paiement">
            <h2>Vos informations de livraison</h2>
            <form class="form-label d-flex flex-column form-fiche-client form-enter" action="" method="post" class="p-y-3 p-x-2" enctype="multipart/form-data" novalidate>
                <div class="row">
                    <div class="col-12 col-lg-6">
                        <div class="row">
                            <div class="col-md-6">
                                <label for="name">Nom</label>
                                <input type="text" name="name" class="form-control form-obligatoire" placeholder="Nom" value="<?php echo $data['membre']['name']; ?>" disabled>
                            </div>
                            <div class="col-md-6">
                                <label for="first_name">Prénom</label>
                                <input type="text" name="first_name" class="form-control form-obligatoire" placeholder="Prénom" value="<?php echo $data['membre']['first_name']; ?>" disabled>
                            </div>
                        </div>
                        <label for="company_service">Société/Service</label>
                        <input type="text" name="company_service" class="form-control form-obligatoire" placeholder="Société/Service" value="<?php echo $data['membre']['company_service']; ?>" disabled>

                        <label for="address">Adresse</label>
                        <input type="text" name="address" class="form-control form-obligatoire" placeholder="Adresse" value="<?php echo $data['membre']['address']; ?>" disabled>

                        <label for="address_supplement">Complément d'adresse</label>
                        <input type="text" name="address_supplement" class="form-control form-obligatoire" placeholder="Complément d'adresse" value="<?php echo $data['membre']['address_supplement']; ?>" disabled>
                    </div>
                    <div class="col-12 col-lg-6">
                        <div class="row">
                            <div class="col-md-4">
                                <label for="code_postal">Code postal</label>
                                <input type="text" name="code_postal" class="form-control form-obligatoire" placeholder="Code postal" value="<?php echo $data['membre']['code_postal']; ?>" disabled>
                            </div>
                            <div class="col-md-8">
                                <label for="city">Ville</label>
                                <input type="text" name="city" class="form-control form-obligatoire" placeholder="Ville" value="<?php echo $data['membre']['city']; ?>" disabled>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <label for="country">Pays</label>
                                <input type="text" name="country" class="form-control form-obligatoire" placeholder="Pays" value="<?php echo $data['membre']['country']; ?>" disabled>
                            </div>
                            <div class="col-md-8">
                                <label for="tel">Téléphone</label>
                                <input type="text" name="tel" class="form-control form-obligatoire" placeholder="Téléphone" value="<?php echo $data['membre']['tel']; ?>" disabled>
                            </div>
                        </div>
                        <label for="mail_membre">Email</label>
                        <input type="text" name="mail_membre" class="form-control form-obligatoire" placeholder="Email" value="<?php echo $data['membre']['mail_member']; ?>" disabled>
                        <div class="col-md-12">
                            <a class="button link-come-back btn-edit-profil d-flex align-items-center justify-content-center" href="/membre/compte">Éditer son profil</a>
                        </div>
                    </div>
                </div>
            </form>
            <h2>Vos informations de facturation (si différentes)</h2>
        </div>
        <form class="form-label form-fiche-client form-enter" action="" method="post" class="p-y-3 p-x-2" enctype="multipart/form-data" novalidate>
            <div class="row">
                <div class="col-md-6">


                    <div class="row">
                        <div class="col-md-6">
                            <label for="name">Nom</label>
                            <input type="text" name="name" class="form-control form-obligatoire" placeholder="Nom" value="<?php if (isset($_POST['name'])) {
    echo $_POST['name'];
} ?>">
                        </div>
                        <div class="col-md-6">
                            <label for="first_name">Prénom</label>
                            <input type="text" name="first_name" class="form-control form-obligatoire" placeholder="Prénom" value="<?php if (isset($_POST['first_name'])) {
    echo $_POST['first_name'];
} ?>">
                        </div>
                    </div>
                    <label for="company_service">Société/Service</label>
                    <input type="text" name="company_service" class="form-control form-obligatoire" placeholder="Société/Service" value="<?php if (isset($_POST['company_service'])) {
    echo $_POST['company_service'];
} ?>">

                    <label for="address">Adresse</label>
                    <input type="text" name="address" class="form-control form-obligatoire" placeholder="Adresse" value="<?php if (isset($_POST['address'])) {
    echo $_POST['address'];
} ?>">
                </div>
                <div class="col-md-6">
                    <label for="address_supplement">Complément d'adresse</label>
                    <input type="text" name="address_supplement" class="form-control form-obligatoire" placeholder="Complément d'adresse" value="<?php if (isset($_POST['address_supplement'])) {
    echo $_POST['address_supplement'];
} ?>">
                    <div class="row">
                        <div class="col-md-4">
                            <label for="code_postal">Code postal</label>
                            <input type="text" name="code_postal" class="form-control form-obligatoire" placeholder="Code postal" value="<?php if (isset($_POST['code_postal'])) {
    echo $_POST['code_postal'];
} ?>">
                        </div>
                        <div class="col-md-8">
                            <label for="city">Ville</label>
                            <input type="text" name="city" class="form-control form-obligatoire" placeholder="Ville" value="<?php if (isset($_POST['city'])) {
    echo $_POST['city'];
} ?>">
                        </div>
                    </div>
                </div>
            </div>
            <?php
                $nb_obiwash = 0;
                foreach ($data['paniers'] as $key => $produit) :
                    $id = $produit['id'];
                    $title = $produit['title'];
                    $trimmed = trim($title, '®');
                    $trimmed = strtolower($trimmed);
                    $obi = 'obiwash';

                    if ($trimmed == $obi) {
                        $nb_obiwash = $_SESSION['Panier'][$id];
                    }

                endforeach;

                if ($nb_obiwash != 0) { ?>
            <p><b>ATTENTION ! Vous avez mis <?= $nb_obiwash ?> Obiwash® dans votre panier.</b><br> Afin de garantir légalement et nominativement chaque appareil, veuillez remplir les champs suivant. Merci de votre compréhension</p>
            <p>* Champs Obligatoires</p>

            <div class="adresse-garantie">
                <h3>Adresse(s) de garantie</h3>

                <?php for ($i = 1; $i <= $nb_obiwash; $i+=1) { ?>
                <h4>Obiwash n°<?= $i ?></h4>
                <label for="address_<?= $i ?>">Nom Prénom - Adresse postale*</label>
                <input type="text" name="address_<?= $i ?>" class="form-control form-obligatoire" placeholder="Nom Prénom - Adresse postale" value="<?php if (isset($_POST['address_'.$i])) {
                    echo $_POST['address_'.$i];
                } ?>">
                <?php if (isset($data['erreur']['address_'.$i])) : ?>
                <div class="alert alert-danger alert-form d-flex"><?= $data['erreur']['address_'.$i] ?></div>
                <?php endif; ?>
                <?php }
                }?>

                <?php
                $ids = array_keys($_SESSION['Panier']);
                $poids = 0;
                foreach ($ids as $key => $id_produit) :
                    foreach ($data['paniers'] as $key => $produit) :
                        $id = $produit['id'];
                        $title = $produit['title'];
                        $poids = $produit['weight'];
                        if ($id_produit == $id) {
                            $total_poids += $poids * $_SESSION['Panier'][$id];
                        }
                    endforeach;
                endforeach;

                //echo $total_poids;
                ?>
                <h2>Choix du mode de livraison</h2>
                <fieldset class="paiement">
                    <ul>
                        <?php
                        $zones_1 = ['Réunion', 'Guadeloupe', 'Martinique', 'Mayotte', 'Guyane', 'Saint-Pierre et Miquelon', 'Saint-Martin', 'Saint-Barthélemy'];
                        $zones_2 = ['Polynésie Française', 'Wallis et Futuna', 'Nouvelle Calédonie', 'TAAF'];
                        $listes = array();

                        if ($data['membre']['country'] == 'France') {
                            $listes = ['Retrait sur place', 'Colissimo a domicile', 'Colissimo a domicile - garanti R2', 'Relais colis'];
                        }

                        foreach ($zones_1 as $zone_1) :
                            if ($data['membre']['country'] == $zone_1) {
                                $listes = ['Colissimo Outre-mer Zone 1'];
                                break;
                            }
                        endforeach;

                        foreach ($zones_2 as $zone_2) :
                            if ($data['membre']['country'] == $zone_2) {
                                $listes = ['Colissimo Outre-mer Zone 2'];
                                break;
                            }
                        endforeach;

                        foreach ($data['livraisons'] as $key => $livraison) {
                            $id = $livraison['id'];
                            $type = $livraison['type'];
                            $tarif_1 = $livraison['tarif_1'];
                            $tarif_2 = $livraison['tarif_2'];
                            $tarif_3 = $livraison['tarif_3'];

                            foreach ($listes as $liste) {
                                if ($type == $liste) { ?>
                        <li class="d-flex justify-content-between align-items-center">
                            <input class="col-md-1" type="radio" name="type_livraison" value="<?= $type ?>" <?php
                                            if ($_POST['type_livraison'] == null && $type == 'Retrait gratuit sur place') {
                                                echo "checked";
                                            } else {
                                                if ($_POST['type_livraison'] == $type) {
                                                    echo "checked";
                                                }
                                            }
                                            ?>>
                            <?php
                                            if ($type == 'Colissimo a domicile' or $type == 'Colissimo a domicile - garanti R2') {
                                                $img = '/public/img/png/colissimo.png';
                                            } elseif ($type == 'Relais colis') {
                                                $img = '/public/img/png/relais-colis.png';
                                            } else {
                                                $img='';
                                            }
                                            echo '<figure class="logo-livraison col-md-1"><img src="'. $img .'"></figure>';
                                            ?>

                            <?php if ($type == 'Relais colis') { ?>
                            <div class="col-md-8 d-flex flex-column choix-relais-colis">
                                <span><?= $type ?></span>
                                <div class="d-flex relais-colis align-items-center">
                                    <a class="button link-relais-colis btn-relais-colis d-flex align-items-center justify-content-center" href="https://www.relaiscolis.com/trouver-un-relais" target="blank">Voir les relais colis</a>
                                    <input type="text" name="address_relais_colis" class="form-control form-obligatoire" placeholder="Merci de copier/collé ici l’adresse du relais colis de votre choix" value="<?php if (isset($_POST['address_relais_colis'])) {
                                                echo $_POST['address_relais_colis'];
                                            } ?>"></input>
                                </div>
                            </div>
                            <?php if (isset($data['erreur']['address_relais_colis'])) : ?>
                            <div class="alert alert-danger alert-form alert-relais-colis"><?= $data['erreur']['address_relais_colis'] ?></div>
                            <?php endif; ?>
                            <?php } else {
                                                echo '<span class="col-md-8">'. $type .'</span>';
                                            }
                                            ?>
                            <div class="prix col-md-2 d-flex align-items-center justify-content-center">
                                    <?php
                                        if ($total_poids >= 5) {
                                            echo '<span class="d-block">' . $tarif_3 . ' €</span>';
                                        } else {
                                            if ($total_poids >= 2) {
                                                echo '<span class="d-block">' . $tarif_2 . ' €</span>';
                                            } else {
                                                echo '<span class="d-block">' . $tarif_1 . ' €</span>';
                                            }
                                        }
                                        ?>
                            </div>
                        </li>
                        <?php
                                }
                            }
                        }
                                                ?>
                    </ul>
                </fieldset>

                <h2>Choix du mode de paiement</h2>
                <fieldset class="paiement">


                    <!-- <ul>
                        <li class="d-flex justify-content-between align-items-center">
                            <input class="grey radio col-md-1" type="radio" name="mode_paiement" value="cb" checked>
                            <div class="col-md-1">
                                <i class="fab fa-cc-visa"></i>
                                <i class="fab fa-cc-mastercard"></i>
                            </div>
                            <div class="col-md-10">
                                <span>Carte Bancaire</span>
                            </div>
                        </li> -->

                        <?php 
                            $frais = DB::select('select * from charges');
                        
                            foreach($frais as $f) {
                                if($f['method'] == 'PayPal'  ) $paypal   = $f['charge'];
                                if($f['method'] == 'PayPlug' ) $payplug  = $f['charge'];
                                if($f['method'] == 'Chèque'  ) $cheque   = $f['charge'];
                                if($f['method'] == 'Virement') $virement = $f['charge'];
                            }

                        ?>

                        <li class="d-flex justify-content-between align-items-center">
                            <input class="grey radio col-md-1" type="radio" name="mode_paiement" value="cb" checked>
                            <div class="col-md-1">
                                <i class="fab fa-cc-visa"></i>
                                <i class="fab fa-cc-mastercard"></i>
                            </div>
                            <div class="col-md-10">
                                <?php 
                                    if($payplug > 0) {
                                        echo "<span>Carte bancaire (frais de participation $payplug €)</span>";
                                    } else {
                                        echo "<span>Carte bancaire</span>";
                                    } 
                                ?>
                            </div>
                        </li>

                        <li class="d-flex justify-content-between align-items-center">
                            <input type="radio" class="radio col-md-1" name="mode_paiement" value="paypal" <?php
                            if ($_POST['mode_paiement'] == 'paypal') {
                                echo "checked";
                            }
                            ?>>
                            <div class="col-md-1">
                                <i class="fab fa-cc-paypal"></i>
                            </div>
                            <div class="col-md-10">
                                <?php 
                                    if($paypal > 0) {
                                        echo "<span>PayPal (frais de participation $paypal €)</span>";
                                    } else {
                                        echo "<span>Paypal</span>";
                                    } 
                                ?>
                            </div>
                        </li>
                        <li class="d-flex justify-content-between align-items-center">
                            <input type="radio" class="radio col-md-1" name="mode_paiement" value="cheque" <?php
                            if ($_POST['mode_paiement'] == 'cheque') {
                                echo "checked";
                            }
                            ?>>
                            <div class="col-md-1">
                                <i class="fas fa-money-check"></i>
                            </div>
                            <div class="col-md-10">
                                <?php 
                                    if($cheque > 0) {
                                        echo "<span>Chèque (frais de participation $cheque €)</span>";
                                    } else {
                                        echo "<span>Chèque</span>";
                                    } 
                                ?>
                            </div>
                        </li>
                        <li class="d-flex justify-content-between align-items-center">
                            <input class="grey radio col-md-1" type="radio" name="mode_paiement" value="virement" <?php
                            if ($_POST['mode_paiement'] == 'virement') {
                                echo "checked";
                            }
                            ?>>
                            <div class="col-md-1">
                                <i class="fas fa-piggy-bank"></i>
                            </div>
                            <div class="col-md-10">
                                <?php 
                                    if($virement > 0) {
                                        echo "<span>Virement (frais de participation $virement €)</span>";
                                    } else {
                                        echo "<span>Virement</span>";
                                    } 
                                ?>
                            </div>
                        </li>
                    </ul>
                </fieldset>
                <div class="container-btn-ordered d-flex justify-content-center">
                    <?php include($_SERVER['DOCUMENT_ROOT'].'/app/views/include/inc/button-come-back.php'); ?>
                    <button type="submit" class="button btn btn-ordered-ok">Continuer la commande<i class="ti-arrow-right"></i></button>

                </div>
        </form>
        <?php } else { ?>
        <section class="page-inner inner-sign-in clearfix">
            <header class="page-header d-flex justify-content-center align-items-center">
                <h1 class="text-xs-center">Connectez-vous</h1>
            </header>
            <div class="container content-log-sign">
                <div class="row">
                    <div class="col-12 col-md-6 col-log-in d-flex flex-column justify-content-center align-items-center">
                        <h2>Pour commander,<span class="d-block">vous devez avoir un compte client et être connecté.</span></h2>
                        <form class="connexion-inner form-enter" action="" method="post" class="p-y-3 p-x-2" enctype="multipart/form-data" novalidate>
                            <!-- <label for="login">Identifiant</label> -->
                            <input type="text" name="login" class="form-control" placeholder="Identifiant" value="<?php if (isset($_POST['login'])) {
                                echo $_POST['login'];
                            } ?>">
                            <!--  <label for="password">Mot de passe</label> -->
                            <input type="password" name="password" class="form-control" placeholder="Mot de passe">
                            <div class="btn input-log-in">
                                <button><input type="submit" value="Se connecter"><i class="ti-lock"></i></button>
                                <a href="/membre/password" class="mdp-forget d-block">Mot de passe oublié ?</a>
                            </div>
                        </form>
                        <?php if (isset($data['erreur'])) : ?>
                        <div class="alert alert-danger alert-on alert-static"><?= $data['erreur'] ?></div>
                        <?php endif; ?>
                    </div>
                    <div class="col-12 col-md-6 col-sign-in d-flex flex-column justify-content-center align-items-center">
                        <h2>Vous n'avez pas encore de compte ?<span class="d-block">Merci de vous inscrire préalablement en cliquant ci-dessous.</span></h2>
                        <a href="/membre/inscription" class="button d-inline-block link-sign-in">S'inscrire<i class="ti-user"></i></a>
                    </div>
                </div>

                <div class="container-btn-ordered d-flex justify-content-center">
                    <?php include($_SERVER['DOCUMENT_ROOT'].'/app/views/include/inc/button-come-back.php'); ?>
                </div>
            </div>
            <?php }?>


        </section>
        <?php require($_SERVER['DOCUMENT_ROOT'].'/app/views/include/footer.php'); ?>
        <script src="../../../public/scripts/cant-go-back.js"></script>