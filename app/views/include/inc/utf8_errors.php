<?php
        ini_set('display_errors', 1);
        ini_set('default_charset', 'UTF-8');
        header('Content-Type: text/html; charset=UTF-8');

        $panier = new Paniers();

        $url_site = 'https://obiwash.gribdev.eu';
        $url_siteCommande = 'https://commandes.obiwash.gribdev.eu';
        $url_css = $url_siteCommande. '/public/css';
        $url_favicon = $url_siteCommande. '/public/img/favicon';

?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="noindex,nofollow" />
    <meta name="description" content="Obiwash® - La boutique -  le mélangeur intelligent pour tous, pour rendre nos lave-linges plus économes en électricité et en eau potable">
    <link rel="apple-touch-icon" sizes="57x57" href="<?= $url_favicon ?>/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?= $url_favicon ?>/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?= $url_favicon ?>/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?= $url_favicon ?>/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?= $url_favicon ?>/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?= $url_favicon ?>/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?= $url_favicon ?>/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?= $url_favicon ?>/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?= $url_favicon ?>/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="<?= $url_favicon ?>/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?= $url_favicon ?>/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?= $url_favicon ?>/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?= $url_favicon ?>/favicon-16x16.png">
    <link rel="manifest" href="<?= $url_favicon ?>/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?= $url_favicon ?>/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <link rel="stylesheet" href="<?= $url_css ?>/style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.12.1/bootstrap-table.min.css">