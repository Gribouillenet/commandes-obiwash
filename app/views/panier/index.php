<?php require($_SERVER['DOCUMENT_ROOT'].'/app/views/include/header.php');?>
	<section class="page-inner clearfix">
		<header class="page-header d-flex justify-content-center align-items-center">
			<h1 class="container">Mon Panier</h1>
			</header>
		<?php include($_SERVER['DOCUMENT_ROOT'].'/app/views/include/cart.php'); ?>

	</section>

<?php require($_SERVER['DOCUMENT_ROOT'].'/app/views/include/footer.php'); ?>
